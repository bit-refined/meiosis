// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::print_stdout,
    clippy::expect_used,
    clippy::float_arithmetic,
    clippy::cast_precision_loss,
    clippy::arithmetic_side_effects,
    clippy::cast_possible_wrap,
    clippy::cast_possible_truncation,
    clippy::unreadable_literal,
    incomplete_features,
    unstable_features
)]
#![feature(generic_const_exprs)]
use core::num::NonZeroUsize;

use rand::{thread_rng, Rng};

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    operators::{
        mutation::{
            add::Add,
            combination::Combination,
            random::single::{finetune::Finetune, nudge::Nudge},
            remove::Remove,
        },
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::{FromGenotype, Phenotype},
    termination::fitness::Fitness as FitnessTermination,
};

// const POINTS_LINE: &[(f64, f64)] = &[(0.0, 0.0), (1.0, 1.0)];

// This is f(x) = -4x + 6 and the algorithm below should approximate it.
const POINTS: &[(f64, f64)] = &[
    (5.3078418550222715, -15.231367420089086),
    (3.8748693134759336, -9.499477253903734),
    (0.6046422408774987, 3.581431036490005),
    (3.222280153468841, -6.889120613875363),
    (9.464384481483332, -31.85753792593333),
    (1.120842761368076, 1.5166289545276959),
    (5.850321737328882, -17.401286949315526),
    (4.920840188524686, -13.683360754098743),
    (2.6887375023885083, -4.754950009554033),
    (3.8250152561196216, -9.300061024478486),
    (2.5532609086536127, -4.213043634614451),
    (1.2699096340853322, 0.9203614636586712),
    (4.306959747399822, -11.227838989599288),
    (5.768401719901601, -17.073606879606405),
    (6.947131240243593, -21.78852496097437),
    (3.4512552431345878, -7.805020972538351),
    (0.837456115598465, 2.65017553760614),
    (7.842237337196466, -25.368949348785865),
    (3.176174049195757, -6.704696196783027),
    (4.829579590319098, -13.318318361276393),
];

struct Polynomial(Vec<f64>);

impl Phenotype for Polynomial {
    type Genotype = Vec<f64>;
}

struct PolynomialRegression {
    points: Vec<(f64, f64)>,
}

impl Environment for PolynomialRegression {}

impl Fitness<PolynomialRegression> for Polynomial {
    fn fitness(&mut self, environment: &PolynomialRegression) -> f64 {
        let mut error_sum = 0.;

        for &(x, y) in &environment.points {
            let mut calculated_y = 0.0;

            for (power, indeterminate) in self.0.iter().enumerate() {
                calculated_y += *indeterminate * x.powi(power as i32);
            }

            let error = (y - calculated_y).powi(2);
            error_sum += error;
        }

        let mean_error = error_sum / environment.points.len() as f64;

        1. / (mean_error + 1.)
    }
}

impl FromGenotype<Vec<f64>, PolynomialRegression> for Polynomial {
    fn from_genotype<RNG>(genotype: &Vec<f64>, _environment: &PolynomialRegression, _rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        Self(genotype.clone())
    }
}

fn main() {
    let environment = PolynomialRegression {
        points: POINTS.to_vec(),
    };
    let mutation = Combination::selective()
        .and(Add::new(1., Some(environment.points.len())))
        .and(Remove::new(1., NonZeroUsize::new(1).unwrap()))
        .and(Nudge::new(1., 1.))
        .and(Finetune::new(1.));

    let mut algo = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(environment)
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(mutation)
        .with_termination(FitnessTermination::new(0.999999999))
        .with_elitism(1)
        // For simple polynomials it is generally faster to turn speciation off.
        .with_speciation_threshold(0.0)
        //.with_interspecies_breeding_chance(0.005)
        .with_random_population(100);

    let result = loop {
        match algo.evaluate::<Polynomial>().check_termination() {
            Ok(new_evaluated) => {
                let iteration = new_evaluated.statistics().iteration;

                if iteration % 100 == 0 {
                    let species = new_evaluated.state().species();

                    let population_count = species.iter().map(|s| s.population.members.len()).sum::<usize>();

                    println!(
                        "generation: {iteration}, population: {population_count}, species: {}",
                        species.len(),
                    );

                    for evaluated_species in species {
                        let fittest = evaluated_species
                            .population
                            .members
                            .iter()
                            .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
                            .expect("iterator can not be empty");

                        println!(
                            "\tSpecies: {}-{}, members: {}, fitness: {}, best member: {}",
                            evaluated_species.identifier.0,
                            evaluated_species.identifier.1,
                            evaluated_species.population.members.len(),
                            fittest.fitness,
                            render_polynomial(&fittest.genotype),
                        );
                    }
                }

                // we evaluated above, so now we need to do the rest of the algorithm, otherwise
                // Rust complains as we're in the wrong state of the state machine
                algo = new_evaluated.select().recombine().mutate().reinsert();
            }
            Err(terminated) => {
                // a termination condition triggered here, so we may want to check the result
                break terminated;
            }
        };
    };

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty");

    println!(
        "generation: {}, fitness: {}, best member: {}",
        result.statistics().iteration,
        fittest.fitness,
        render_polynomial(&fittest.genotype),
    );
}

fn render_polynomial(genes: &[f64]) -> String {
    let indeterminates = genes
        .iter()
        .enumerate()
        .map(|(power, indeterminate)| match power {
            0 => indeterminate.to_string(),
            1 => format!("{indeterminate}x"),
            _ => format!("{indeterminate}x^{power}"),
        })
        .collect::<Vec<_>>()
        .join(" + ");

    format!("f(x) = {indeterminates}")
}
