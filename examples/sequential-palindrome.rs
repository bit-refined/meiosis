// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::use_debug,
    clippy::print_stdout,
    clippy::expect_used,
    clippy::float_arithmetic,
    clippy::arithmetic_side_effects,
    clippy::too_many_lines,
    clippy::cast_precision_loss,
    clippy::integer_division,
    clippy::indexing_slicing,
    incomplete_features,
    unstable_features
)]
#![feature(generic_const_exprs)]

use core::{
    fmt::{Debug, Formatter},
    num::NonZeroUsize,
};

use rand::{thread_rng, Rng};

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    neural::{
        activation::{
            binary::SummationBinary,
            float::ActivationFunction,
            gaussian::SummationGaussian,
            identity::SummationIdentity,
            linear_unit::{
                SummationLeakyRectifiedLinearUnit, SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
            },
            logistic::SummationLogistic,
            mish::SummationMish,
            softplus::SummationSoftPlus,
            tangent::SummationHyperbolicTangent,
        },
        input::ToNetworkInput,
        network::{recurrent::Recurrent, Network},
    },
    operators::{
        mutation::{
            add::network::ExpandNetwork,
            combination::Combination,
            random::single::{finetune::Finetune, nudge::Nudge, uniform::network::UniformNetwork},
            remove::Remove,
        },
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::FromGenotype,
    termination::fitness::Fitness as FitnessTermination,
};

// Anything above 5 takes quite some time.
const PALINDROME_LENGTH: usize = 3;
const EVALUATION_COUNT: usize = 1000;

struct PalindromeEnvironment;

impl Environment for PalindromeEnvironment {}

impl Fitness<PalindromeEnvironment> for Network<1, 1, f64, Recurrent<f64>> {
    fn fitness(&mut self, _environment: &PalindromeEnvironment) -> f64 {
        let mut fitness = 0.0;

        // This function makes sure that about 50% of the sequences are palindromes.
        let sequences = generate_sequences(PALINDROME_LENGTH, EVALUATION_COUNT);

        for full_sequence in sequences {
            // Each evaluation needs a fully reset network.
            self.reset();

            let mut predicted_correct = 0;

            for (index, input) in full_sequence.iter().enumerate() {
                // We use partial sequences to reward partial success.
                // Note that this does not mean that the networks can detect shorter palindromes,
                // as a longer sequence overwrites the previous `correct` value.
                let sequence = &full_sequence[..=index];
                let is_palindrome = is_palindrome(sequence);

                self.set_inputs(*input);
                self.tick();

                let output = self.get_outputs::<f64>();

                let is_palindrome_from_network = output >= 0.5;

                if is_palindrome_from_network == is_palindrome && sequence.len() > predicted_correct {
                    predicted_correct = sequence.len();
                }
            }

            fitness += predicted_correct as f64 / PALINDROME_LENGTH as f64
        }

        fitness
    }
}

// The following are trivial implementations, but they perfectly demonstrate usage of the In- and Output traits.
#[derive(Clone, Copy, PartialEq)]
enum BinaryInput {
    Zero,
    One,
}

// One could also use a single input for the network and have them learn to differentiate between
// various values, but using two inputs is simply faster to learn for the sake of this example.
impl ToNetworkInput<f64, 1> for BinaryInput {
    fn to_network_input(self) -> [f64; 1] {
        match self {
            BinaryInput::Zero => [0.0],
            BinaryInput::One => [1.0],
        }
    }
}

impl Debug for BinaryInput {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match *self {
            BinaryInput::Zero => write!(f, "0"),
            BinaryInput::One => write!(f, "1"),
        }
    }
}

fn generate_sequences(sequence_length: usize, sequence_count: usize) -> Vec<Vec<BinaryInput>> {
    assert!(sequence_length > 1, "Sequence length must be greater than 1.");
    let half_length = sequence_length / 2;

    let mut rng = thread_rng();
    let mut sequences = Vec::new();

    // Generate half palindromes and half non-palindromes.
    for _ in 0..sequence_count / 2 {
        let mut palindrome = (0..half_length)
            .map(|_| if rng.gen() { BinaryInput::One } else { BinaryInput::Zero })
            .collect::<Vec<_>>();

        let mut reversed = palindrome.clone();
        reversed.reverse();

        // For odd lengths, add a random central element.
        if sequence_length % 2 == 1 {
            palindrome.push(if rng.gen() { BinaryInput::One } else { BinaryInput::Zero });
        }

        // Concatenate the original half and the reversed half to form a palindrome.
        palindrome.extend(reversed);
        sequences.push(palindrome);

        // Generate a non-palindrome sequence.
        let mut non_palindrome: Vec<BinaryInput>;
        loop {
            non_palindrome = (0..sequence_length)
                .map(|_| if rng.gen() { BinaryInput::One } else { BinaryInput::Zero })
                .collect();
            if !is_palindrome(&non_palindrome) {
                break;
            }
        }
        sequences.push(non_palindrome);
    }

    // Handle the case where sequence_count is odd.
    if sequence_count % 2 == 1 {
        let mut non_palindrome: Vec<BinaryInput>;
        loop {
            non_palindrome = (0..sequence_length)
                .map(|_| if rng.gen() { BinaryInput::One } else { BinaryInput::Zero })
                .collect();
            if !is_palindrome(&non_palindrome) {
                break;
            }
        }
        sequences.push(non_palindrome);
    }

    sequences
}

fn is_palindrome(sequence: &[BinaryInput]) -> bool {
    let len = sequence.len();

    sequence
        .iter()
        // For even-length sequences, this covers all necessary comparisons.
        // For odd-length sequences, the middle element doesn't need to be compared since it's the same in both the forward and reversed halves.
        .take(len / 2)
        .zip(sequence.iter().rev())
        .all(|(a, b)| a == b)
}

fn main() {
    let available_activation_functions: Vec<ActivationFunction<f64>> = vec![
        ActivationFunction::Binary(SummationBinary::new()),
        ActivationFunction::Logistic(SummationLogistic::new()),
        ActivationFunction::Gaussian(SummationGaussian::new()),
        ActivationFunction::Identity(SummationIdentity::new()),
        ActivationFunction::HyperbolicTangent(SummationHyperbolicTangent::new()),
        ActivationFunction::RectifiedLinearUnit(SummationRectifiedLinearUnit::new()),
        ActivationFunction::SoftPlus(SummationSoftPlus::new()),
        ActivationFunction::LeakyRectifiedLinearUnit(SummationLeakyRectifiedLinearUnit::new()),
        ActivationFunction::SigmoidLinearUnit(SummationSigmoidLinearUnit::new()),
        ActivationFunction::Mish(SummationMish::new()),
    ];

    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(PalindromeEnvironment)
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(
            Combination::selective()
                .and(
                    ExpandNetwork::new(0.3)
                        .with_max_genes(50)
                        .with_activation_functions(available_activation_functions.clone()),
                )
                .and(UniformNetwork::new(1.0).with_activation_functions(available_activation_functions))
                .and(Remove::new(0.25, NonZeroUsize::new(1).unwrap()))
                .and(Nudge::new(1.0, 1.0))
                .and(Finetune::new(1.0)),
        )
        .with_termination(FitnessTermination::new(EVALUATION_COUNT as f64));

    let mut algo = config
        .with_elitism(2)
        //.with_speciation_threshold(0.95)
        //.with_interspecies_breeding_chance(0.001)
        .with_random_population(100);

    let result = loop {
        match algo
            .evaluate::<Network<1, 1, f64, Recurrent<f64>>>()
            .check_termination()
        {
            Ok(evaluated) => {
                let iteration = evaluated.statistics().iteration;

                if iteration % 1000 == 0 {
                    let species = evaluated.state().species();

                    let population_count = species.iter().map(|s| s.population.members.len()).sum::<usize>();

                    println!(
                        "generation: {iteration}, population: {population_count}, species: {}",
                        species.len(),
                    );

                    for evaluated_species in species {
                        let fittest = evaluated_species
                            .population
                            .members
                            .iter()
                            .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
                            .expect("iterator can not be empty");

                        let genotype = &fittest.genotype;
                        let fittest_network = Network::from_genotype(&fittest.genotype, &(), &mut thread_rng());

                        let hidden = fittest_network.neurons().len();
                        let connections = fittest_network.connections().len();

                        println!(
                            "\tSpecies: {}-{}, members: {}, fitness: {}, best member: \"H:{} - C:{} - I:{}\"",
                            evaluated_species.identifier.0,
                            evaluated_species.identifier.1,
                            evaluated_species.population.members.len(),
                            fittest.fitness,
                            hidden,
                            connections,
                            genotype.instructions().len()
                        );
                    }
                }

                algo = evaluated.select().recombine().mutate().reinsert();
            }
            Err(terminated) => break terminated,
        }
    };

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty")
        .clone();

    let mut fittest_network = Network::from_genotype(&fittest.genotype, &(), &mut thread_rng());
    let genotype = fittest.genotype;

    let hidden = fittest_network.neurons().len();
    let connections = fittest_network.connections().len();

    println!(
        "\nGeneration: {}, fitness: {}, best member: \"H:{} - C:{} - I:{}\"",
        result.statistics().iteration,
        fittest.fitness,
        hidden,
        connections,
        genotype.instructions().len()
    );

    fittest_network.reset();

    println!("Genotype: {genotype:?}");
    println!("Network: {fittest_network:?}");

    for sequence in generate_sequences(PALINDROME_LENGTH, 5) {
        println!("Full Sequence: {sequence:?}");

        let len = sequence.len();
        let is_palindrome = sequence
            .iter()
            // For even-length sequences, this covers all necessary comparisons.
            // For odd-length sequences, the middle element doesn't need to be compared since it's the same in both the forward and reversed halves.
            .take(len / 2)
            .zip(sequence.iter().rev())
            .all(|(a, b)| a == b);
        println!("\tIs Palindrome: {is_palindrome}");

        fittest_network.reset();

        for input in sequence {
            fittest_network.set_inputs(input);
            fittest_network.tick();
        }

        let output = fittest_network.get_outputs::<f64>();

        let is_palindrome_from_network = output >= 0.5;

        println!("\tIs Palindrome (network): {is_palindrome_from_network}");
    }

    println!("Rendered MD Mermaid:");
    println!("{}\n", fittest_network.render_mermaid());
}
