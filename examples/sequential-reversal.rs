// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::use_debug,
    clippy::print_stdout,
    clippy::expect_used,
    clippy::float_arithmetic,
    clippy::arithmetic_side_effects,
    clippy::too_many_lines,
    clippy::indexing_slicing,
    clippy::cast_precision_loss,
    incomplete_features,
    unstable_features
)]
#![feature(generic_const_exprs)]

use core::{
    fmt::{Debug, Formatter},
    num::NonZeroUsize,
};

use rand::{thread_rng, Rng};

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    neural::{
        activation::{
            binary::SummationBinary,
            float::ActivationFunction,
            gaussian::SummationGaussian,
            identity::SummationIdentity,
            linear_unit::{
                SummationLeakyRectifiedLinearUnit, SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
            },
            logistic::SummationLogistic,
            mish::SummationMish,
            softplus::SummationSoftPlus,
            tangent::SummationHyperbolicTangent,
        },
        input::ToNetworkInput,
        network::{recurrent::Recurrent, Network},
    },
    operators::{
        mutation::{
            add::network::ExpandNetwork,
            combination::Combination,
            random::single::{finetune::Finetune, nudge::Nudge, uniform::network::UniformNetwork},
            remove::Remove,
        },
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::FromGenotype,
    termination::fitness::Fitness as FitnessTermination,
};

// Anything above 2 takes quite some time.
const SEQUENCE_LENGTH: usize = 2;
const EVALUATION_COUNT: usize = 1000;

struct PalindromeEnvironment;

impl Environment for PalindromeEnvironment {}

impl Fitness<PalindromeEnvironment> for Network<1, 1, f64, Recurrent<f64>> {
    fn fitness(&mut self, _environment: &PalindromeEnvironment) -> f64 {
        let mut fitness = 0.0;

        let sequences = generate_sequences(SEQUENCE_LENGTH, EVALUATION_COUNT);

        for sequence in sequences {
            // Each evaluation needs a fully reset network.
            self.reset();

            let mut detected_correct = 0;

            for input in &sequence {
                self.set_inputs(*input);
                self.tick();
            }

            for i in (1..=SEQUENCE_LENGTH).rev() {
                // We use -1 as a marker "token" to tell the network that we'd like to query the sequence now.
                self.set_inputs(-1.0);
                self.tick();

                let output = self.get_outputs::<f64>();

                if (output >= 0.5 && sequence[i - 1] == BinaryInput::One)
                    || (output < 0.5 && sequence[i - 1] == BinaryInput::Zero)
                {
                    detected_correct += 1;
                }
            }

            fitness += f64::from(detected_correct) / SEQUENCE_LENGTH as f64
        }

        fitness
    }
}

// The following are trivial implementations, but they perfectly demonstrate usage of the In- and Output traits.
#[derive(Clone, Copy, PartialEq)]
enum BinaryInput {
    Zero,
    One,
}

// One could also use a single input for the network and have them learn to differentiate between
// various values, but using two inputs is simply faster to learn for the sake of this example.
impl ToNetworkInput<f64, 1> for BinaryInput {
    fn to_network_input(self) -> [f64; 1] {
        match self {
            BinaryInput::Zero => [0.0],
            BinaryInput::One => [1.0],
        }
    }
}

impl Debug for BinaryInput {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match *self {
            BinaryInput::Zero => write!(f, "0"),
            BinaryInput::One => write!(f, "1"),
        }
    }
}

fn generate_sequences(sequence_length: usize, sequence_count: usize) -> Vec<Vec<BinaryInput>> {
    assert!(sequence_length > 1, "Sequence length must be greater than 1.");

    let mut rng = thread_rng();
    let mut sequences = Vec::new();

    while sequences.len() < sequence_count {
        let sequence = (0..sequence_length)
            .map(|_| if rng.gen() { BinaryInput::One } else { BinaryInput::Zero })
            .collect::<Vec<_>>();

        if let Some(first) = sequence.first() {
            if sequence.iter().all(|&x| x == *first) {
                continue;
            }
        }

        sequences.push(sequence)
    }

    sequences
}

fn main() {
    let available_activation_functions: Vec<ActivationFunction<f64>> = vec![
        ActivationFunction::Binary(SummationBinary::new()),
        ActivationFunction::Logistic(SummationLogistic::new()),
        ActivationFunction::Gaussian(SummationGaussian::new()),
        ActivationFunction::Identity(SummationIdentity::new()),
        ActivationFunction::HyperbolicTangent(SummationHyperbolicTangent::new()),
        ActivationFunction::RectifiedLinearUnit(SummationRectifiedLinearUnit::new()),
        ActivationFunction::SoftPlus(SummationSoftPlus::new()),
        ActivationFunction::LeakyRectifiedLinearUnit(SummationLeakyRectifiedLinearUnit::new()),
        ActivationFunction::SigmoidLinearUnit(SummationSigmoidLinearUnit::new()),
        ActivationFunction::Mish(SummationMish::new()),
    ];

    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(PalindromeEnvironment)
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(
            Combination::selective()
                .and(
                    ExpandNetwork::new(0.3)
                        .with_max_genes(500)
                        .with_activation_functions(available_activation_functions.clone()),
                )
                .and(UniformNetwork::new(1.0).with_activation_functions(available_activation_functions))
                .and(Remove::new(0.25, NonZeroUsize::new(1).unwrap()))
                .and(Nudge::new(1.0, 1.0))
                .and(Finetune::new(1.0)),
        )
        .with_termination(FitnessTermination::new(EVALUATION_COUNT as f64));

    let mut algo = config
        .with_elitism(2)
        //.with_speciation_threshold(0.95)
        //.with_interspecies_breeding_chance(0.001)
        .with_random_population(25);

    let result = loop {
        match algo
            .evaluate::<Network<1, 1, f64, Recurrent<f64>>>()
            .check_termination()
        {
            Ok(evaluated) => {
                let iteration = evaluated.statistics().iteration;

                if iteration % 1000 == 0 {
                    let species = evaluated.state().species();

                    let population_count = species.iter().map(|s| s.population.members.len()).sum::<usize>();

                    println!(
                        "generation: {iteration}, population: {population_count}, species: {}",
                        species.len(),
                    );

                    for evaluated_species in species {
                        let fittest = evaluated_species
                            .population
                            .members
                            .iter()
                            .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
                            .expect("iterator can not be empty");

                        let genotype = &fittest.genotype;
                        let fittest_network = Network::from_genotype(&fittest.genotype, &(), &mut thread_rng());

                        let hidden = fittest_network.neurons().len();
                        let connections = fittest_network.connections().len();

                        println!(
                            "\tSpecies: {}-{}, members: {}, fitness: {}, best member: \"H:{} - C:{} - I:{}\"",
                            evaluated_species.identifier.0,
                            evaluated_species.identifier.1,
                            evaluated_species.population.members.len(),
                            fittest.fitness,
                            hidden,
                            connections,
                            genotype.instructions().len()
                        );
                    }
                }

                algo = evaluated.select().recombine().mutate().reinsert();
            }
            Err(terminated) => break terminated,
        }
    };

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty")
        .clone();

    let mut fittest_network = Network::from_genotype(&fittest.genotype, &(), &mut thread_rng());
    let genotype = fittest.genotype;

    let hidden = fittest_network.neurons().len();
    let connections = fittest_network.connections().len();

    println!(
        "\nGeneration: {}, fitness: {}, best member: \"H:{} - C:{} - I:{}\"",
        result.statistics().iteration,
        fittest.fitness,
        hidden,
        connections,
        genotype.instructions().len()
    );

    fittest_network.reset();

    println!("Genotype: {genotype:?}");
    println!("Network: {fittest_network:?}");

    for sequence in generate_sequences(SEQUENCE_LENGTH, 5) {
        println!("Full Sequence: {sequence:?}");

        fittest_network.reset();

        for input in &sequence {
            fittest_network.set_inputs(*input);
            fittest_network.tick();
        }

        let mut output_sequence = Vec::with_capacity(SEQUENCE_LENGTH);

        for i in (1..=SEQUENCE_LENGTH).rev() {
            // We use -1 as a marker "token" to tell the network that we'd like to query the sequence now.
            fittest_network.set_inputs(-1.0);
            fittest_network.tick();

            let output = fittest_network.get_outputs::<f64>();

            if output >= 0.5 && sequence[i - 1] == BinaryInput::One {
                output_sequence.push(BinaryInput::One);
            }

            if output < 0.5 && sequence[i - 1] == BinaryInput::Zero {
                output_sequence.push(BinaryInput::Zero);
            }
        }

        println!("Output: {output_sequence:?}");
    }

    println!("Rendered MD Mermaid:");
    println!("{}\n", fittest_network.render_mermaid());
}
