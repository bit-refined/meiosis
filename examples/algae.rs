// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::print_stdout,
    clippy::indexing_slicing,
    clippy::float_cmp,
    clippy::float_arithmetic,
    clippy::arithmetic_side_effects,
    clippy::integer_division,
    clippy::pattern_type_mismatch,
    clippy::expect_used,
    clippy::too_many_lines,
    clippy::non_ascii_literal,
    clippy::cast_sign_loss,
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::shadow_reuse
)]

use core::{
    fmt::{Display, Formatter, Write},
    num::NonZeroUsize,
    time::Duration,
};
use std::thread::sleep;

use rand::{
    seq::{IteratorRandom, SliceRandom},
    thread_rng, Rng,
};
use rayon::prelude::*;

use meiosis::{
    environment::Environment,
    genotype::network::{Network as NetworkGenotype, Recurrent as RecurrentType},
    neural::{
        activation::{
            binary::SummationBinary,
            float::ActivationFunction,
            gaussian::SummationGaussian,
            identity::SummationIdentity,
            linear_unit::{
                SummationLeakyRectifiedLinearUnit, SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
            },
            logistic::SummationLogistic,
            mish::SummationMish,
            softplus::SummationSoftPlus,
            tangent::SummationHyperbolicTangent,
        },
        input::ToNetworkInput,
        network::{recurrent::Recurrent, Network},
        neuron::Neuron,
        output::FromNetworkOutput,
    },
    operators::mutation::{
        add::network::ExpandNetwork,
        combination::Combination,
        random::single::{finetune::Finetune, nudge::Nudge, uniform::network::UniformNetwork},
        remove::Remove,
        Mutation,
    },
    phenotype::FromGenotype,
    random::Random,
};

const MAP_SIZE: usize = 30;
const MIN_ALGAE: usize = MAP_SIZE;
const MAX_ALGAE: usize = MAP_SIZE.pow(2);

const MOVE_COST: f64 = 0.01;
const ITERATION_COST: f64 = 0.02;
// Per iteration, each algae receives energy equivalent to
// `Light Intensity` multiplied by this constant.
const ENERGY_ABSORPTION_RATE: f64 = 0.5;

const ITERATION_WAIT_TIME: Duration = Duration::from_millis(66);
// the amount of iterations the light source doesn't move
const LIGHT_SOURCE_MOVE_SPEED: usize = 10;

const ENABLE_FAST_FORWARD_FEATURE: bool = true;

const WINNER_AGE: usize = 10000;

// COLOURS:
const RESET: &str = "\x1b[0m";
const LIGHT_GREEN: &str = "\x1b[92m";
const GREEN: &str = "\x1b[32m";
const YELLOW: &str = "\x1b[33m";
const LIGHT_YELLOW: &str = "\x1b[93m";
const LIGHT_RED: &str = "\x1b[91m";
const RED: &str = "\x1b[31m";
const GREY: &str = "\x1b[90m";

// These are in order from zero to 100% energy.
const COLOURS: [&str; 7] = [GREY, RED, LIGHT_RED, LIGHT_YELLOW, YELLOW, GREEN, LIGHT_GREEN];

struct PositionAndLight {
    x: usize,
    y: usize,
    source_x: usize,
    source_y: usize,
}

fn light_intensity_to_source(source_x: usize, source_y: usize, x: usize, y: usize) -> f64 {
    let distance = (x as f64 - source_x as f64).hypot(y as f64 - source_y as f64);

    // This is a variation of the inverse square law, adjusted so the first few cells away from the
    // light should be able to sustain an algae.
    1. / (distance + 1.).mul_add(distance + 1., 1.)
}

impl ToNetworkInput<f64, 4> for PositionAndLight {
    fn to_network_input(self) -> [f64; 4] {
        let x_ratio = self.x as f64 / MAP_SIZE as f64;
        let y_ratio = self.y as f64 / MAP_SIZE as f64;
        let source_x_ratio = self.source_x as f64 / MAP_SIZE as f64;
        let source_y_ratio = self.source_y as f64 / MAP_SIZE as f64;

        [x_ratio, y_ratio, source_x_ratio, source_y_ratio]
    }
}

#[derive(Default)]
struct PetriDish {
    light_source_x: usize,
    light_source_y: usize,
    algae: Vec<(Algae, usize, usize)>,
}

impl PetriDish {
    fn algae_count(&self) -> usize {
        self.algae.len()
    }
}

impl Display for PetriDish {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        let max_width = MAP_SIZE + 2; // board + border
        for y in 0..max_width {
            let last_y = y == max_width - 1;
            for x in 0..max_width {
                let last_x = x == max_width - 1;
                match (x, y) {
                    // Corners
                    (0, 0) => {
                        f.write_char('╔')?;
                    }
                    (0, _) if last_y => {
                        f.write_char('╚')?;
                    }
                    (_, 0) if last_x => {
                        f.write_char('╗')?;
                    }
                    (_, _) if last_x && last_y => {
                        f.write_char('╝')?;
                    }
                    // Borders
                    (0, _) => {
                        f.write_char('║')?;
                    }
                    (_, 0) => {
                        f.write_str("═══")?;
                    }
                    (pos_x, _) if pos_x == max_width - 1 => {
                        f.write_char('║')?;
                    }
                    (_, pos_y) if pos_y == max_width - 1 => {
                        f.write_str("═══")?;
                    }
                    (x, y) => {
                        if x - 1 == self.light_source_x && y - 1 == self.light_source_y {
                            f.write_char(' ')?;
                            f.write_str(YELLOW)?;
                            f.write_char('☀')?;
                            f.write_str(RESET)?;
                            f.write_char(' ')?;
                            continue;
                        }

                        if let Some((algae, _, _)) = self
                            .algae
                            .iter()
                            .find(|(_a, pos_x, pos_y)| x - 1 == *pos_x && y - 1 == *pos_y)
                        {
                            let segment_percent = 1.0 / COLOURS.len() as f64;
                            let segments = algae.energy / segment_percent;
                            let colour = COLOURS[(segments.floor() as usize).clamp(0, COLOURS.len() - 1)];

                            f.write_char(' ')?;
                            f.write_str(colour)?;
                            f.write_char('¤')?;
                            f.write_str(RESET)?;
                            f.write_char(' ')?;
                        } else {
                            f.write_str("   ")?;
                        }
                    }
                };

                if x == max_width - 1 {
                    f.write_char('\n')?;
                }
            }
        }

        Ok(())
    }
}

impl Environment for PetriDish {}

#[derive(Clone, Debug)]
struct Algae {
    age: usize,
    // Our little algae has no other properties apart from its "brain" that require genetic encoding,
    // so we can use the recurrent phenotype directly.
    brain: Network<4, 5, f64, Recurrent<f64>>,
    energy: f64,
    genotype: NetworkGenotype<4, 5, f64, RecurrentType>,
}

impl Algae {
    fn random(mut rng: &mut impl Rng) -> Self {
        let random_genotype = NetworkGenotype::random(&mut rng);

        Self {
            age: 0,
            brain: Network::from_genotype(&random_genotype, &(), &mut rng),
            energy: 0.5,
            genotype: random_genotype,
        }
    }
}

#[derive(Copy, Clone)]
enum Intention {
    Nothing,
    Up,
    Right,
    Down,
    Left,
}

impl<'output> FromNetworkOutput<'output, f64, 5> for Intention {
    fn from_network_output(output: &'output [Neuron<f64>; 5]) -> Self {
        let mut biggest_value = 0.;
        let mut biggest_index = 0;
        for (index, neuron) in output.iter().enumerate() {
            if *neuron.output() > biggest_value {
                biggest_index = index;
                biggest_value = *neuron.output();
            }
        }

        match biggest_index {
            0 => Intention::Nothing,
            1 => Intention::Up,
            2 => Intention::Right,
            3 => Intention::Down,
            4 => Intention::Left,
            _ => unreachable!(),
        }
    }
}

fn main() {
    let mut dish = PetriDish::default();
    assert!(
        MAP_SIZE.pow(2) > MIN_ALGAE,
        "MAP_SIZE is too small to fit minimal algae amount"
    );

    let mut rng = thread_rng();
    let mut fast_forward = false;
    let mut iteration = 0;

    let winner_index = loop {
        iteration += 1;
        // replenish with random algae till we're at the minimum amount
        let algae_count = dish.algae_count();
        if algae_count < MIN_ALGAE {
            let missing = MIN_ALGAE - algae_count;

            for _ in 0..missing {
                let random_x = rng.gen_range(0..MAP_SIZE);
                let random_y = rng.gen_range(0..MAP_SIZE);
                dish.algae.push((Algae::random(&mut rng), random_x, random_y));
            }
        }

        if iteration % LIGHT_SOURCE_MOVE_SPEED == 0 {
            // move the light source
            let can_move_up = dish.light_source_y > 0;
            let can_move_down = dish.light_source_y < MAP_SIZE - 1;
            let can_move_right = dish.light_source_x < MAP_SIZE - 1;
            let can_move_left = dish.light_source_x > 0;

            // we always want to move the light source so we have to
            // decide between the possible directions
            let direction = [can_move_up, can_move_down, can_move_right, can_move_left]
                .into_iter()
                .enumerate()
                .filter(|(_, val)| *val)
                .map(|(index, _)| index)
                .choose(&mut rng)
                .expect("there's always at least two directions we can move");
            match direction {
                0 => dish.light_source_y -= 1,
                1 => dish.light_source_y += 1,
                2 => dish.light_source_x += 1,
                3 => dish.light_source_x -= 1,
                _ => unreachable!(),
            }
        }

        // being alive costs energy, so we subtract a minuscule amount
        dish.algae.par_iter_mut().for_each(|(algae, _, _)| {
            algae.age += 1;
            algae.energy -= ITERATION_COST;
        });

        dish.algae.retain(|(a, _, _)| a.energy > 0.0);

        let mut oldest = 0;
        let mut oldest_coords = 0;

        // find out what the algae want to do
        let mut intentions = Vec::new();
        for (index, &mut (ref mut algae, x, y)) in dish.algae.iter_mut().enumerate() {
            algae.brain.set_bias(1.0);
            algae.brain.set_inputs(PositionAndLight {
                x,
                y,
                source_x: dish.light_source_x,
                source_y: dish.light_source_y,
            });
            algae.brain.tick();
            let intention = algae.brain.get_outputs::<Intention>();
            intentions.push((index, intention));

            // while we're iterating all algae, we might as well give them the energy they've gained
            algae.energy = light_intensity_to_source(dish.light_source_x, dish.light_source_y, x, y)
                .mul_add(ENERGY_ABSORPTION_RATE, algae.energy)
                .clamp(0., 1.);

            // and we're also gonna find out which is the oldest survivor
            if algae.age > oldest {
                oldest = algae.age;
                oldest_coords = index;
            }
        }

        // Algae might want to move to the position of an existing algae, even though that existing
        // one may also move. To give each algae an even chance, we shuffle the intentions, so that
        // there is no preference and we don't have algae in the top left of the dish unable to move
        // first.
        intentions.shuffle(&mut rng);

        if oldest < WINNER_AGE * 8 / 10 && !fast_forward && iteration > 500 && ENABLE_FAST_FORWARD_FEATURE {
            fast_forward = true;

            println!("Fast forwarding evolution until we see interesting patterns emerge...")
        }

        if oldest > WINNER_AGE * 8 / 10 {
            fast_forward = false;
            println!("Replaying in real time...")
        }

        if fast_forward {
            if iteration % 100_000 == 0 {
                println!("Oldest algae so far: {oldest} iterations");
            }
        } else {
            // We print all of this in case we have a winner, so we can abort early.
            print!("\x1B[2J\x1B[1;1H");
            println!("{dish}");
            println!("Oldest algae: {oldest} iterations",);

            if oldest >= WINNER_AGE {
                break oldest_coords;
            }

            sleep(ITERATION_WAIT_TIME);
        }

        // move the algae (notice that we didn't do this in the step above as mutating while iterating
        // might have bad consequences, and thanks to the borrow checker is pretty much impossible)
        for (index, intention) in intentions {
            let &mut (ref mut algae, ref mut x, ref mut y) = &mut dish.algae[index];
            match intention {
                Intention::Nothing => continue,
                Intention::Up => {
                    if *y == 0 {
                        // we can't move upwards if we're hitting the edge of the petri dish
                        continue;
                    }

                    *y -= 1;
                    algae.energy -= MOVE_COST;
                }
                Intention::Right => {
                    if *x == MAP_SIZE - 1 {
                        // we can't move right if we're hitting the edge of the petri dish
                        continue;
                    }

                    *x += 1;
                    algae.energy -= MOVE_COST;
                }
                Intention::Down => {
                    if *y == MAP_SIZE - 1 {
                        // we can't move down if we're hitting the edge of the petri dish
                        continue;
                    }

                    *y += 1;
                    algae.energy -= MOVE_COST;
                }
                Intention::Left => {
                    if *x == 0 {
                        // we can't move left if we're hitting the edge of the petri dish
                        continue;
                    }

                    *x -= 1;
                    algae.energy -= MOVE_COST;
                }
            }
        }

        let mut new_algae = Vec::new();

        // the algae has gathered enough energy to divide itself
        for &mut (ref mut algae, x, y) in &mut dish.algae {
            if algae.energy == 1.0 {
                let mut clone = algae.clone();
                clone.energy = 0.5;
                clone.age = 0;
                algae.energy = 0.5;

                let available_activation_functions: Vec<ActivationFunction<f64>> = vec![
                    ActivationFunction::Binary(SummationBinary::new()),
                    ActivationFunction::Logistic(SummationLogistic::new()),
                    ActivationFunction::Gaussian(SummationGaussian::new()),
                    ActivationFunction::Identity(SummationIdentity::new()),
                    ActivationFunction::HyperbolicTangent(SummationHyperbolicTangent::new()),
                    ActivationFunction::RectifiedLinearUnit(SummationRectifiedLinearUnit::new()),
                    ActivationFunction::SoftPlus(SummationSoftPlus::new()),
                    ActivationFunction::LeakyRectifiedLinearUnit(SummationLeakyRectifiedLinearUnit::new()),
                    ActivationFunction::SigmoidLinearUnit(SummationSigmoidLinearUnit::new()),
                    ActivationFunction::Mish(SummationMish::new()),
                ];

                let mutation = Combination::selective()
                    .and(
                        ExpandNetwork::new(0.3)
                            .with_max_genes(5000)
                            .with_activation_functions(available_activation_functions.clone()),
                    )
                    .and(UniformNetwork::new(0.3).with_activation_functions(available_activation_functions))
                    .and(Remove::new(0.3, NonZeroUsize::new(1).unwrap()))
                    .and(Nudge::new(0.3, 1.0))
                    .and(Finetune::new(1.0));

                mutation.mutate(&mut clone.genotype, &mut rng);

                clone.brain = Network::from_genotype(&clone.genotype, &(), &mut rng);

                new_algae.push((clone, x, y));
            }
        }

        // if the last iteration produced new algae, we randomly place them
        for (algae, _parent_x, _parent_y) in new_algae {
            if dish.algae_count() < MAX_ALGAE {
                // we find the closest spot to the original position of the algae this clone
                // was created from using √((x_2 - x_1)^2 + (y_2 - y_1)^2)
                // let mut min_distance = f64::INFINITY;
                // let mut min_slot = (MAP_SIZE / 2, MAP_SIZE / 2);
                //
                // 'rows: for x in 0..MAP_SIZE {
                //     for y in 0..MAP_SIZE {
                //         if dish.algae.iter().any(|(a, pos_x, pos_y)| *pos_x == x && *pos_y == y) {
                //             continue;
                //         }
                //         let distance =
                //             ((x as f64 - parent_x as f64).powi(2) + (y as f64 - parent_y as f64).powi(2)).sqrt();
                //
                //         if distance == 1.0 {
                //             min_slot = (x, y);
                //
                //             break 'rows;
                //         }
                //
                //         if distance < min_distance {
                //             min_slot = (x, y);
                //             min_distance = distance;
                //         }
                //     }
                // }
                //
                // dish.algae.push((algae, min_slot.0, min_slot.1));

                let random_x = rng.gen_range(0..MAP_SIZE);
                let random_y = rng.gen_range(0..MAP_SIZE);
                dish.algae.push((algae, random_x, random_y));
            } else {
                break;
            }
        }
    };

    let (winner, _, _) = dish.algae[winner_index].clone();

    println!("Blueprint of winning algae in Mermaid MD:");
    println!("{}", winner.brain.render_mermaid())
}
