// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::use_debug,
    clippy::print_stdout,
    clippy::cast_precision_loss,
    clippy::arithmetic_side_effects,
    clippy::indexing_slicing,
    clippy::expect_used
)]

use rand::{thread_rng, Rng};

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    operators::{
        mutation::random::single::{nudge::Nudge, uniform::Uniform},
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::{FromGenotype, Phenotype},
    termination::generation::Generation,
};

// The following is an implementation of
// https://en.wikipedia.org/wiki/Knapsack_problem

const AVAILABLE_BOXES: [Box; 5] = [
    Box { value: 4, weight: 12 }, // green
    Box { value: 2, weight: 2 },  // blue
    Box { value: 2, weight: 1 },  // gray
    Box { value: 1, weight: 1 },  // orange
    Box { value: 10, weight: 4 }, // yellow
];

#[derive(Copy, Clone)]
struct Box {
    value: usize,
    weight: usize,
}

struct Knapsack {
    max_weight: usize,
}

impl Environment for Knapsack {}

// We're only allowed to select one of each box, so we store the selection as bools.
#[derive(Copy, Clone)]
struct SingleSelection([bool; 5]);

impl Phenotype for SingleSelection {
    type Genotype = [bool; 5];
}

impl FromGenotype<[bool; 5], Knapsack> for SingleSelection {
    fn from_genotype<RNG>(genotype: &[bool; 5], _environment: &Knapsack, _rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        SingleSelection(*genotype)
    }
}

impl Fitness<Knapsack> for SingleSelection {
    fn fitness(&mut self, environment: &Knapsack) -> f64 {
        let boxes = self
            .0
            .iter()
            .enumerate()
            .filter_map(|(index, selected)| selected.then(|| AVAILABLE_BOXES[index]));

        let (value, weight) = boxes.fold((0, 0), |(sum_value, sum_weight), b| {
            (sum_value + b.value, sum_weight + b.weight)
        });

        if weight > environment.max_weight {
            return 0.0;
        }

        value as f64
    }
}

// If there's multiple of each box available, we store how many per index we want to take.
#[derive(Clone, Debug)]
struct MultiSelection([u8; 5]);

impl Phenotype for MultiSelection {
    type Genotype = [u8; 5];
}

impl FromGenotype<[u8; 5], Knapsack> for MultiSelection {
    fn from_genotype<RNG>(genotype: &[u8; 5], _environment: &Knapsack, _rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        MultiSelection(*genotype)
    }
}

impl Fitness<Knapsack> for MultiSelection {
    fn fitness(&mut self, environment: &Knapsack) -> f64 {
        let mut sum_value = 0;
        let mut sum_weight = 0;

        for (index, &count) in self.0.iter().enumerate() {
            let chosen_box = AVAILABLE_BOXES[index];

            sum_value += chosen_box.value * count as usize;
            sum_weight += chosen_box.weight * count as usize;

            if sum_weight > environment.max_weight {
                return 0.0;
            }
        }

        sum_value as f64
    }
}

// It should be noted, that the above phenotypes are essentially transparent types
// as their genotype is exactly the same.
// This, depending on the problem, may be more optimal as it requires less
// allocation, or none at all. On the other hand, it makes certain problems
// harder to understand.
// In this example, we could've used the same phenotype for both variants
// in the form of `Vec<Box>` but this makes it harder to return it back
// into the genotype, unless we store it in the phenotype.

fn run_single_selection() -> <SingleSelection as Phenotype>::Genotype {
    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(Knapsack{ max_weight: 15 })
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(Uniform::new(0.1))
        // `with_termination` invisibly switches to the algorithm type,
        // allowing us to configure the final details
        .with_termination(Generation::new(100));

    // set up final runtime parameters
    let classic = config
        .with_elitism(1)
        // if 3 of the 5 genes are the same, they're the same species
        .with_speciation_threshold(0.6)
        .with_interspecies_breeding_chance(0.005)
        .with_random_population(100);

    // we let it run until our termination determines we're done
    let result = classic.run::<SingleSelection>();

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty");

    println!(
        "Single-selection - fitness: {}, best member: \"{:?}\"",
        fittest.fitness, &fittest.genotype,
    );

    fittest.genotype
}

fn run_multi_selection() -> <MultiSelection as Phenotype>::Genotype {
    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(Knapsack{ max_weight: 15 })
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(Nudge::new(0.2, 2))
        // Because we only use 5 out of 255 available gene values, it takes considerably longer to
        // find valid solutions. Implementations should prefer to use their own genomic type.
        .with_termination(Generation::new(100));

    // set up final runtime parameters
    let classic = config
        .with_elitism(1)
        // if 3 of the 5 genes are the same, they're the same species
        .with_speciation_threshold(0.6)
        .with_interspecies_breeding_chance(0.005)
        // we seed the population with a valid genotype to speed up the search
        .with_seeded_population(100, [[0,0,0,0,0]]);

    // we let it run until our termination determines we're done
    let result = classic.run::<MultiSelection>();

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty");

    println!(
        "Multi-selection - fitness: {}, best member: \"{:?}\"",
        fittest.fitness, &fittest.genotype,
    );

    fittest.genotype
}

fn main() {
    // The best solution if you're only allowed to select one of each box is to take all except for
    // the first one.
    let single_selection_solution = run_single_selection();
    assert_eq!(single_selection_solution, [false, true, true, true, true]);

    // The best solution if you're allowed to take as many boxes of each type as you want is:
    // three 10-4 (yellow) boxes and three 2-1 (gray) boxes
    let multi_selection_solution = run_multi_selection();
    assert_eq!(multi_selection_solution, [0, 0, 3, 0, 3]);
}
