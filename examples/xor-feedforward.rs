// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::use_debug,
    clippy::print_stdout,
    clippy::expect_used,
    clippy::float_arithmetic,
    clippy::arithmetic_side_effects,
    clippy::too_many_lines,
    incomplete_features,
    unstable_features
)]
#![feature(generic_const_exprs)]

use core::num::NonZeroUsize;

use rand::thread_rng;

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    neural::{
        activation::{
            binary::SummationBinary,
            float::ActivationFunction,
            gaussian::SummationGaussian,
            identity::SummationIdentity,
            linear_unit::{
                SummationLeakyRectifiedLinearUnit, SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
            },
            logistic::SummationLogistic,
            mish::SummationMish,
            softplus::SummationSoftPlus,
            tangent::SummationHyperbolicTangent,
        },
        input::ToNetworkInput,
        network::{feedforward::FeedForward, Network},
        neuron::Neuron,
        output::FromNetworkOutput,
    },
    operators::{
        mutation::{
            add::network::ExpandNetwork,
            combination::Combination,
            random::single::{finetune::Finetune, nudge::Nudge, uniform::network::UniformNetwork},
            remove::Remove,
        },
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::FromGenotype,
    termination::fitness::Fitness as FitnessTermination,
};

struct XOREnvironment;

impl Environment for XOREnvironment {}

impl Fitness<XOREnvironment> for Network<2, 2, f64, FeedForward<f64>> {
    fn fitness(&mut self, _environment: &XOREnvironment) -> f64 {
        let mut error = 0.;

        let inputs = [(false, false), (false, true), (true, false), (true, true)];

        for (a, b) in inputs {
            let expected = a ^ b;
            let input = Input { a, b };
            self.set_inputs(input);
            self.set_bias(1.0);

            self.evaluate();

            let outputs = self.get_outputs::<Output>();

            // As an additional requirement we only accept an answer if the value is above a certain threshold.
            // This is essentially our "confidence"
            if (outputs.is_true - outputs.is_false).abs() > 0.8 {
                let (expected_true, expected_false) = if expected { (1.0, 0.0) } else { (0.0, 1.0) };

                error += (outputs.is_true - expected_true).abs();
                error += (outputs.is_false - expected_false).abs();
            } else {
                error += 100.;
            }
        }

        1. / (error + 1.)
    }
}

// The following are trivial implementations, but they perfectly demonstrate usage of the In- and Output traits.

struct Input {
    a: bool,
    b: bool,
}

impl ToNetworkInput<f64, 2> for Input {
    fn to_network_input(self) -> [f64; 2] {
        let a = if self.a { 1.0 } else { 0.0 };
        let b = if self.b { 1.0 } else { 0.0 };

        [a, b]
    }
}

struct Output {
    // one parameter would've been fine, but more outputs can actually help the network net develop faster
    is_true: f64,
    is_false: f64,
}

impl<'output> FromNetworkOutput<'output, f64, 2> for Output {
    fn from_network_output(output: &'output [Neuron<f64>; 2]) -> Self {
        Self {
            is_true: *output[0].output(),
            is_false: *output[1].output(),
        }
    }
}

fn main() {
    let available_activation_functions: Vec<ActivationFunction<f64>> = vec![
        ActivationFunction::Binary(SummationBinary::new()),
        ActivationFunction::Logistic(SummationLogistic::new()),
        ActivationFunction::Gaussian(SummationGaussian::new()),
        ActivationFunction::Identity(SummationIdentity::new()),
        ActivationFunction::HyperbolicTangent(SummationHyperbolicTangent::new()),
        ActivationFunction::RectifiedLinearUnit(SummationRectifiedLinearUnit::new()),
        ActivationFunction::SoftPlus(SummationSoftPlus::new()),
        ActivationFunction::LeakyRectifiedLinearUnit(SummationLeakyRectifiedLinearUnit::new()),
        ActivationFunction::SigmoidLinearUnit(SummationSigmoidLinearUnit::new()),
        ActivationFunction::Mish(SummationMish::new()),
    ];

    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(XOREnvironment)
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(
            Combination::selective()
                .and(
                    ExpandNetwork::new(0.3)
                        .with_max_genes(500)
                        .with_activation_functions(available_activation_functions.clone()),
                )
                .and(UniformNetwork::new(1.0).with_activation_functions(available_activation_functions))
                .and(Remove::new(0.25, NonZeroUsize::new(1).unwrap()))
                .and(Nudge::new(1.0, 1.0))
                .and(Finetune::new(1.0)),
        )
        .with_termination(FitnessTermination::new(0.99999));

    let mut algo = config
        .with_elitism(1)
        .with_speciation_threshold(0.82)
        .with_interspecies_breeding_chance(0.001)
        .with_random_population(100);

    let result = loop {
        match algo
            .evaluate::<Network<2, 2, f64, FeedForward<f64>>>()
            .check_termination()
        {
            Ok(evaluated) => {
                let iteration = evaluated.statistics().iteration;

                if iteration % 1000 == 0 {
                    let species = evaluated.state().species();

                    let population_count = species.iter().map(|s| s.population.members.len()).sum::<usize>();

                    println!(
                        "generation: {iteration}, population: {population_count}, species: {}",
                        species.len(),
                    );

                    for evaluated_species in species {
                        let fittest = evaluated_species
                            .population
                            .members
                            .iter()
                            .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
                            .expect("iterator can not be empty");

                        let genotype = &fittest.genotype;
                        let fittest_network = Network::from_genotype(genotype, &(), &mut thread_rng());

                        let layers = fittest_network.get_layers();

                        let hidden: usize = layers.iter().map(|l| l.neurons().len()).sum();
                        let connections = layers.iter().map(|l| l.connections().len()).sum::<usize>()
                            + fittest_network.get_output_connections().len();

                        println!(
                            "\tSpecies: {}-{}, members: {}, fitness: {}, best member: \"L:{} - H:{} - C:{} - I:{}\"",
                            evaluated_species.identifier.0,
                            evaluated_species.identifier.1,
                            evaluated_species.population.members.len(),
                            fittest.fitness,
                            layers.len(),
                            hidden,
                            connections,
                            genotype.instructions().len()
                        );
                    }
                }

                algo = evaluated.select().recombine().mutate().reinsert();
            }
            Err(terminated) => break terminated,
        }
    };

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty")
        .clone();

    let genotype = fittest.genotype;
    let mut fittest_network = Network::from_genotype(&genotype, &(), &mut thread_rng());

    let layers = fittest_network.get_layers();
    let hidden: usize = layers.iter().map(|l| l.neurons().len()).sum();
    let connections =
        layers.iter().map(|l| l.connections().len()).sum::<usize>() + fittest_network.get_output_connections().len();

    println!(
        "\nGeneration: {}, fitness: {}, best member: \"L:{} - H:{} - C:{} - I:{}\"",
        result.statistics().iteration,
        fittest.fitness,
        layers.len(),
        hidden,
        connections,
        genotype.instructions().len()
    );

    fittest_network.reset();

    println!("Genotype: {genotype:?}");
    println!("Network: {fittest_network:?}");

    let inputs = [(false, false), (false, true), (true, false), (true, true)];

    for (a, b) in inputs {
        fittest_network.set_inputs(Input { a, b });
        fittest_network.set_bias(1.);
        print!("{a} ^ {b}: ");

        fittest_network.evaluate();

        let outputs = fittest_network.get_outputs::<Output>();
        println!(
            "true: {}, false: {}, (expected: {})",
            outputs.is_true,
            outputs.is_false,
            a ^ b
        );
    }

    println!("Rendered MD Mermaid:");
    println!("{}\n", fittest_network.render_mermaid());
}
