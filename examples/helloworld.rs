// For the sake of readability, we allow all these lints up here instead of in the code below.
#![allow(
    clippy::print_stdout,
    clippy::cast_precision_loss,
    clippy::float_arithmetic,
    clippy::arithmetic_side_effects,
    clippy::indexing_slicing,
    clippy::expect_used,
    incomplete_features,
    unstable_features
)]
#![feature(generic_const_exprs)]

use rand::{thread_rng, Rng};

use meiosis::{
    algorithm::fitness::Configuration,
    environment::Environment,
    fitness::Fitness,
    operators::{
        mutation::{add::Add, combination::Combination, random::multi::nudge::Nudge, remove::Remove, swap::Swap},
        recombination::single_point::SinglePoint,
        selection::fitness_proportionate::StochasticAcceptance,
    },
    phenotype::{FromGenotype, Phenotype},
    termination::fitness::Fitness as FitnessTermination,
};

const TARGET: &str = "Hello, World!";
//const TARGET: &str = "Hello, I'm apparently very slow at developing!";

#[derive(Debug, Clone, PartialEq)]
struct EvolvedString(Vec<u8>);

impl Phenotype for EvolvedString {
    type Genotype = Vec<u8>;
}

struct EvolveHelloWorld {
    target: Vec<u8>,
}

impl FromGenotype<Vec<u8>, EvolveHelloWorld> for EvolvedString {
    fn from_genotype<RNG>(genotype: &Vec<u8>, _environment: &EvolveHelloWorld, _rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        EvolvedString(genotype.clone())
    }
}

impl Environment for EvolveHelloWorld {}

/// Define how well [`EvolvedString`] does in an [`EvolveHelloWorld`] environment.
impl Fitness<EvolveHelloWorld> for EvolvedString {
    fn fitness(&mut self, environment: &EvolveHelloWorld) -> f64 {
        let chars = &self.0;
        let target = &environment.target;

        let correct = chars
            .iter()
            .zip(target)
            .map(|(c, t)| c == t)
            .filter(|result| *result)
            .count();

        correct as f64 / self.0.len().max(environment.target.len()) as f64
    }
}

fn main() {
    // note that all chances are set to 1 because we selectively do only one mutation per mutation
    let mutation = Combination::selective()
        .and(Add::new(1.0, None))
        .and(Remove::new(1.0, 5.try_into().unwrap()))
        .and(Swap::new(1.0))
        .and(Nudge::new(0.3, 10));

    let config = Configuration::new()
        .with_rng(thread_rng())
        .with_environment(EvolveHelloWorld {
            target: TARGET.as_bytes().to_vec(),
        })
        .with_selection(StochasticAcceptance::default())
        .with_recombination(SinglePoint::new())
        .with_mutation(mutation)
        // `with_termination` invisibly switches to the algorithm type,
        // allowing us to configure the final details
        .with_termination(FitnessTermination::new(1.));

    // set up final runtime parameters
    let mut classic = config
        .with_elitism(2)
        .with_speciation_threshold(0.999)
        .with_interspecies_breeding_chance(0.005)
        .with_random_population(100);

    // we're manually iterating the algorithm so we can print out statistics
    let result = loop {
        match classic.evaluate::<EvolvedString>().check_termination() {
            Ok(new_evaluated) => {
                let iteration = new_evaluated.statistics().iteration;

                if iteration % 100 == 0 {
                    let species = new_evaluated.state().species();

                    let population_count = species.iter().map(|s| s.population.members.len()).sum::<usize>();

                    println!(
                        "generation: {iteration}, population: {population_count}, species: {}",
                        species.len(),
                    );

                    for evaluated_species in species {
                        let fittest = evaluated_species
                            .population
                            .members
                            .iter()
                            .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
                            .expect("iterator can not be empty");

                        println!(
                            "\tSpecies: {}-{}, members: {}, fitness: {}, best member: \"{}\"",
                            evaluated_species.identifier.0,
                            evaluated_species.identifier.1,
                            evaluated_species.population.members.len(),
                            fittest.fitness,
                            String::from_utf8_lossy(&fittest.genotype),
                        );
                    }
                }

                // we evaluated above, so now we need to do the rest of the algorithm, otherwise
                // Rust complains as we're in the wrong state of the state machine
                classic = new_evaluated.select().recombine().mutate().reinsert();
            }
            Err(terminated) => {
                // a termination condition triggered here, so we may want to check the result
                break terminated;
            }
        };
    };

    let species = result.state().species();
    let fittest = species
        .iter()
        .flat_map(|s| &s.population.members)
        .max_by(|a, b| a.fitness.total_cmp(&b.fitness))
        .expect("iterator can not be empty");

    println!(
        "generation: {}, fitness: {}, best member: \"{}\"",
        result.statistics().iteration,
        fittest.fitness,
        String::from_utf8_lossy(&fittest.genotype),
    );
}
