# `Meiosis`

[![Docs][docs-badge]][docs-url]
[![Crates.io][crates-badge]][crates-url]
[![License][license-badge]][license-url]
[![Deps.rs][deps-badge]][deps-url]
[![Build Status][build-badge]][build-url]
[![Code Coverage][coverage-badge]][coverage-url]
[![Contributors][contributors-badge]][contributors-url]


[docs-badge]: https://docs.rs/meiosis/badge.svg
[docs-url]: https://docs.rs/meiosis
[crates-badge]: https://img.shields.io/crates/v/meiosis.svg
[crates-url]: https://crates.io/crates/meiosis
[license-badge]: https://img.shields.io/crates/l/meiosis
[license-url]: https://gitlab.com/bit-refined/meiosis/-/blob/master/LICENSE.md
[deps-badge]: https://deps.rs/repo/gitlab/bit-refined/meiosis/status.svg
[deps-url]: https://deps.rs/repo/gitlab/bit-refined/meiosis
[build-badge]: https://gitlab.com/bit-refined/meiosis/badges/master/pipeline.svg
[build-url]: https://gitlab.com/bit-refined/meiosis/commits/master
[coverage-badge]: https://gitlab.com/bit-refined/meiosis/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/bit-refined/meiosis/commits/master
[contributors-badge]: https://img.shields.io/gitlab/contributors/bit-refined/meiosis
[contributors-url]: https://gitlab.com/bit-refined/meiosis/-/graphs/master

# Overview
## Current Status
At the moment this crate is still in its early stages. There's a nightly requirement
for features, very unstable APIs and nothing should be considered unchanging.
I don't even know what to put in this README yet. It should also be noted that I use
this library to learn API design and dive deeper into evolutionary algorithms - in
the future I also want to tackle neuroevolution.

## Goals
This library arose from the fact, that a lot of libraries out there do not leverage
compile time evaluated invariants leading to the underlying code constantly verifying
those iteration over iteration, resulting in ever so slightly less performance.

Because evolutionary algorithms take quite a long time to evolve, it is very annoying
if there's a bug or runtime error. To combat that, algorithms in this library are
written in such a way that most if not all components are evaluated at compile time.
If that should not be possible, there will at least be assertions, so that any problem
should come up very early, and not possibly hours into the evolution.

Methods to reach compile time evaluation consist of using state machines, arrays
where possible and setting configuration options before entering main loops, preferably
static, so that any branches can be eliminated upon compilation, as the compiler
should hopefully notice that those values can not change anymore once set.

### Components
Components in this library are written in such a way, that they *should* be plug-and-playable
with all available algorithms that use them. Generic parameters together with these components (e.g. selection, mutation)
should result in the Rust compiler being able to infer most if not all types.

Of course, should you decide to write your own algorithm, these can still be used freely.
If there is a requirement for you, like a trait signature change, to make things work, do not
hesitate to open an issue! I would love to see more use-cases that I may not have thought about
when designing the API.

### Algorithms
Algorithms provided in this library have been developed mostly without an associated paper
describing how they should work. My main goal was to understand their functionality and then
to implement it in a rather naive way. The result is code that should hopefully be easy to read
and understand.



# Notes
## Special Thanks
I'm terrible at thinking of names for my projects, so thank you, [Patrick Harden](https://github.com/PatrickHarden)
for coming up with `Meiosis`.

The name of this crate was originally registered by [Tim Byrne](https://github.com/TheLocehiliosan) on crates.io.
Thank you so much for transferring ownership!

Originally met through the [Saltwater](https://github.com/jyn514/saltwater) project, I would like to
thank [Andrei](https://github.com/xTachyon) and [Julia](https://github.com/pythondude325) for their continued support in the prototyping phase. They were my rubber ducks and sample size
of two for all things "what would users of the library like".

A ton of inspiration was taken from the following crates on how to solve certain problems, be it algorithms,
library layout or examples. A big thank you to the authors and contributors of these crates:
- [genevo](https://crates.io/crates/genevo)
- [oxineat](https://crates.io/crates/oxineat)
- [radiate](https://crates.io/crates/radiate)

If you feel like I forgot to mention you, please leave me a message or open a (confidential) issue.
Any inspiration and help I received is equally as important as the code itself.
