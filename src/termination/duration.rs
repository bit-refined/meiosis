use super::{Statistics, Termination};
use crate::species::EvaluatedSpecies;

/// MISSING DOCS
struct Duration(core::time::Duration);

impl<PHENOTYPE> Termination<PHENOTYPE> for Duration {
    #[allow(clippy::todo)]
    fn terminate(&mut self, _statistics: &Statistics, _species: &[EvaluatedSpecies<PHENOTYPE>]) -> bool {
        todo!()
    }
}
