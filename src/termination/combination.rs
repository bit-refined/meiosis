#![allow(box_pointers)]

use core::marker::PhantomData;

use crate::{algorithm::fitness::Statistics, species::EvaluatedSpecies};

use super::Termination;

/// All stored termination strategies have to return `true` to terminate.
struct All;

/// Only one of all stored termination strategies has to return `true` to terminate.
struct Any;

/// MISSING DOCS
#[allow(missing_debug_implementations)] // We can't do so because we can not guarantee `Termination` to implement it.
pub struct Combination<S, PHENOTYPE, const STRATEGIES: usize> {
    /// MISSING DOCS
    strategy: PhantomData<S>,
    //phenotype: PhantomData<PHENOTYPE>,
    /// MISSING DOCS
    strategies: [Box<dyn Termination<PHENOTYPE>>; STRATEGIES],
}

impl<PHENOTYPE> Combination<All, PHENOTYPE, 0> {
    /// MISSING DOCS
    #[must_use]
    pub fn all() -> Combination<All, PHENOTYPE, 0> {
        Combination {
            strategy: PhantomData::<All>,
            //phenotype: PhantomData::<ALGO>,
            strategies: [],
        }
    }
}

impl<ALGO> Combination<Any, ALGO, 0> {
    /// MISSING DOCS
    #[must_use]
    pub fn any() -> Combination<Any, ALGO, 0> {
        Combination {
            strategy: PhantomData::<Any>,
            strategies: [],
        }
    }
}

impl<S, ALGO, const STRATEGIES: usize> Combination<S, ALGO, STRATEGIES> {
    /// MISSING DOCS
    #[allow(clippy::expect_used, clippy::missing_panics_doc)]
    pub fn and<T>(self, termination: T) -> Combination<S, ALGO, { STRATEGIES + 1 }>
    where
        T: Termination<ALGO> + 'static,
    {
        let boxed: Box<dyn Termination<ALGO>> = Box::new(termination);

        // construct an iterator from existing elements and a single new one (`Option` implements `Iterator`)
        let mut iter = self.strategies.into_iter().chain(Some(boxed));
        // create array with iterator results
        // this can't fail as lengths are known exactly and always match.
        // `from_fn` can infer the array length from the return type
        let strategies = core::array::from_fn(|_| iter.next().expect("constructed iterator has known length"));

        Combination {
            strategy: self.strategy,
            strategies,
        }
    }
}

impl<PHENOTYPE, const STRATEGIES: usize> Termination<PHENOTYPE> for Combination<All, PHENOTYPE, STRATEGIES> {
    fn terminate(&mut self, statistics: &Statistics, species: &[EvaluatedSpecies<PHENOTYPE>]) -> bool {
        self.strategies.iter_mut().all(|t| t.terminate(statistics, species))
    }
}

impl<PHENOTYPE, const STRATEGIES: usize> Termination<PHENOTYPE> for Combination<Any, PHENOTYPE, STRATEGIES> {
    fn terminate(&mut self, statistics: &Statistics, species: &[EvaluatedSpecies<PHENOTYPE>]) -> bool {
        self.strategies.iter_mut().any(|t| t.terminate(statistics, species))
    }
}
