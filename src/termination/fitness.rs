use crate::species::EvaluatedSpecies;

use super::{Statistics, Termination};

/// MISSING DOCS
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Fitness(f64);

impl Fitness {
    /// MISSING DOCS
    /// # Panics
    /// Panics if given fitness is not finite or negative.
    #[must_use]
    pub fn new(fitness: f64) -> Self {
        assert!(fitness.is_finite(), "fitness should be of finite value");
        assert!(fitness.is_sign_positive(), "fitness should be of positive value");

        Self(fitness)
    }
}

impl<PHENOTYPE> Termination<PHENOTYPE> for Fitness {
    fn terminate(&mut self, _statistics: &Statistics, species: &[EvaluatedSpecies<PHENOTYPE>]) -> bool {
        species
            .iter()
            .flat_map(|s| &s.population.members)
            .any(|m| m.fitness >= self.0)
    }
}
