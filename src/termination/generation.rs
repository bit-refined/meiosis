use core::num::NonZeroUsize;

use crate::species::EvaluatedSpecies;

use super::{Statistics, Termination};

/// MISSING DOCS
#[derive(Copy, Clone, Debug)]
pub struct Generation(NonZeroUsize);

impl Generation {
    /// MISSING DOCS
    /// # Panics
    /// Panics if the given generation is zero.
    #[must_use]
    #[allow(clippy::expect_used)]
    pub fn new(generation: usize) -> Self {
        Self(generation.try_into().expect("generation should be greater than zero"))
    }
}

impl<PHENOTYPE> Termination<PHENOTYPE> for Generation {
    fn terminate(&mut self, statistics: &Statistics, _species: &[EvaluatedSpecies<PHENOTYPE>]) -> bool {
        statistics.iteration >= self.0.get()
    }
}
