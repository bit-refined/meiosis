use crate::{algorithm::fitness::Statistics, species::EvaluatedSpecies};

/// MISSING DOCS
pub mod combination;
/// MISSING DOCS
mod duration;
/// MISSING DOCS
pub mod fitness;
/// MISSING DOCS
pub mod generation;

/// A termination determines whether the running algorithm has reached a state that indicates
/// that it is done or should be aborted. The most used termination conditions like generation limits
/// or fitness targets are already implemented and ready to be used for genetic algorithms included
/// in this crate.
pub trait Termination<GENOTYPE> {
    /// This function determines whether a termination condition has been reached.
    fn terminate(&mut self, statistics: &Statistics, species: &[EvaluatedSpecies<GENOTYPE>]) -> bool;

    /// Should the termination strategy mutate an internal state we may want to reset it.
    fn reset(&mut self) {}
}
