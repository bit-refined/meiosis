use crate::random::Random;
use rand::Rng;

/// A population is a set amount of [`crate::genotype::Genotype`]s that are being evaluated, selected, recombined and
/// mutated, to then produce a new population.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub struct Population<GENOTYPE> {
    /// Members are stored in an arbitrary order.
    pub members: Vec<GENOTYPE>,
}

impl<GENOTYPE> Population<GENOTYPE>
where
    GENOTYPE: Random,
{
    /// Creates a new population using the provided random number generator to create random genotypes
    /// until it has reached the defined size.
    pub fn random<RNG>(size: usize, mut rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        let members = (0..size).map(|_| GENOTYPE::random(&mut rng)).collect();
        Population { members }
    }
}

impl<GENOTYPE> Population<GENOTYPE> {
    /// Creates a new population using the provided seeds until the population has reached the
    /// provided size. If the iterator is too short, we cycle through it again until we have enough
    /// elements to fill the population with the desired size. If the iterator is longer than is
    /// required to reach the given population size, elements at the end will be cut off and ignored.
    ///
    /// If random distribution is intended, the iterator should be shuffled beforehand.
    ///
    /// If an exact sized population should be created with exactly as many members as there are items
    /// in the iterator [`Population::from`] should be used instead.
    pub fn seeded<ITER>(size: usize, seeds: ITER) -> Self
    where
        ITER: IntoIterator<Item = GENOTYPE>,
        <ITER as IntoIterator>::IntoIter: Clone,
    {
        let iter = seeds.into_iter().cycle().take(size);
        Population {
            members: iter.collect(),
        }
    }
}

impl<GENOTYPE, ITER> From<ITER> for Population<GENOTYPE>
where
    ITER: IntoIterator<Item = GENOTYPE>,
{
    fn from(value: ITER) -> Self {
        Population {
            members: value.into_iter().collect(),
        }
    }
}

/// [`crate::genotype::Genotype`]s have been converted into [`crate::phenotype::Phenotype`]s and been evaluated.
#[derive(Debug, Clone, PartialEq)]
#[allow(clippy::module_name_repetitions)]
#[non_exhaustive]
pub struct EvaluatedPopulation<GENOTYPE> {
    /// Evaluated members are stored in an arbitrary order.
    pub members: Vec<Evaluated<GENOTYPE>>,
}

/// MISSING DOCS
#[derive(Debug, Clone, PartialEq)]
#[non_exhaustive]
pub struct Evaluated<GENOTYPE> {
    /// MISSING DOCS
    pub genotype: GENOTYPE,
    /// MISSING DOCS
    pub fitness: f64,
    /// MISSING DOCS
    /// # Note
    /// If speciation is turned off, this value is not set and ignored.
    pub adjusted_fitness: f64,
}

/// MISSING DOCS
#[derive(Debug)]
#[non_exhaustive]
pub struct Species<GENOTYPE> {
    /// MISSING DOCS
    pub identifier: (usize, usize),
    /// MISSING DOCS
    pub representative: GENOTYPE,
    /// MISSING DOCS
    pub population: Population<GENOTYPE>,
}

/// MISSING DOCS
#[derive(Debug, PartialEq)]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)] // TODO: fix this
pub struct EvaluatedSpecies<GENOTYPE> {
    /// MISSING DOCS
    pub identifier: (usize, usize),
    /// MISSING DOCS
    pub sum_adjusted_fitness: f64,
    /// MISSING DOCS
    pub population: EvaluatedPopulation<GENOTYPE>,
}

/// MISSING DOCS
#[derive(Debug)]
#[non_exhaustive]
pub struct Parents<GENOTYPE> {
    /// MISSING DOCS
    pub identifier: (usize, usize),
    /// MISSING DOCS
    pub max_size: usize,
    /// MISSING DOCS
    pub representative: GENOTYPE,
    /// MISSING DOCS
    pub population: EvaluatedPopulation<GENOTYPE>,
}

/// MISSING DOCS
#[derive(Debug)]
#[non_exhaustive]
pub struct Bare<GENOTYPE> {
    /// MISSING DOCS
    pub identifier: (usize, usize),
    /// MISSING DOCS
    pub max_size: usize,
    /// MISSING DOCS
    pub representative: GENOTYPE,
}
