/// Mutation changes genes in a genotype.
pub mod mutation;
/// Recombination is used to create novell solutions from parents, allowing offspring to potentially
/// inherit more and more positive traits.
pub mod recombination;
/// Selection is used to decide which solutions get a chance to recombine. This is usually depending
/// on a previous evaluation to rank solutions.
pub mod selection;
