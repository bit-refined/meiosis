use core::cmp::Ordering;

use crate::{environment::Environment, phenotype::Phenotype};

/// MISSING DOCS
mod floats;

/// Fitness of [Phenotype] in an [Environment].
pub trait Fitness<ENVIRONMENT>: Phenotype
where
    ENVIRONMENT: Environment,
{
    /// This function should return how well a [Phenotype] does in the environment.
    fn fitness(&mut self, environment: &ENVIRONMENT) -> f64;
}

/// Usually just requiring [`Ord`] would be possible, however there are cases of types for which
/// this is impossible, so we use this trait to allow those kinds of types to also be compared.
///
/// If this trait is being implemented on a type that may produce invalid values (like `-Inf` on [`f64`]),
/// those values should always be marked as [`core::cmp::Ordering::Less`], so they have the highest
/// chance of being ignored by any selection strategy.
// TODO: future trait for generic fitness values
trait FitnessValue {
    /// MISSING DOCS
    fn compare(&self, rhs: &Self) -> Ordering;
}
