#![allow(incomplete_features, unstable_features)]
#![feature(generic_const_exprs)]
#![feature(array_methods)]
#![feature(hash_extract_if)]
#![feature(trait_upcasting)]
#![feature(float_next_up_down)]

//! # Experimental!
//! This library requires nightly as it is using the `generic_const_exprs` feature.
//!
//! # Overview
//! In general, the API of this library is still very much in development, but should be ready
//! for experimental use.
//!
//! # Development using `meiosis`
//! Because this library heavily relies on type inference, development may sometimes trigger `rustc`
//! to suggest adding type annotations on some types. Ideally, this should not be necessary. It is
//! recommended to keep writing code and using it, till the compiler can figure out how all of the
//! genetic algorithm components fit together. Finally, once everything is in place, all of these
//! abstractions should have absolutely zero cost, as there is only one final implementation that
//! will run, which upholds all trait requirements and type invariants. In theory, having more
//! invariants known and checked for at compile time, we should end up with more efficient code.
//!
//! # Goals and Guarantees
//! ## Invalid States and Errors
//! When setting up an algorithm and for example choosing what `Selection` and `Recombination`
//! strategies to use, most if not all variables are known at compile time. This library tries to
//! leverage that as much as possible by using generics and traits that will only compile if the
//! basics are implemented and some sizes are given (like population size).
//!
//! Should there be a variable that can only be known at runtime but was provided by the user, it
//! will be checked thanks to descriptive `assert!`s all over the code base. Over time, and with
//! each new version, a goal of this library is to replace runtime variables more and more by generic
//! replacements. Because of the nature of genetic algorithms it is imperative that `const`
//! environments are leveraged as much as possible to reduce runtime invariants, binary size, and
//! execution time.
//!
//! ## Style and Target Audience
//! `meiosis` does not make assumptions about the knowledge of the user, which means that the code will
//! purposefully "look naive" in most places and it'll have comments for any line of code that
//! requires advanced knowledge about *why* it does what it does.
//!
//! In addition, there shall be no missing documentation, including on private items or trait
//! implementations, strictly enforced by clippy.
//! Because the topic of this library is so interwoven with biology any terminology used will also list
//! aliases to make it easier for beginners to search through, no matter what field they come from.
//!
//! If you have trouble to find something or have a hard time to understand a bit of code
//! or how to use certain components, please do not hesitate to ask in an issue.
//! You may also open a *confident* issue, should you want to include bits and pieces of production code to
//! make issues reproducible.
//!
//! ## Mistakes
//! This library is a product of a personal interest in the field of genetic algorithms and API design,
//! so it might end up containing a few mistakes or bad phrasing. Please, if you are knowledgeable in
//! this field or found an obvious error, do let me know in an issue!
//!
//! I want this library to be an example for Rust beginners to learn from, and I try to maintain
//! a high quality standard. If you, dear reader, have ideas, improvements or wish for a feature,
//! please do not hesitate to open an issue. I would love to hear how people use this library and
//! what I can do to ease their pain points with it.

/// This module contains algorithms composed using the remaining parts of this library.
pub mod algorithm;
/// The environment module contains everything necessary to evaluate and simulate agents in.
pub mod environment;
/// Fitness is used in non-continuous evaluations to rank solutions.
pub mod fitness;
/// Genes describe the smallest component evolutionary algorithms manipulate and compose to make up solutions.
pub mod gene;
/// Using several genes, genotypes describe the genetic blueprint to solutions.
pub mod genotype;
/// MISSING DOCS
pub mod multithreading;
/// Sometimes generating random genes is too radical, so slightly nudging them is preferable.
/// To do so, we need this module and contained traits to describe how to manipulate genes.
pub mod neighbour;
/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod neural;
/// Operators are the main component of every genetic algorithm, describing how to select, combine
/// and mutate genotypes, driving evolution.
pub mod operators;
/// Using a genetic blueprint and an environment in which these get created, phenotypes are solutions
/// to an optimization problem.
pub mod phenotype;
/// Several components in this library require random values, for example for genes and genotypes.
pub mod random;
/// Traits required for the serialization feature. Due to the way this is achieved, the module is also
/// available without it enabled.
mod serialization;
/// This module contains everything required to describe a pool of solutions and how they relate to
/// each other if grouped in species.
pub mod species;
/// Algorithms should terminate at some point and this module contains traits and implementations
/// to achieve that.
pub mod termination;
