use crate::gene::Gene;

/// [`Genotype`] and related implementations for arrays.
mod array;
/// [`Genotype`] and related implementations for neural networks.
#[cfg(feature = "neural_networks")]
pub mod network;
/// [`Genotype`] and related implementations for [`::alloc::string`].
mod string;
/// [`Genotype`] and related implementations for [`::alloc::vec`].
mod vector;

/// This trait describes the genetic construct holding `Gene`s.
pub trait Genotype {
    /// MISSING DOCS
    type Gene: Gene;
}

/// The distance or genomic difference between [`Genotype`]s is used in some algorithms
/// for multi-object optimization, solved using speciation.
pub trait Distance: Genotype {
    /// This function returns the genetic distance of two [`Genotype`]s so that
    /// they can be sorted into species in a process called "speciation".
    ///
    /// The distance is defined as how different they are, so that completely different
    /// [`Genotype`]s are 100% apart, so they have a distance of `1.0`.
    /// This normalization is necessary as genome size needs to be irrelevant.
    ///
    /// Similar, two exactly identical genomes need to have a distance of `0.0`.
    ///
    /// # Note
    /// The return type is a 32 bit float because those generally compute faster.
    /// We require speed as this function will be called a lot for speciation.
    fn distance(&self, other: &Self) -> f32;
}

// TODO: implement this for all sorts of `std` ordered collections!
