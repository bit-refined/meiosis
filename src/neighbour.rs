use rand::Rng;

/// Some mutations may not want to change single genes arbitrarily and instead only "nudge" them
/// in a direction. For this, we need to know what a predecessor or successor of a gene is.
///
/// # Example that *can* implement this trait
/// Simple. Every integer. We can go up and down the "number line" and look at the neighbours
/// of any integer. Some genetic algorithms may rely on only tweaking genes ever so slightly,
/// which means using a strategy that uses this trait would be perfect for such a use case.
///
/// # Example that *can't* implement this trait
/// For example real DNA has four possible values: A, T, G, C. These don't really have an order
/// and in terms of mutation they're all equally distanced from each other, by one.
/// One *could* implement this trait for them and use mutation strategies that rely on this trait,
/// however they would not be any different from simply using the `Random` mutation, which
/// just picks a random one out of the four.
pub trait Neighbour: Sized {
    /// This method acts as a more convenient way to get a potential successor.
    /// If the type this is implemented for has a potential faster way to generate this it can
    /// act as a shortcut and potential optimization.
    ///
    /// Note that this function still requires [Rng] in case a value has multiple values with the
    /// same distance of 1, making it possible to randomly choose one.
    fn successor<RNG>(&self, rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng;

    /// This method acts as a more convenient way to get a potential predecessor.
    /// If the type this is implemented for has a potential faster way to generate this it can
    /// act as a shortcut and potential optimization.
    ///
    /// Note that this function still requires [Rng] in case a value has multiple values with the
    /// same distance of 1, making it possible to randomly choose one.
    fn predecessor<RNG>(&self, rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng;
}

/// Similar to [`Neighbour`] this trait allows "jumping the line". It may be impossible to exactly
/// define neighbours, but using this trait one can implement a maximum range that a value can be
/// nudged in.
#[allow(clippy::module_name_repetitions)]
pub trait DistancedNeighbour<D = usize>: Sized {
    /// This method should return a random value that is no further "away" than `maximal_distance`
    /// in the "forward" direction. If no such value is available, it returns `None`.
    ///
    /// # Example
    /// If the value is `4` and we execute this method with a maximal distance of `3`,
    /// it should return any value in the range of `5..=7`.
    fn random_forward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &D) -> Option<Self>
    where
        RNG: Rng;

    /// This method should return a random value that is no further "away" than `maximal_distance`
    /// in the "backward" direction. If no such value is available, it returns `None`.
    ///
    /// # Example
    /// If the value is `4` and we execute this method with a maximal distance of `3`,
    /// it should return any value in the range of `1..=3`.
    fn random_backward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &D) -> Option<Self>
    where
        RNG: Rng;
}
