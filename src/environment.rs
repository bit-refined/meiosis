/// The environment is used to determine fitness of [`crate::genotype::Genotype`]s.
/// In practical terms this means for fitness-based algorithms it usually contains the training data,
/// where as in opportunity-based algorithms it can be seen as the virtual world they live in.
pub trait Environment {
    /// In fitness-driven genetic algorithms the environment may change to prevent overfitting.
    ///
    /// In an opportunity-driven genetic algorithm the environment can be changed by the population,
    /// but may itself change over time to create higher evolutionary pressure.
    fn update(&mut self, _iteration: u64) {
        // by default the environment does not change
    }
}

/// If there's no environment needed, this implementation ensures that we always have an empty environment available.
impl Environment for () {}
