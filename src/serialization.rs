#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

#[cfg(not(feature = "serialization"))]
/// This marker trait helps implementing serialization without having to duplicate code.
/// Without it traits, structs and implementations using `where` would require duplication.
/// Serialization feature is disabled, resulting in this trait not having any super traits.
pub trait MaybeSerde {}

#[cfg(not(feature = "serialization"))]
impl<T> MaybeSerde for T {}

#[cfg(feature = "serialization")]
/// This marker trait helps implementing serialization without having to duplicate code.
/// Without it traits, structs and implementations using `where` would require duplication.
/// Serialization feature is enabled, resulting in this trait now requiring `Serialize` and `Deserialize`.
pub trait MaybeSerde: Serialize + for<'de> Deserialize<'de> {}

#[cfg(feature = "serialization")]
impl<T> MaybeSerde for T where T: Serialize + for<'de> Deserialize<'de> {}
