use rand::Rng;

use crate::{
    environment::Environment,
    gene::network::feedforward::Instruction,
    genotype::network::{Feedforward as FeedforwardGenotype, Network as NetworkGenotype},
    neural::{
        network::{
            feedforward::{Connection, FeedForward},
            Network,
        },
        neuron::Neuron,
        value::Value,
    },
    phenotype::{FromGenotype, Phenotype},
};

impl<T, const IN: usize, const OUT: usize> Phenotype for Network<IN, OUT, T, FeedForward<T>>
where
    T: Value,
{
    type Genotype = NetworkGenotype<IN, OUT, T, FeedforwardGenotype>;
}

/// MISSING DOCS
struct LastConnection {
    /// MISSING DOCS
    layer: Option<usize>,
    /// MISSING DOCS
    connection: Option<Connection>,
}

impl<T, const IN: usize, const OUT: usize, ENV> FromGenotype<<Self as Phenotype>::Genotype, ENV>
    for Network<IN, OUT, T, FeedForward<T>>
where
    ENV: Environment,
    T: Value,
{
    #[allow(clippy::cognitive_complexity, clippy::too_many_lines)]
    fn from_genotype<RNG>(
        genotype: &NetworkGenotype<IN, OUT, T, FeedforwardGenotype>,
        _environment: &ENV,
        _rng: &mut RNG,
    ) -> Self
    where
        RNG: Rng,
    {
        let mut network = Network::new().feedforward();
        network.set_outputs(genotype.outputs.clone());

        // This algorithm uses two variable and one static layer:
        // Origin: This is the layer coming before the selected layer. Initially this is the input.
        // Layer: This is the selected layer. Initially there is none.
        // Output: This is always the outputs.

        let mut origin_cursor: usize = 0;
        let mut layer_cursor: Option<usize> = None;
        let mut output_cursor: usize = 0;
        let mut last_connection = LastConnection {
            layer: None,
            connection: None,
        };

        for instruction in genotype.instructions() {
            match *instruction {
                Instruction::Neuron => {
                    let layers = network.get_layers().len();

                    if layers > 0 {
                        // This arithmetic is safe because of the condition.
                        #[allow(clippy::arithmetic_side_effects)]
                        network.add_hidden(layers - 1, Neuron::default());
                    }
                }
                Instruction::LayerActivationFunction(ref activation_function) =>
                {
                    #[allow(clippy::expect_used)]
                    if let Some(neuron) = layer_cursor {
                        let layer = network
                            .get_layers()
                            .len()
                            .checked_sub(1)
                            .expect("network should contain layers if layer cursor is Some");

                        let hidden = network.get_hidden_mut(layer, neuron);

                        hidden.activation_function = activation_function.clone();
                    }
                }
                Instruction::OutputActivationFunction(ref activation_function) => {
                    network.set_output_activation_function(output_cursor, activation_function.clone());
                }
                Instruction::NextOrigin => {
                    let layers = network.get_layers().len();

                    // The subtraction of -1 is always safe. For the first path because the constructor guarantees at least one input,
                    // and for the second path because adding a layer adds at least one neuron.
                    // We also know that there's at least two layers because of the condition.
                    #[allow(clippy::arithmetic_side_effects)]
                    // While there's no or only a single layer, our origin is the input layer.
                    let max = if layers <= 1 {
                        IN - 1
                    } else {
                        network.get_layer(layers - 2).neurons().len() - 1
                    };

                    origin_cursor = origin_cursor.saturating_add(1).min(max);
                }
                Instruction::PreviousOrigin => {
                    origin_cursor = origin_cursor.saturating_sub(1);
                }
                Instruction::NextLayerNeuron => {
                    // This arithmetic is safe as every created layer has at least one neuron in it.
                    #[allow(clippy::arithmetic_side_effects, clippy::expect_used)]
                    if let Some(ref mut neuron) = layer_cursor {
                        let neurons = network
                            .get_layers()
                            .last()
                            .expect("cursor can only be set if there already exists at least one layer")
                            .neurons()
                            .len();

                        *neuron = neuron.saturating_add(1).min(neurons - 1);
                    }
                }
                Instruction::PreviousLayerNeuron => {
                    if let Some(ref mut neuron) = layer_cursor {
                        *neuron = neuron.saturating_sub(1);
                    }
                }
                // OUT is guaranteed by the constructor to be at least one
                #[allow(clippy::arithmetic_side_effects)]
                Instruction::NextOutput => {
                    output_cursor = output_cursor.saturating_add(1).min(OUT - 1);
                }
                Instruction::PreviousOutput => {
                    output_cursor = output_cursor.saturating_sub(1);
                }
                Instruction::ConnectTo => {
                    if let Some(neuron) = layer_cursor {
                        let layers = network.get_layers().len();
                        assert!(
                            layers > 0,
                            "layer cursor should only be set if there already exists at least one layer"
                        );

                        let connection = Connection::FromPrevious {
                            from: origin_cursor,
                            to: neuron,
                        };

                        // This arithmetic is safe as `layer_cursor` is only `Some()` if there exists at least one layer.
                        #[allow(clippy::arithmetic_side_effects)]
                        if !network.has_layer_connection(&connection, layers - 1) {
                            if layers == 1 {
                                network.connect_input_to_hidden(origin_cursor, neuron, T::zero());
                                last_connection = LastConnection {
                                    layer: Some(0),
                                    connection: Some(connection),
                                };
                            } else {
                                network.connect_hidden_to_hidden(origin_cursor, layers - 1, neuron, T::zero());
                                last_connection = LastConnection {
                                    layer: Some(layers - 1),
                                    connection: Some(connection),
                                };
                            }
                        }
                    } else {
                        let connection = Connection::FromPrevious {
                            from: origin_cursor,
                            to: output_cursor,
                        };

                        if !network.has_output_connection(&connection) {
                            network.connect_input_to_output(origin_cursor, output_cursor, T::zero());
                            last_connection = LastConnection {
                                layer: None,
                                connection: Some(connection),
                            };
                        }
                    }
                }
                Instruction::ConnectFrom => {
                    if let Some(neuron) = layer_cursor {
                        let layers = network.get_layers().len();
                        assert!(
                            layers > 0,
                            "layer cursor should only be set if there already exists at least one layer"
                        );

                        let connection = Connection::FromPrevious {
                            from: neuron,
                            to: output_cursor,
                        };

                        if !network.has_output_connection(&connection) {
                            network.connect_hidden_to_output(neuron, output_cursor, T::zero());
                            last_connection = LastConnection {
                                layer: None,
                                connection: Some(connection),
                            };
                        }
                    } else {
                        let connection = Connection::FromPrevious {
                            from: origin_cursor,
                            to: output_cursor,
                        };

                        if !network.has_output_connection(&connection) {
                            network.connect_input_to_output(origin_cursor, output_cursor, T::zero());
                            last_connection = LastConnection {
                                layer: None,
                                connection: Some(connection),
                            };
                        }
                    }
                }
                Instruction::Bias => {
                    if let Some(neuron) = layer_cursor {
                        // This arithmetic is safe as `layer_cursor` is only `Some()` if there exists at least one layer.
                        #[allow(clippy::arithmetic_side_effects)]
                        let layer = network.get_layers().len() - 1;
                        let connection = Connection::FromBias { to: neuron };

                        if !network.has_layer_connection(&connection, layer) {
                            network.connect_bias_to_hidden(layer, neuron, T::zero());
                        }

                        last_connection = LastConnection {
                            layer: Some(layer),
                            connection: Some(connection),
                        };
                    }
                }
                Instruction::OutputBias => {
                    let connection = Connection::FromBias { to: output_cursor };

                    if !network.has_output_connection(&connection) {
                        network.connect_bias_to_output(output_cursor, T::zero());
                    }

                    last_connection = LastConnection {
                        layer: None,
                        connection: Some(connection),
                    };
                }
                Instruction::Weight(ref weight) => {
                    let LastConnection { layer, connection } = last_connection;

                    #[allow(clippy::shadow_reuse)]
                    if let Some(ref connection) = connection {
                        network.set_weight(layer, *connection, weight.clone());
                    }
                }
                Instruction::Layer => {
                    let layers = network.get_layers().len();

                    network.add_layer(layers);
                    network.add_hidden(layers, Neuron::default());

                    layer_cursor = Some(0);

                    // if our origin tape also switches layers we need to reset it so it doesn't point
                    // to non-existent neurons
                    if layers > 0 {
                        origin_cursor = 0;
                    }

                    // TODO: ONLY TRY THIS, TO SEE IF IT WORKS
                    // let max_origin = if layers == 0 { IN - 1 } else { 0 };
                    //
                    // for origin in 0..=max_origin {
                    //     if layers == 0 {
                    //         network.connect_input_to_hidden(origin, 0, T::zero())
                    //     } else {
                    //         network.connect_hidden_to_hidden(layers - 1, origin, layers, 0, T::zero());
                    //     }
                    // }
                    //
                    // for output in 0..OUT {
                    //     network.connect_hidden_to_output(0, output, T::zero());
                    // }

                    // our stored connection might've been deleted in the process of adding a new
                    // layer, so we need to reset it
                    if let Some(ref connection) = last_connection.connection {
                        if let Some(layer) = last_connection.layer {
                            if !network.has_layer_connection(connection, layer) {
                                last_connection = LastConnection {
                                    layer: None,
                                    connection: None,
                                };

                                continue;
                            }
                        }

                        if !network.has_output_connection(connection) {
                            last_connection = LastConnection {
                                layer: None,
                                connection: None,
                            };
                        }
                    }
                }
            }
        }

        network.cleanup_connections();
        network.cleanup_neurons();

        network
    }
}
