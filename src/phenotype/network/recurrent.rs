use rand::Rng;

use crate::{
    environment::Environment,
    gene::network::recurrent::Instruction,
    genotype::network::{Network as NetworkGenotype, Recurrent as RecurrentGenotype},
    neural::{
        network::{
            recurrent::{Connection, Recurrent},
            Network,
        },
        neuron::Neuron,
        value::Value,
    },
    phenotype::{FromGenotype, Phenotype},
};

impl<T, const IN: usize, const OUT: usize> Phenotype for Network<IN, OUT, T, Recurrent<T>>
where
    T: Value,
{
    type Genotype = NetworkGenotype<IN, OUT, T, RecurrentGenotype>;
}

impl<T, const IN: usize, const OUT: usize, ENV> FromGenotype<<Self as Phenotype>::Genotype, ENV>
    for Network<IN, OUT, T, Recurrent<T>>
where
    ENV: Environment,
    T: Value,
{
    #[allow(clippy::arithmetic_side_effects)]
    fn from_genotype<RNG>(
        genotype: &NetworkGenotype<IN, OUT, T, RecurrentGenotype>,
        _environment: &ENV,
        _rng: &mut RNG,
    ) -> Self
    where
        RNG: Rng,
    {
        assert!(IN > 0, "network should have at least one input");
        assert!(OUT > 0, "network should have at least one output");

        let mut network = Network::new().recurrent();
        network.set_outputs(genotype.outputs.clone());

        let mut hidden = 0_usize;
        let mut origin_cursor = 0_usize;
        let mut destination_cursor = 0_usize;
        let mut last_connection = None;

        // The two "tapes" are origin and destination.
        // Origin: all inputs + hidden
        // Destination: all outputs + hidden

        for instruction in genotype.instructions() {
            let input = origin_cursor < IN;
            let output = destination_cursor < OUT;

            match *instruction {
                Instruction::Neuron => {
                    network.add_hidden(Neuron::default());
                    hidden = hidden.saturating_add(1);
                }
                Instruction::ActivationFunction(ref activation_function) => {
                    if output {
                        network.set_output_activation_function(destination_cursor, activation_function.clone());
                    } else {
                        // The condition makes sure the destination cursor is pointing to a hidden neuron,
                        // making this arithmetic safe.
                        #[allow(clippy::arithmetic_side_effects)]
                        let neuron = network.get_hidden_mut(destination_cursor - OUT);
                        neuron.activation_function = activation_function.clone();
                    }
                }
                // The worst thing to happen in this arithmetic is for IN to be zero, which the constructor
                // makes sure never happens.
                #[allow(clippy::arithmetic_side_effects)]
                Instruction::NextOrigin => {
                    origin_cursor = origin_cursor.saturating_add(1).min(IN + hidden - 1);
                }
                Instruction::PreviousOrigin => {
                    origin_cursor = origin_cursor.saturating_sub(1);
                }
                // The worst thing to happen in this arithmetic is for OUT to be zero, which the constructor
                // makes sure never happens.
                #[allow(clippy::arithmetic_side_effects)]
                Instruction::NextDestination => {
                    destination_cursor = destination_cursor.saturating_add(1).min(OUT + hidden - 1);
                }
                Instruction::PreviousDestination => {
                    destination_cursor = destination_cursor.saturating_sub(1);
                }
                Instruction::Connect => {
                    // This is safe because `input` and `output` check whether the respective cursors are
                    // pointing to hidden neurons or the constant in- and outputs.
                    #[allow(clippy::arithmetic_side_effects)]
                    let connection = match (input, output) {
                        (true, true) => Connection::InputToOutput(origin_cursor, destination_cursor),
                        (true, false) => Connection::InputToHidden(origin_cursor, destination_cursor - OUT),
                        (false, true) => Connection::HiddenToOutput(origin_cursor - IN, destination_cursor),
                        (false, false) => Connection::HiddenToHidden(origin_cursor - IN, destination_cursor - OUT),
                    };

                    if !network.has_connection(&connection) {
                        network.connect(connection, T::zero());
                    }

                    last_connection = Some(connection);
                }
                Instruction::ConnectBias => {
                    let connection = if output {
                        Connection::BiasToOutput(destination_cursor)
                    } else {
                        // The condition makes sure the destination cursor is pointing to a hidden neuron,
                        // making this arithmetic safe.
                        #[allow(clippy::arithmetic_side_effects)]
                        Connection::BiasToHidden(destination_cursor - OUT)
                    };

                    if !network.has_connection(&connection) {
                        network.connect(connection, T::zero());
                    }

                    last_connection = Some(connection);
                }
                Instruction::Weight(ref weight) => {
                    if let Some(ref connection) = last_connection {
                        network.set_weight(*connection, weight.clone());
                    }
                }
            }
        }

        network.cleanup_connections();

        // Note that we're reversing the traversal to make the removal safe.
        'neurons: for index in (0..network.neurons().len()).rev() {
            let mut input = false;
            let mut output = false;

            for connection in network.connections().keys() {
                match *connection {
                    Connection::InputToOutput(_, _) | Connection::BiasToHidden(_) | Connection::BiasToOutput(_) => {
                        continue
                    }
                    Connection::InputToHidden(_, to) => input |= to == index,
                    Connection::HiddenToHidden(from, to) => {
                        // We ignore loops with this condition.
                        if from != to {
                            input |= to == index;
                            output |= from == index;
                        }
                    }
                    Connection::HiddenToOutput(from, _) => output |= from == index,
                }

                if input && output {
                    continue 'neurons;
                }
            }

            // We can't reach this point if both are true, but to make this refactor-safe we still check.
            if !input || !output {
                network.remove_hidden(index);
            }
        }

        network
    }
}
