/// Phenotype implementation for feedforward networks.
pub mod feedforward;
/// Phenotype implementation for recurrent networks.
pub mod recurrent;
