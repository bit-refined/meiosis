use rand::Rng;

use crate::{
    genotype::network::{Feedforward, Network, Recurrent},
    neural::value::Value,
    operators::mutation::{remove::Remove, Mutation},
};

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Feedforward>, RNG> for Remove
where
    RNG: Rng,
    T: Value,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Feedforward>, rng: &mut RNG) {
        self.mutate(&mut genotype.instructions, rng);
    }
}

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Recurrent>, RNG> for Remove
where
    RNG: Rng,
    T: Value,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Recurrent>, rng: &mut RNG) {
        self.mutate(&mut genotype.instructions, rng);
    }
}
