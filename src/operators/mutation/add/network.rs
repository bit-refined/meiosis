use core::num::NonZeroUsize;

use rand::{seq::SliceRandom, Rng};

use crate::{
    gene::network::{feedforward::Instruction as FFInst, recurrent::Instruction as RCInst},
    genotype::network::{Feedforward, Network, Recurrent},
    neural::{activation::ActivationType, value::Value},
    operators::mutation::Mutation,
    random::Random,
};

/// This mutation strategy adds instructions to the construction plan of a feedforward network.
#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct ExpandNetwork<T>
where
    T: Value,
{
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    chance: f64,
    /// If the `Genotype` we're mutating already has this many genes, the strategy will do nothing.
    max_genes: Option<NonZeroUsize>,
    /// MISSING DOCS
    activation_functions: Vec<<T as ActivationType>::Function>,
}

impl<T> ExpandNetwork<T>
where
    T: Value,
{
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(chance: f64) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );

        Self {
            chance,
            max_genes: None,
            activation_functions: vec![],
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if `max_genes` is set to 0.
    #[allow(clippy::expect_used)]
    #[must_use]
    pub fn with_max_genes(mut self, max_genes: usize) -> Self {
        self.max_genes = Some(
            NonZeroUsize::new(max_genes).expect("maximum amount of genes should be set to a value greater than zero"),
        );

        self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn with_activation_function(mut self, activation_function: <T as ActivationType>::Function) -> Self {
        self.activation_functions.push(activation_function);

        self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn with_activation_functions(mut self, activation_functions: Vec<<T as ActivationType>::Function>) -> Self {
        self.activation_functions = activation_functions;

        self
    }
}

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Feedforward>, RNG> for ExpandNetwork<T>
where
    RNG: Rng,
    T: Value + Random,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Feedforward>, rng: &mut RNG) {
        assert!(
            !self.activation_functions.is_empty(),
            "strategy has no activation functions to choose from"
        );

        if let Some(max_genes) = self.max_genes {
            if genotype.instructions.len() >= max_genes.get() {
                return;
            }
        }

        if rng.gen_bool(self.chance) {
            #[allow(clippy::unwrap_used)]
            let gene = match rng.gen_range(0..=14_usize) {
                0 => FFInst::Neuron,
                1 => FFInst::LayerActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                2 => FFInst::OutputActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                3 => FFInst::NextOrigin,
                4 => FFInst::PreviousOrigin,
                5 => FFInst::NextLayerNeuron,
                6 => FFInst::PreviousLayerNeuron,
                7 => FFInst::NextOutput,
                8 => FFInst::PreviousOutput,
                9 => FFInst::ConnectTo,
                10 => FFInst::ConnectFrom,
                11 => FFInst::Bias,
                12 => FFInst::OutputBias,
                13 => FFInst::Weight(T::random(rng)),
                14 => FFInst::Layer,
                _ => unreachable!(),
            };

            let position_for_new_gene = rng.gen_range(0..=genotype.instructions.len());
            genotype.instructions.insert(position_for_new_gene, gene);
        }
    }
}

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Recurrent>, RNG> for ExpandNetwork<T>
where
    RNG: Rng,
    T: Value + Random,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Recurrent>, rng: &mut RNG) {
        assert!(
            !self.activation_functions.is_empty(),
            "strategy has no activation functions to choose from"
        );

        if let Some(max_genes) = self.max_genes {
            if genotype.instructions.len() >= max_genes.get() {
                return;
            }
        }

        if rng.gen_bool(self.chance) {
            let gene = match rng.gen_range(0..=8_usize) {
                0 => RCInst::Neuron,
                #[allow(clippy::unwrap_used)]
                1 => RCInst::ActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                2 => RCInst::NextOrigin,
                3 => RCInst::PreviousOrigin,
                4 => RCInst::NextDestination,
                5 => RCInst::PreviousDestination,
                6 => RCInst::Connect,
                7 => RCInst::ConnectBias,
                8 => RCInst::Weight(T::random(rng)),
                _ => unreachable!(),
            };

            let position_for_new_gene = rng.gen_range(0..=genotype.instructions.len());
            genotype.instructions.insert(position_for_new_gene, gene);
        }
    }
}
