use core::num::NonZeroUsize;

use rand::Rng;

use crate::{gene::Gene, random::Random};

use super::Mutation;

/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

/// This strategy extends the genome by adding a single random gene.
///
/// It is advised to check the trait implementation documentation for more details on how genes are added.
#[derive(Copy, Clone, Debug)]
pub struct Add {
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    chance: f64,
    /// If the `Genotype` we're mutating already has this many genes, the strategy will do nothing.
    max_genes: Option<NonZeroUsize>,
}

impl Add {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`. Also panics if
    /// `max_genes` is `Some(0)`
    #[allow(clippy::expect_used)]
    #[must_use]
    pub fn new(chance: f64, max_genes: Option<usize>) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );

        Self {
            chance,
            max_genes: max_genes.map(|m| {
                m.try_into()
                    .expect("with a maximum set, the value should be greater than zero")
            }),
        }
    }
}

/// Mutation for [Vec] pushes new [Gene]s randomly into [Vec].
impl<G, RNG> Mutation<Vec<G>, RNG> for Add
where
    G: Gene + Random,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        if let Some(max_genes) = self.max_genes {
            if genotype.len() >= max_genes.get() {
                return;
            }
        }

        if rng.gen_bool(self.chance) {
            let random_gene = G::random(rng);

            let position_for_new_gene = rng.gen_range(0..=genotype.len());
            genotype.insert(position_for_new_gene, random_gene);
        }
    }
}

/// Mutation for [String] pushes new [Gene]s randomly to the [String].
impl<RNG> Mutation<String, RNG> for Add
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        if let Some(max_genes) = self.max_genes {
            if genotype.len() >= max_genes.get() {
                return;
            }
        }

        if rng.gen_bool(self.chance) {
            let random_gene = char::random(rng);

            // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
            // as that may require resizing.
            // The next best solution is to collect all chars into a Vec, manipulate that, and
            // convert it back into a String.
            let mut chars = genotype.chars().collect::<Vec<_>>();

            let position_for_new_gene = rng.gen_range(0..=chars.len());
            chars.insert(position_for_new_gene, random_gene);

            *genotype = chars.iter().collect::<String>();
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Add, Mutation};

    #[test]
    fn vector() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Add {
            chance: 1.0_f64,
            max_genes: None,
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 1_921_246_409, 2, 3, 4]);
    }

    #[test]
    fn vector_max_genes() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Add {
            chance: 1.0_f64,
            max_genes: Some(5.try_into().unwrap()),
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 2, 3, 4]);
    }

    #[test]
    fn string() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Add {
            chance: 1.0_f64,
            max_genes: None,
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "te\u{79f2e}st");
    }
}
