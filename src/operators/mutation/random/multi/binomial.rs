use rand::Rng;

use crate::{gene::Gene, operators::mutation::Mutation, random::Random as RandomGene};

/// This strategy will randomly change multiple genes to new random values. It goes through gene by
/// gene and according to the `rate` may mutate it.
/// It will only ever mutate a gene once, but may mutate more or less than the average.
#[derive(Copy, Clone, Debug)]
pub struct Binomial {
    /// A value in the range of `[0.0, 0.1]` describing in percent how many genes will be mutated.
    rate: f64,
}

impl Binomial {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `rate` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(rate: f64) -> Self {
        assert!(
            rate >= 0.0_f64,
            "mutation rate has to be above or equal 0.0, but is {rate}",
        );
        assert!(
            rate <= 1.0_f64,
            "mutation rate has to be below or equal 1.0, but is {rate}",
        );

        Self { rate }
    }
}

impl<G, const GENES: usize, RNG> Mutation<[G; GENES], RNG> for Binomial
where
    G: Gene + RandomGene,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut [G; GENES], rng: &mut RNG) {
        for gene in genotype {
            if rng.gen_bool(self.rate) {
                *gene = G::random(rng);
            }
        }
    }
}

impl<G, RNG> Mutation<Vec<G>, RNG> for Binomial
where
    G: Gene + RandomGene,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        for gene in genotype {
            if rng.gen_bool(self.rate) {
                *gene = G::random(rng);
            }
        }
    }
}

impl<RNG> Mutation<String, RNG> for Binomial
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
        // as that may require resizing.
        // The next best solution is to collect all chars into a Vec, manipulate that, and
        // convert it back into a String.
        let mut chars = genotype.chars().collect::<Vec<_>>();

        for char in &mut chars {
            if rng.gen_bool(self.rate) {
                *char = char::random(rng);
            }
        }

        *genotype = chars.iter().collect::<String>();
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Binomial, Mutation};

    #[test]
    fn array() {
        // we set mutation chance to 0.4 so 2 genes should mutate
        let mutation = Binomial { rate: 0.4 };
        let mut rng = SmallRng::seed_from_u64(4);

        let mut array = [0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 239, 2, 163, 4]);
    }

    #[test]
    fn vector() {
        // we set mutation chance to 0.4 so 2 genes should mutate
        let mutation = Binomial { rate: 0.4 };
        let mut rng = SmallRng::seed_from_u64(4);

        let mut array = vec![0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 239, 2, 163, 4]);
    }

    #[test]
    fn string() {
        // we set mutation chance to 0.25 so 1 gene should mutate
        let mutation = Binomial { rate: 0.25 };
        let mut rng = SmallRng::seed_from_u64(1);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "t\u{31df6}st");
    }
}
