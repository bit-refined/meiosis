use rand::Rng;

use crate::{gene::Gene, neighbour::DistancedNeighbour, operators::mutation::Mutation};

/// This strategy will randomly change existing genes to new values that are guaranteed to be no
/// further away than `maximum_distance`. Like in all other strategies, random chance can also mean,
/// that a new gene will not be different from the previous gene. A typical case might be that a
/// gene is at the maximum value of its type (like [`u32::MAX`]) and the strategy wants to nudge in the
/// forward direction. Since there is no value available in that direction, the gene stays unchanged.
///
/// However, if the [Gene] type implements [`DistancedNeighbour`] in such a way that circular neighbour relations
/// are possible, it may always find a new gene.
///
/// # Note
/// Distribution is binomial. See [`super::binomial::Binomial`].
#[derive(Copy, Clone, Debug)]
pub struct Nudge<D = usize> {
    /// This value describes the biggest distance a new gene is allowed to be "away" from what was
    /// in its place before mutation. For more information check the [DistancedNeighbour] trait or specific
    /// implementations, depending on what type of genes are being mutated.
    maximum_distance: D,
    /// A value in the range of `[0.0, 0.1]` describing in percent how many genes will be mutated.
    rate: f64,
}

impl<D> Nudge<D> {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `rate` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(rate: f64, maximum_distance: D) -> Self {
        assert!(
            rate >= 0.0_f64,
            "mutation rate has to be above or equal 0.0, but is {rate}",
        );
        assert!(
            rate <= 1.0_f64,
            "mutation rate has to be below or equal 1.0, but is {rate}",
        );

        Self { maximum_distance, rate }
    }
}

impl<G, const GENES: usize, RNG> Mutation<[G; GENES], RNG> for Nudge
where
    G: Gene + DistancedNeighbour,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut [G; GENES], rng: &mut RNG) {
        for gene in genotype {
            if rng.gen_bool(self.rate) {
                let possible_new_gene = if rng.gen() {
                    // we want a forward gene
                    gene.random_forward_neighbour(rng, &self.maximum_distance)
                } else {
                    // we want a backward gene
                    gene.random_backward_neighbour(rng, &self.maximum_distance)
                };

                if let Some(new_gene) = possible_new_gene {
                    *gene = new_gene;
                }
            }
        }
    }
}

impl<G, RNG> Mutation<Vec<G>, RNG> for Nudge
where
    G: Gene + DistancedNeighbour,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        for gene in genotype {
            if rng.gen_bool(self.rate) {
                let possible_new_gene = if rng.gen() {
                    // we want a forward gene
                    gene.random_forward_neighbour(rng, &self.maximum_distance)
                } else {
                    // we want a backward gene
                    gene.random_backward_neighbour(rng, &self.maximum_distance)
                };

                if let Some(new_gene) = possible_new_gene {
                    *gene = new_gene;
                }
            }
        }
    }
}

impl<RNG> Mutation<String, RNG> for Nudge
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
        // as that may require resizing.
        // The next best solution is to collect all chars into a Vec, manipulate that, and
        // convert it back into a String.
        let mut chars = genotype.chars().collect::<Vec<_>>();

        for gene in &mut chars {
            if rng.gen_bool(self.rate) {
                let possible_new_gene = if rng.gen() {
                    // we want a forward gene
                    gene.random_forward_neighbour(rng, &self.maximum_distance)
                } else {
                    // we want a backward gene
                    gene.random_backward_neighbour(rng, &self.maximum_distance)
                };

                if let Some(new_gene) = possible_new_gene {
                    *gene = new_gene;
                }
            }
        }

        *genotype = chars.iter().collect::<String>();
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Mutation, Nudge};

    #[test]
    fn array() {
        let mutation = Nudge {
            maximum_distance: 1.try_into().unwrap(),
            rate: 0.4,
        };
        let mut rng = SmallRng::seed_from_u64(3);

        let mut array = [0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 2, 2, 4]);
    }

    #[test]
    fn vector() {
        let mutation = Nudge {
            maximum_distance: 1.try_into().unwrap(),
            rate: 0.4,
        };
        let mut rng = SmallRng::seed_from_u64(3);

        let mut array = vec![0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 2, 2, 4]);
    }

    #[test]
    fn string() {
        let mutation = Nudge {
            maximum_distance: 1.try_into().unwrap(),
            rate: 0.25,
        };
        let mut rng = SmallRng::seed_from_u64(1);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "tdst");
    }
}
