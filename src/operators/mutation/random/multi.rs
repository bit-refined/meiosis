/// MISSING DOCS
pub mod binomial;
/// MISSING DOCS
pub mod nudge;
/// MISSING DOCS
pub mod uniform;

/// MISSING DOCS
// This should only be the case with huge values, which is not something I care about in the current
// state of this crate.
#[allow(clippy::cast_precision_loss)]
#[allow(clippy::float_arithmetic)]
// We verify `rate` to be positive, so this can never happen.
#[allow(clippy::cast_sign_loss)]
// This is intended and may be replaced by a safer way in the future.
#[allow(clippy::cast_possible_truncation, clippy::as_conversions)]
fn mutations_by_rate(genes: usize, rate: f64) -> usize {
    assert!(
        0.0_f64 < rate && rate < f64::INFINITY,
        "mutation rate has to be a finite positive number"
    );

    (genes as f64 * rate).floor() as usize
}
