use rand::Rng;

use crate::{gene::Gene, operators::mutation::Mutation, random::Random as RandomGene};

/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

/// This strategy will randomly change an existing gene to a new value.
/// # Panics
/// A runtime check is performed to make sure the `chance` of mutation is in the valid range of `[0.0, 1.0]`.
#[derive(Copy, Clone, Debug)]
pub struct Uniform {
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    ///
    /// It is very important to keep in mind that this is the mathematical chance of *triggering*
    /// a mutation, thereby a gene mutating to the same value it was before is still valid.
    /// For an outside observer it looks as if the genome hasn't changed, but a mutation still occurred.
    chance: f64,
}

impl Uniform {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(chance: f64) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );

        Self { chance }
    }
}

impl<G, const GENES: usize, RNG> Mutation<[G; GENES], RNG> for Uniform
where
    G: Gene + RandomGene,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut [G; GENES], rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let random_gene = G::random(rng);
            let random_slot = rng.gen_range(0..genotype.len());
            genotype[random_slot] = random_gene;
        }
    }
}

impl<G, RNG> Mutation<Vec<G>, RNG> for Uniform
where
    G: Gene + RandomGene,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let random_gene = G::random(rng);
            let random_slot = rng.gen_range(0..genotype.len());
            genotype[random_slot] = random_gene;
        }
    }
}

impl<RNG> Mutation<String, RNG> for Uniform
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let random_gene = char::random(rng);

            // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
            // as that may require resizing.
            // The next best solution is to collect all chars into a Vec, manipulate that, and
            // convert it back into a String.
            let mut chars = genotype.chars().collect::<Vec<_>>();
            let random_slot = rng.gen_range(0..chars.len());
            chars[random_slot] = random_gene;

            *genotype = chars.iter().collect::<String>();
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Mutation, Uniform};

    #[test]
    fn array() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Uniform { chance: 1.0 };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = [0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 201, 3, 4]);
    }

    #[test]
    fn vector() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Uniform { chance: 1.0 };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 201, 3, 4]);
    }

    #[test]
    fn string() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Uniform { chance: 1.0 };
        let mut rng = SmallRng::seed_from_u64(4);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "t\u{ad95b}st");
    }
}
