use crate::{
    gene::network::{feedforward::Instruction, recurrent::Instruction as RecurrentInstruction},
    genotype::network::{Feedforward, Network, Recurrent},
    neighbour::Neighbour,
    neural::value::Value,
    operators::mutation::{random::single::finetune::Finetune, Mutation},
};
use rand::{seq::IteratorRandom, Rng};

/// This strategy fine tunes weights in feedforward network instructions. Keep in mind that instructions
/// do not necessarily translate directly into final neuron connection weights, because there might
/// be two consecutive weight instructions - the second one making the first one irrelevant as it
/// overwrites the value.
impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Feedforward>, RNG> for Finetune
where
    RNG: Rng,
    T: Value + Neighbour,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Feedforward>, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let weights = genotype
                .instructions
                .iter_mut()
                .filter_map(|instruction| match *instruction {
                    Instruction::Weight(ref mut weight) => Some(weight),
                    Instruction::Neuron
                    | Instruction::LayerActivationFunction(_)
                    | Instruction::OutputActivationFunction(_)
                    | Instruction::NextOrigin
                    | Instruction::PreviousOrigin
                    | Instruction::NextLayerNeuron
                    | Instruction::PreviousLayerNeuron
                    | Instruction::NextOutput
                    | Instruction::PreviousOutput
                    | Instruction::ConnectTo
                    | Instruction::ConnectFrom
                    | Instruction::Bias
                    | Instruction::OutputBias
                    | Instruction::Layer => None,
                });

            if let Some(weight) = weights.choose(rng) {
                let possible_new_weight = if rng.gen() {
                    // we want a forward gene
                    weight.successor(rng)
                } else {
                    // we want a backward gene
                    weight.predecessor(rng)
                };

                if let Some(new_weight) = possible_new_weight {
                    *weight = new_weight;
                }
            }
        }
    }
}

/// This strategy fine tunes weights in recurrent network instructions. Keep in mind that instructions
/// do not necessarily translate directly into final neuron connection weights, because there might
/// be two consecutive weight instructions - the second one making the first one irrelevant as it
/// overwrites the value.
impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Recurrent>, RNG> for Finetune
where
    RNG: Rng,
    T: Value + Neighbour,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Recurrent>, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let weights = genotype
                .instructions
                .iter_mut()
                .filter_map(|instruction| match *instruction {
                    RecurrentInstruction::Weight(ref mut weight) => Some(weight),
                    RecurrentInstruction::Neuron
                    | RecurrentInstruction::ActivationFunction(_)
                    | RecurrentInstruction::NextOrigin
                    | RecurrentInstruction::PreviousOrigin
                    | RecurrentInstruction::NextDestination
                    | RecurrentInstruction::PreviousDestination
                    | RecurrentInstruction::Connect
                    | RecurrentInstruction::ConnectBias => None,
                });

            if let Some(weight) = weights.choose(rng) {
                let possible_new_weight = if rng.gen() {
                    // we want a forward gene
                    weight.successor(rng)
                } else {
                    // we want a backward gene
                    weight.predecessor(rng)
                };

                if let Some(new_weight) = possible_new_weight {
                    *weight = new_weight;
                }
            }
        }
    }
}
