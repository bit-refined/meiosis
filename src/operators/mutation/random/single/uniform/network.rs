use rand::{seq::SliceRandom, Rng};

use crate::{
    gene::network::{feedforward::Instruction as FFInst, recurrent::Instruction as RCInst},
    genotype::network::{Feedforward, Network, Recurrent},
    neural::{activation::ActivationType, value::Value},
    operators::mutation::Mutation,
    random::Random,
};

/// MISSING DOCS
#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct UniformNetwork<T>
where
    T: Value,
{
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    ///
    /// It is very important to keep in mind that this is the mathematical chance of *triggering*
    /// a mutation, thereby a gene mutating to the same value it was before is still valid.
    /// For an outside observer it looks as if the genome hasn't changed, but a mutation still occurred.
    chance: f64,
    /// MISSING DOCS
    activation_functions: Vec<<T as ActivationType>::Function>,
}

impl<T> UniformNetwork<T>
where
    T: Value,
{
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(chance: f64) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );
        Self {
            chance,
            activation_functions: vec![],
        }
    }

    /// MISSING DOCS
    #[must_use]
    pub fn with_activation_function(mut self, activation_function: <T as ActivationType>::Function) -> Self {
        self.activation_functions.push(activation_function);

        self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn with_activation_functions(mut self, activation_functions: Vec<<T as ActivationType>::Function>) -> Self {
        self.activation_functions = activation_functions;

        self
    }
}

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Feedforward>, RNG> for UniformNetwork<T>
where
    RNG: Rng,
    T: Value + Random,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Feedforward>, rng: &mut RNG) {
        assert!(
            !self.activation_functions.is_empty(),
            "strategy has no activation functions to choose from"
        );

        // Indexing is alright here because `random_slot` is guaranteed to be a valid index.
        #[allow(clippy::indexing_slicing, clippy::unwrap_used)]
        if rng.gen_bool(self.chance) && !genotype.instructions.is_empty() {
            let gene = match rng.gen_range(0..=14_usize) {
                0 => FFInst::Neuron,
                1 => FFInst::LayerActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                2 => FFInst::OutputActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                3 => FFInst::NextOrigin,
                4 => FFInst::PreviousOrigin,
                5 => FFInst::NextLayerNeuron,
                6 => FFInst::PreviousLayerNeuron,
                7 => FFInst::NextOutput,
                8 => FFInst::PreviousOutput,
                9 => FFInst::ConnectTo,
                10 => FFInst::ConnectFrom,
                11 => FFInst::Bias,
                12 => FFInst::OutputBias,
                13 => FFInst::Weight(T::random(rng)),
                14 => FFInst::Layer,
                _ => unreachable!(),
            };

            let random_slot = rng.gen_range(0..genotype.instructions.len());
            genotype.instructions[random_slot] = gene;
        }
    }
}

impl<const IN: usize, const OUT: usize, T, RNG> Mutation<Network<IN, OUT, T, Recurrent>, RNG> for UniformNetwork<T>
where
    RNG: Rng,
    T: Value + Random,
{
    fn mutate(&self, genotype: &mut Network<IN, OUT, T, Recurrent>, rng: &mut RNG) {
        assert!(
            !self.activation_functions.is_empty(),
            "strategy has no activation functions to choose from"
        );

        // Indexing is alright here because `random_slot` is guaranteed to be a valid index.
        #[allow(clippy::indexing_slicing, clippy::unwrap_used)]
        if rng.gen_bool(self.chance) && !genotype.instructions.is_empty() {
            let gene = match rng.gen_range(0..=8_usize) {
                0 => RCInst::Neuron,
                1 => RCInst::ActivationFunction(self.activation_functions.choose(rng).unwrap().clone()),
                2 => RCInst::NextOrigin,
                3 => RCInst::PreviousOrigin,
                4 => RCInst::NextDestination,
                5 => RCInst::PreviousDestination,
                6 => RCInst::Connect,
                7 => RCInst::ConnectBias,
                8 => RCInst::Weight(T::random(rng)),
                _ => unreachable!(),
            };

            let random_slot = rng.gen_range(0..genotype.instructions.len());
            genotype.instructions[random_slot] = gene;
        }
    }
}
