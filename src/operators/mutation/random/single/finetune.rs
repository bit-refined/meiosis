/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

use crate::{gene::Gene, neighbour::Neighbour, operators::mutation::Mutation};
use rand::Rng;

/// Similar to [`crate::operators::mutation::random::single::nudge::Nudge`], this mutation strategy randomly changes genes, but because we're using
/// [`Neighbour`], new values will lie directly next to the original gene value. This allows for
/// minute changes to the genome, making it possible to hone in to local or even global maxima.
#[derive(Copy, Clone, Debug)]
pub struct Finetune {
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    chance: f64,
}

impl Finetune {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(chance: f64) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );

        Self { chance }
    }
}
impl<G, const GENES: usize, RNG> Mutation<[G; GENES], RNG> for Finetune
where
    G: Gene + Neighbour,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut [G; GENES], rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let random_slot = rng.gen_range(0..GENES);
            let selected_gene = &mut genotype[random_slot];

            let possible_new_gene = if rng.gen() {
                // we want a forward gene
                selected_gene.successor(rng)
            } else {
                // we want a backward gene
                selected_gene.predecessor(rng)
            };

            if let Some(new_gene) = possible_new_gene {
                *selected_gene = new_gene;
            }
        }
    }
}

impl<G, RNG> Mutation<Vec<G>, RNG> for Finetune
where
    G: Gene + Neighbour,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            let random_slot = rng.gen_range(0..genotype.len());
            let selected_gene = &mut genotype[random_slot];

            let possible_new_gene = if rng.gen() {
                // we want a forward gene
                selected_gene.successor(rng)
            } else {
                // we want a backward gene
                selected_gene.predecessor(rng)
            };

            if let Some(new_gene) = possible_new_gene {
                *selected_gene = new_gene;
            }
        }
    }
}

impl<RNG> Mutation<String, RNG> for Finetune
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        // this is okay because we're indexing with a value that can't possibly be outside of the slice range
        #[allow(clippy::indexing_slicing)]
        if rng.gen_bool(self.chance) {
            // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
            // as that may require resizing.
            // The next best solution is to collect all chars into a Vec, manipulate that, and
            // convert it back into a String.
            let mut chars = genotype.chars().collect::<Vec<_>>();

            let random_slot = rng.gen_range(0..chars.len());
            let selected_gene = &mut chars[random_slot];

            let possible_new_gene = if rng.gen() {
                // we want a forward gene
                selected_gene.successor(rng)
            } else {
                // we want a backward gene
                selected_gene.predecessor(rng)
            };

            if let Some(new_gene) = possible_new_gene {
                *selected_gene = new_gene;
            }

            *genotype = chars.iter().collect::<String>();
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::operators::mutation::random::single::finetune::Finetune;
    use rand::{rngs::SmallRng, SeedableRng};

    use super::Mutation;

    #[test]
    fn array() {
        let mutation = Finetune { chance: 1.0_f64 };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = [0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 1, 3, 4]);
    }

    #[test]
    fn vector() {
        let mutation = Finetune { chance: 1.0_f64 };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0_u8, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 1, 3, 4]);
    }

    #[test]
    fn string() {
        let mutation = Finetune { chance: 1.0_f64 };
        let mut rng = SmallRng::seed_from_u64(4);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "tfst");
    }

    #[test]
    #[allow(clippy::float_cmp, clippy::unreadable_literal)]
    fn floats() {
        let mutation = Finetune { chance: 1.0_f64 };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = [0.0, 1.0, 2.0, 3.0, 4.0];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0.0, 1.0, 1.9999999999999998, 3.0, 4.0]);
    }
}
