use core::num::NonZeroUsize;

use rand::Rng;

use crate::gene::Gene;

use super::Mutation;

/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

/// This strategy shortens the genome by removing a single gene.
///
/// It is advised to check the trait implementation documentation for more details on how genes are removed.
#[derive(Copy, Clone, Debug)]
pub struct Remove {
    /// A value in the range of `[0.0, 0.1]` describing how likely a random mutation will occur.
    chance: f64,
    /// If the `Genotype` we're mutating would end up with less than the here defined genes,
    /// the strategy will do nothing.
    min_genes: NonZeroUsize,
}

impl Remove {
    /// MISSING DOCS
    /// # Panics
    /// Panics if `chance` of mutation is outside the valid range of `[0.0, 1.0]`.
    #[must_use]
    pub fn new(chance: f64, min_genes: NonZeroUsize) -> Self {
        assert!(
            chance >= 0.0_f64,
            "chance of mutation has to be above or equal 0.0, but is {chance}",
        );
        assert!(
            chance <= 1.0_f64,
            "chance of mutation has to be below or equal 1.0, but is {chance}",
        );

        Self { chance, min_genes }
    }
}

/// [Mutation] for [Vec] removes random [Gene]s.
/// This is not the fastest way to remove elements, but it results in uniform removal.
impl<G, RNG> Mutation<Vec<G>, RNG> for Remove
where
    G: Gene,
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut Vec<G>, rng: &mut RNG) {
        if genotype.len() <= self.min_genes.get() {
            return;
        }

        if rng.gen_bool(self.chance) {
            let gene_to_remove = rng.gen_range(0..genotype.len());
            let _removed = genotype.remove(gene_to_remove);
        }
    }
}

/// [Mutation] for [String] removes random [Gene]s.
/// This is not the fastest way to remove elements, but it results in uniform removal.
impl<RNG> Mutation<String, RNG> for Remove
where
    RNG: Rng,
{
    fn mutate(&self, genotype: &mut String, rng: &mut RNG) {
        if genotype.len() <= self.min_genes.get() {
            return;
        }

        if rng.gen_bool(self.chance) {
            // Because of the UTF-8 nature of Strings, we can not manipulate `char`s in place,
            // as that may require resizing.
            // The next best solution is to collect all chars into a Vec, manipulate that, and
            // convert it back into a String.
            let mut chars = genotype.chars().collect::<Vec<_>>();

            let gene_to_remove = rng.gen_range(0..chars.len());
            _ = chars.remove(gene_to_remove);

            *genotype = chars.iter().collect::<String>();
        }
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Mutation, Remove};

    #[test]
    fn vector() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Remove {
            chance: 1.0_f64,
            min_genes: 1.try_into().unwrap(),
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 3, 4]);
    }

    #[test]
    fn vector_min_genes() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Remove {
            chance: 1.0_f64,
            min_genes: 5.try_into().unwrap(),
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut array = vec![0, 1, 2, 3, 4];

        mutation.mutate(&mut array, &mut rng);

        assert_eq!(array, [0, 1, 2, 3, 4]);
    }

    #[test]
    fn string() {
        // we set mutation chance to 1 to guarantee it
        let mutation = Remove {
            chance: 1.0_f64,
            min_genes: 1.try_into().unwrap(),
        };
        let mut rng = SmallRng::seed_from_u64(0);

        let mut string = String::from("test");

        mutation.mutate(&mut string, &mut rng);

        assert_eq!(string, "tet");
    }
}
