use core::num::NonZeroUsize;

use rand::Rng;

use crate::species::Evaluated;

/// MISSING DOCS
pub mod fitness_proportionate;
/// MISSING DOCS
mod random;
/// MISSING DOCS
mod tournament;
/// MISSING DOCS
pub mod truncate;

/// MISSING DOCS
type Selected<GENOTYPE> = Vec<Evaluated<GENOTYPE>>;
/// MISSING DOCS
type NonSelected<GENOTYPE> = Vec<Evaluated<GENOTYPE>>;

/// Using a certain strategy, a population is divided into selected and non-selected members.
///
/// Some strategies might have duplicate entries, depending on how they determine the parents.
pub trait Selection<GENOTYPE> {
    /// MISSING DOCS
    fn select<RNG>(
        &self,
        rng: &mut RNG,
        population: Vec<Evaluated<GENOTYPE>>,
    ) -> (Selected<GENOTYPE>, NonSelected<GENOTYPE>)
    where
        RNG: Rng;

    /// Some selection strategies will only return a single member from the original population.
    ///
    /// # Panics
    /// This method will panic if not implemented by the `Strategy`.
    #[allow(clippy::panic)]
    fn select_single<RNG>(
        &self,
        _population: Vec<Evaluated<GENOTYPE>>,
        _rng: &mut RNG,
    ) -> (Evaluated<GENOTYPE>, NonSelected<GENOTYPE>)
    where
        RNG: Rng,
    {
        panic!("This selection strategy does not implement selection of a single member.")
    }
}

/// This is an internal function that helps convert a strategy that only provides a `select_single` implementation,
/// to naively also provide one for a multi-select. Usually `max_selections` is stored in the strategy as a field,
/// so the user can configure them.
fn select_multiple_from_single<Strategy, GENOTYPE, RNG>(
    strategy: &Strategy,
    mut population: Vec<Evaluated<GENOTYPE>>,
    rng: &mut RNG,
    max_selections: Option<NonZeroUsize>,
) -> (Selected<GENOTYPE>, NonSelected<GENOTYPE>)
where
    Strategy: Selection<GENOTYPE>,
    RNG: Rng,
{
    let selections = max_selections.map_or(population.len(), NonZeroUsize::get);

    assert!(
        selections < population.len(),
        "selection always produces less individuals than there were originally in the population"
    );

    let mut selected = Vec::with_capacity(population.len());

    loop {
        // Stochastic acceptance is one of those selections that only returns a single selection.
        let (selection, non_selected) = strategy.select_single(population, rng);

        // store the original selection
        selected.push(selection);

        // make all non-selected members our new population
        population = non_selected;

        // Note: it would be sufficient to simply check for equality here, but defensive programming can't hurt :)
        if selected.len() >= selections {
            return (selected, population);
        }
    }
}
