use rand::Rng;

use crate::genotype::Genotype;

/// MISSING DOCS
pub mod add;
/// MISSING DOCS
pub mod combination;
/// MISSING DOCS
pub mod random;
/// MISSING DOCS
pub mod remove;
/// MISSING DOCS
pub mod swap;

/// Using a certain mutation strategy, genotypes will be slightly altered before the final new
/// population is fed into the next loop of a genetic algorithm.
///
/// # Foreign Implementation
/// While implementing this for a foreign `Genotype`, it is imperative to make the
/// semantic and mathematical distinction of mutation. Only because a mutation has been triggered,
/// does not mean the mutation can't end up looking like the original `Genotype`. The reason for
/// this is that by taking away this option, the mutation would no longer be uniform. This is
/// especially bad for `Genotypes` with low-variation `Gene`s like `bool` or enums.
///
/// Foreign implementations should uphold any already existing runtime panic to make sure usage
/// aborts as early in the program execution as possible.
///
/// # Notes
/// The `RNG` parameter would usually be part of the `mutate` function, similar to the design of
/// the [`super::recombination::Recombination`] and `selection` API, but because we have to
/// convert mutation strategies into trait objects, we can not have generic parameters be part of
/// the function. For more information, see:
/// <https://doc.rust-lang.org/reference/items/traits.html#object-safety>
pub trait Mutation<G, RNG>
where
    G: Genotype,
    RNG: Rng,
{
    /// MISSING DOCS
    fn mutate(&self, genotype: &mut G, rng: &mut RNG);
}
