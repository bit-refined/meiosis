use rand::Rng;

use crate::genotype::Genotype;

/// MISSING DOCS
pub mod single_point;

/// MISSING DOCS
pub trait Recombination<GENOTYPE, const PARENTS: usize = 2, const OFFSPRING: usize = 2>
where
    GENOTYPE: Genotype,
{
    /// This is more or less a hack at the moment. It allows us to refer to the parent amount
    /// without having to actually set the generic parameters when referring to the trait.
    const PARENTS: usize = PARENTS;
    /// This is more or less a hack at the moment. It allows us to refer to the offspring amount
    /// without having to actually set the generic parameters when referring to the trait.
    const OFFSPRING: usize = OFFSPRING;

    /// MISSING DOCS
    fn crossover<RNG>(&self, rng: &mut RNG, parents: [GENOTYPE; PARENTS]) -> [GENOTYPE; OFFSPRING]
    where
        RNG: Rng;
}
