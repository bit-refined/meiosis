use rand::Rng;

use crate::gene::Gene;

use super::Recombination;

/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

/// MISSING DOCS
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
#[non_exhaustive]
pub struct SinglePoint;

impl SinglePoint {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }
}

/// `Vec<T>` has a variable length, so offspring will also have variable length, however never shorter
/// or longer than the parents.
impl<GENE> Recombination<Vec<GENE>> for SinglePoint
where
    GENE: Gene,
{
    // Every use of indexing slicing is behind a length check and construction of new Vec is done
    // using open ranges.
    #[allow(clippy::indexing_slicing)]
    fn crossover<R>(&self, rng: &mut R, parents: [Vec<GENE>; 2]) -> [Vec<GENE>; 2]
    where
        R: Rng,
    {
        if parents[0].is_empty() || parents[1].is_empty() {
            return parents;
        }

        let [mut parent_one, mut parent_two] = parents;

        // We can not cut outside of the length of the shorted Vec, as we might run into the following issue:
        // Parent 1: aaaaaa
        // Parent 2: bbb
        // If our cutting point is now 4, we would end up with these:
        // 1: aaaa aa
        // 2: bbb
        // Switching the ends of 1 and 2, offspring would look like this:
        // "aaaa"
        // "bbb aa"
        // As you can see, we're missing a gene at index 4 for offspring 2.
        // To prevent this from happening, we only ever cut at a point that is inside of both parents.
        let shortest_parent = parent_one.len().min(parent_two.len());

        if shortest_parent == 1 {
            // If one of the parents only has a single gene, then that is all we can work with.
            // In short, the first gene between the two parents gets swapped.
            let [mut offspring1, mut offspring2] = [parent_one, parent_two];
            core::mem::swap(&mut offspring1[0], &mut offspring2[0]);

            return [offspring1, offspring2];
        }

        // We need at least a single item to be switched, so the minimum index is 1.
        // The cutting point index is assumed to be the start of the second offspring.
        let cutting_point_index = rng.gen_range(1..shortest_parent);

        parent_one[..cutting_point_index].swap_with_slice(&mut parent_two[..cutting_point_index]);

        [parent_one, parent_two]
    }
}

/// `[T; N]` has a fixed length, so offspring will also have the same length.
impl<GENE, const GENES: usize> Recombination<[GENE; GENES]> for SinglePoint
where
    GENE: Gene,
{
    // const PARENTS: usize = 2;
    // const OFFSPRING: usize = 2;

    /// # Panic
    /// A genome can never be empty, as compared to a vector it has no chance to ever grow.
    fn crossover<R>(&self, rng: &mut R, parents: [[GENE; GENES]; 2]) -> [[GENE; GENES]; 2]
    where
        R: Rng,
    {
        let [mut parent_one, mut parent_two] = parents;

        assert!(GENES > 0, "can not perform crossover on empty genome");

        if GENES == 1 {
            // If one of the parents only has a single gene, then that is all we can work with.
            // In short, the first gene between the two parents gets swapped.
            let [mut offspring1, mut offspring2] = [parent_one, parent_two];
            core::mem::swap(&mut offspring1[0], &mut offspring2[0]);

            return [offspring1, offspring2];
        }

        // We need at least a single item to be switched, so the minimum index is 1.
        // The cutting point index is assumed to be the start of the second offspring,
        // so we also need an item there.
        let cutting_point_index = rng.gen_range(1..GENES);

        parent_one[cutting_point_index..].swap_with_slice(&mut parent_two[cutting_point_index..]);

        [parent_one, parent_two]
    }
}

/// This implementation is very naive and allocates.
impl Recombination<String> for SinglePoint {
    // const PARENTS: usize = 2;
    // const OFFSPRING: usize = 2;

    fn crossover<R>(&self, rng: &mut R, parents: [String; 2]) -> [String; 2]
    where
        R: Rng,
    {
        let [parent_one, parent_two] = parents;

        // convert strings into char vectors
        let parent1_chars = parent_one.chars().collect::<Vec<_>>();
        let parent2_chars = parent_two.chars().collect();

        // reuse the Vec<DNA> implementation
        let offspring = self.crossover(rng, [parent1_chars, parent2_chars]);

        // convert vectors back to strings
        offspring.map(|vec| vec.into_iter().collect::<String>())
    }
}

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Recombination, SinglePoint};

    #[test]
    fn vector_crossover() {
        let mut rng = SmallRng::seed_from_u64(3);
        let crossover = SinglePoint;

        let parent1 = vec![1, 2, 3, 4];
        let parent2 = vec![5, 6, 7, 8, 9, 10];

        let offspring = crossover.crossover(&mut rng, [parent1, parent2]);

        assert_eq!(offspring, [vec![5, 6, 3, 4], vec![1, 2, 7, 8, 9, 10]]);
    }

    #[test]
    fn vector_crossover_same() {
        let mut rng = SmallRng::seed_from_u64(0);
        let crossover = SinglePoint;

        let parent1 = vec![1, 2, 3];
        let parent2 = vec![1, 2, 3];

        let offspring = crossover.crossover(&mut rng, [parent1, parent2]);

        assert_eq!(offspring[0], offspring[1]);
    }

    #[test]
    fn vector_crossover_simple() {
        let mut rng = SmallRng::seed_from_u64(0);
        let crossover = SinglePoint;

        let parent1 = vec![1, 2];
        let parent2 = vec![3, 4];

        let offspring = crossover.crossover(&mut rng, [parent1, parent2]);

        assert_eq!(offspring, [vec![3, 2], vec![1, 4]]);
    }

    #[test]
    fn array_crossover_same() {
        let mut rng = SmallRng::seed_from_u64(0);
        let crossover = SinglePoint;

        let parent1 = [1, 2, 3];
        let parent2 = [1, 2, 3];

        let offspring = crossover.crossover(&mut rng, [parent1, parent2]);

        assert_eq!(offspring[0], offspring[1]);
    }

    #[test]
    fn array_crossover_simple() {
        let mut rng = SmallRng::seed_from_u64(0);
        let crossover = SinglePoint;

        let parent1 = [1, 2];
        let parent2 = [3, 4];

        let offspring = crossover.crossover(&mut rng, [parent1, parent2]);

        assert_eq!(offspring, [[1, 4], [3, 2]]);
    }
}
