use rand::Rng;

use crate::{
    genotype::{network::Network, Genotype},
    neural::value::Value,
    operators::recombination::{single_point::SinglePoint, Recombination},
};

impl<const IN: usize, const OUT: usize, T, K> Recombination<Network<IN, OUT, T, K>> for SinglePoint
where
    Network<IN, OUT, T, K>: Genotype,
    T: Value,
{
    // const PARENTS: usize = 2;
    // const OFFSPRING: usize = 2;

    fn crossover<RNG>(&self, rng: &mut RNG, parents: [Network<IN, OUT, T, K>; 2]) -> [Network<IN, OUT, T, K>; 2]
    where
        RNG: Rng,
    {
        let [parent_one, parent_two] = parents;

        let Network {
            outputs: mut outputs_one,
            instructions: instructions_one,
            bias: mut bias_one,
        } = parent_one;
        let Network {
            outputs: mut outputs_two,
            instructions: instructions_two,
            bias: mut bias_two,
        } = parent_two;

        if OUT == 1 {
            // if we only have one output we might switch them
            if rng.gen() {
                core::mem::swap(&mut outputs_one, &mut outputs_two);
            }
        } else {
            // we have at least two outputs, so we find a splitting point
            let point = rng.gen_range(1..OUT);

            outputs_one[..point].swap_with_slice(&mut outputs_two[..point]);
        }

        if rng.gen() {
            core::mem::swap(&mut bias_one, &mut bias_two);
        }

        let [new_instructions_one, new_instructions_two] = self.crossover(rng, [instructions_one, instructions_two]);

        let offspring1 = Network {
            outputs: outputs_one,
            instructions: new_instructions_one,
            bias: bias_one,
        };

        let offspring2 = Network {
            outputs: outputs_two,
            instructions: new_instructions_two,
            bias: bias_two,
        };

        [offspring1, offspring2]
    }
}
