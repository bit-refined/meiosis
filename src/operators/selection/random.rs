use super::{NonSelected, Rng, Selected, Selection};
use crate::species::Evaluated;

/// In this strategy we randomly select parents from the existing population.
/// There is no selection pressure towards fitter individuals and therefore this strategy is usually avoided.
struct Random;

impl<GENOTYPE> Selection<GENOTYPE> for Random {
    fn select<RNG>(
        &self,
        rng: &mut RNG,
        population: Vec<Evaluated<GENOTYPE>>,
    ) -> (Selected<GENOTYPE>, NonSelected<GENOTYPE>)
    where
        RNG: Rng,
    {
        population.into_iter().partition(|_g| rng.gen())
    }
}

#[cfg(test)]
mod tests {
    use crate::{phenotype::Phenotype, species::Evaluated};
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Random, Selection};

    #[test]
    fn selection() {
        impl Phenotype for Vec<i32> {
            type Genotype = Vec<i32>;
        }

        let mut rng = SmallRng::seed_from_u64(2);
        let selection = Random;

        let population = vec![0, 1, 2, 3, 4, 5, 6]
            .into_iter()
            .map(|val| Evaluated {
                genotype: vec![val],
                fitness: 0.5,
                adjusted_fitness: 0.0,
            })
            .collect();

        let (selected, non_selected) = selection.select(&mut rng, population);

        let selected_values = selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        let non_selected_values = non_selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        assert_eq!(selected_values, vec![0, 2, 4, 5]);
        assert_eq!(non_selected_values, vec![1, 3, 6]);
    }
}
