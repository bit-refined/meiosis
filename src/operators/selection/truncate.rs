use crate::{
    operators::selection::{NonSelected, Selected, Selection},
    species::Evaluated,
};
use rand::Rng;

// # Notes
// As this selection strategy is meant to be applied to the whole population at once, it typically
// would not have a `select_single` implementation and thus panic if used. However, because
// truncation with one individual is equivalent to elitism with one individual, it is implemented
// like that for convinience reasons.
/// # Aliases
/// Maximize Selector
/// # External resources
/// <https://en.wikipedia.org/wiki/Truncation_selection>
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Truncation {
    /// This value dictates in percent as a value from 0.0 to 1.0 how much of the population to keep.
    ///
    /// Given that this is a ratio, the amount of individuals to keep might not calculate to an integer
    /// value, in which case we round down. Say you have 10 individuals, and you want to keep 25%, then
    /// the selection will select 2.
    ///
    /// A similar restriction happens when there are individuals with the same fitness right on the
    /// boundary. As a result, equally good individuals may not be selected.
    ///
    /// # Note
    /// This selection strategy does not use a random number generator - it's output is always
    /// deterministic. The desirability of this property depends on the use-case and objective.
    proportion_to_keep: f64,
}

impl Truncation {
    /// MISSING DOCS
    /// # Panics
    /// Setting this to a value outside of the accepted bound `[0.0, 1.0]` will panic.
    #[must_use]
    pub fn new(proportion_to_keep: f64) -> Self {
        assert!(
            proportion_to_keep >= 0.0_f64,
            "proportion to keep has to be above or equal 0.0 but is {proportion_to_keep}",
        );
        assert!(
            proportion_to_keep <= 1.0_f64,
            "proportion to keep has to be below or equal 1.0 but is {proportion_to_keep}",
        );

        Self { proportion_to_keep }
    }
}

impl<GENOTYPE> Selection<GENOTYPE> for Truncation {
    fn select<RNG>(
        &self,
        _rng: &mut RNG,
        mut population: Vec<Evaluated<GENOTYPE>>,
    ) -> (Selected<GENOTYPE>, NonSelected<GENOTYPE>)
    where
        RNG: Rng,
    {
        // as unlikely as the two following cases might be, it can't hurt to early-return

        // no members were selected
        if self.proportion_to_keep == 0.0_f64 {
            return (Vec::new(), population);
        }

        // this is fine, given that we assert valid value ranges
        #[allow(clippy::float_arithmetic)]
        // all members were selected
        if (self.proportion_to_keep - 1.0).abs() < f64::EPSILON {
            return (population, Vec::new());
        }

        let population_size = population.len();

        // using `as` this liberal is pretty bad, but I don't have time right now, will enhance later
        #[allow(clippy::cast_precision_loss, clippy::float_arithmetic)]
        // this is fine as the original value converted to f64 was positive anyway
        #[allow(clippy::cast_sign_loss)]
        // this is intended
        #[allow(clippy::cast_possible_truncation, clippy::as_conversions)]
        let passing_members = (population_size as f64 * self.proportion_to_keep) as usize; // "as usize" automatically rounds down for us

        #[allow(clippy::expect_used)] // In its current state, we *want* panics.
        population.sort_unstable_by(|a, b| {
            b.fitness
                .partial_cmp(&a.fitness)
                .expect("f64 will be replaced by a generic in the future, till then this should hopefully never panic")
        });

        let non_selected = population.split_off(passing_members);

        (population, non_selected)
    }

    // /// Selects the best individual from the population. This is equivalent to elitism of one.
    // /// This method always returns the first individual, should all individuals have the same fitness.
    // fn select_single<R>(
    //     &self,
    //     mut population: Vec<Evaluated<Genotype>>,
    //     _rng: &mut R,
    // ) -> (Evaluated<Genotype>, NonSelected<Genotype>)
    // where
    //     R: Rng,
    // {
    //     // find the element with the highest fitness
    //     let mut best_member_index = 0;
    //     let mut best_member_fitness = 0.;
    //
    //     for (index, member) in population.iter().enumerate() {
    //         // if it is better than an already checked member, store it for later use
    //         if member.fitness > best_member_fitness {
    //             best_member_index = index;
    //             best_member_fitness = member.fitness;
    //         }
    //     }
    //
    //     let best_member = population.remove(best_member_index);
    //
    //     (best_member, population)
    // }
}

#[cfg(test)]
mod tests {
    use crate::species::Evaluated;
    use rand::{rngs::SmallRng, SeedableRng};

    use super::{Selection, Truncation};

    #[test]
    fn selection_50_percent() {
        let mut rng = SmallRng::seed_from_u64(0);
        let selection = Truncation {
            proportion_to_keep: 0.5,
        };

        let population = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            .into_iter()
            .map(|val| Evaluated {
                genotype: vec![val],
                fitness: 0.1 * f64::from(val),
                adjusted_fitness: 0.0,
            })
            .collect();

        let (selected, non_selected) = selection.select(&mut rng, population);

        let selected_values = selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        let non_selected_values = non_selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        assert_eq!(selected_values, vec![10, 9, 8, 7, 6]);
        assert_eq!(non_selected_values, vec![5, 4, 3, 2, 1]);
    }

    #[test]
    fn selection_25_percent() {
        let mut rng = SmallRng::seed_from_u64(0);
        let selection = Truncation {
            proportion_to_keep: 0.25,
        };

        let population = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            .into_iter()
            .map(|val| Evaluated {
                genotype: vec![val],
                fitness: 0.1 * f64::from(val),
                adjusted_fitness: 0.0,
            })
            .collect();

        let (selected, non_selected) = selection.select(&mut rng, population);

        let selected_values = selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        let non_selected_values = non_selected.into_iter().flat_map(|ev| ev.genotype).collect::<Vec<_>>();

        assert_eq!(selected_values, vec![10_i32, 9]);
        assert_eq!(non_selected_values, vec![8_i32, 7, 6, 5, 4, 3, 2, 1]);
    }
}
