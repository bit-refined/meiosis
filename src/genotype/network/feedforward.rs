use rand::Rng;

use crate::{
    gene::network::feedforward::Instruction,
    genotype::{
        network::{Feedforward, Network},
        Genotype,
    },
    neural::{neuron::Neuron, value::Value},
    random::Random,
};

impl<const IN: usize, const OUT: usize, T> Genotype for Network<IN, OUT, T, Feedforward>
where
    T: Value,
{
    type Gene = Instruction<T>;
}

/// This implementation generates a fully connected feedforward network without any layers,
/// but keeps all activation functions to the default identity function.
impl<const IN: usize, const OUT: usize, T> Random for Network<IN, OUT, T, Feedforward>
where
    T: Value + Random,
{
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized,
    {
        let mut instructions = Vec::new();

        for input in 0..IN {
            if input > 0 {
                instructions.push(Instruction::NextOrigin);
            }

            for output in 0..OUT {
                if output > 0 {
                    instructions.push(Instruction::NextOutput);
                }
                instructions.push(Instruction::ConnectTo);
                instructions.push(Instruction::Weight(T::random(rng)));
            }

            #[allow(clippy::arithmetic_side_effects)]
            let reset = core::iter::once(Instruction::PreviousOutput).cycle().take(OUT - 1);
            instructions.extend(reset);
        }

        Self {
            outputs: core::array::from_fn(|_| Neuron::default()),
            instructions,
            bias: T::random(rng),
        }
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use rand::{rngs::SmallRng, SeedableRng};

    use crate::{
        gene::network::feedforward::Instruction,
        genotype::network::{Feedforward, Network as NetworkGenotype},
        neural::network::Network,
        phenotype::FromGenotype,
        random::Random,
    };

    #[test]
    fn simple_random_feedforward() {
        let mut rng = SmallRng::seed_from_u64(0);
        let genotype = NetworkGenotype::<1, 1, f64, Feedforward>::random(&mut rng);

        let expected_instructions = vec![Instruction::ConnectTo, Instruction::Weight(0.447_325_038_120_276_35)];

        assert_eq!(expected_instructions, genotype.instructions);

        // To verify that our instruction set produces the network we want, we render it using Mermaid.
        let phenotype = Network::from_genotype(&genotype, &(), &mut rng);

        let expected = r#"graph LR
	subgraph Inputs
		direction TB
		i0(("Input 0"))
	end

	subgraph Outputs
		direction TB
		o0(("Output 0<br>Identity: x"))
	end

	i0-- "0.44732503812027635" --> o0"#;

        assert_eq!(expected, phenotype.render_mermaid());
    }

    #[test]
    #[allow(clippy::too_many_lines)]
    fn random_feedforward() {
        let mut rng = SmallRng::seed_from_u64(0);
        let genotype = NetworkGenotype::<4, 7, f64, Feedforward>::random(&mut rng);

        let expected_instructions = vec![
            Instruction::ConnectTo,
            Instruction::Weight(0.447_325_038_120_276_35),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.439_140_270_900_856_4),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.979_880_251_245_095_6),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.462_167_227_291_666_8),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.897_079_022_279_925_3),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.942_949_831_320_834_5),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.588_147_506_808_707_5),
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::NextOrigin,
            Instruction::ConnectTo,
            Instruction::Weight(0.456_371_967_112_629_84),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.395_144_166_898_645_07),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.818_850_944_859_681_4),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.240_907_377_849_783_78),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.976_381_911_235_181_7),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.644_516_786_145_846_7),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.054_609_122_883_568_006),
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::NextOrigin,
            Instruction::ConnectTo,
            Instruction::Weight(0.921_572_188_966_583_5),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.225_107_514_275_483_03),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.232_095_618_246_201_5),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.296_974_370_392_482_53),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.787_838_282_856_479_4),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.724_292_066_430_471_4),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.582_143_409_538_771_5),
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::NextOrigin,
            Instruction::ConnectTo,
            Instruction::Weight(0.186_416_121_877_369_9),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.253_926_038_484_92),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.864_587_667_941_046_6),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.390_336_558_080_199_3),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.900_556_511_316_377_9),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.971_217_789_037_264_1),
            Instruction::NextOutput,
            Instruction::ConnectTo,
            Instruction::Weight(0.035_269_129_241_661_035),
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
            Instruction::PreviousOutput,
        ];

        assert_eq!(expected_instructions, genotype.instructions);

        // To verify that our instruction set produces the network we want, we render it using Mermaid.
        let phenotype = Network::from_genotype(&genotype, &(), &mut rng);

        let expected = r#"graph LR
	subgraph Inputs
		direction TB
		i0(("Input 0"))
		i1(("Input 1"))
		i2(("Input 2"))
		i3(("Input 3"))
	end

	subgraph Outputs
		direction TB
		o0(("Output 0<br>Identity: x"))
		o1(("Output 1<br>Identity: x"))
		o2(("Output 2<br>Identity: x"))
		o3(("Output 3<br>Identity: x"))
		o4(("Output 4<br>Identity: x"))
		o5(("Output 5<br>Identity: x"))
		o6(("Output 6<br>Identity: x"))
	end

	i0-- "0.44732503812027635" --> o0
	i0-- "0.4391402709008564" --> o1
	i0-- "0.9798802512450956" --> o2
	i0-- "0.4621672272916668" --> o3
	i0-- "0.8970790222799253" --> o4
	i0-- "0.9429498313208345" --> o5
	i0-- "0.5881475068087075" --> o6
	i1-- "0.45637196711262984" --> o0
	i1-- "0.39514416689864507" --> o1
	i1-- "0.8188509448596814" --> o2
	i1-- "0.24090737784978378" --> o3
	i1-- "0.9763819112351817" --> o4
	i1-- "0.6445167861458467" --> o5
	i1-- "0.054609122883568006" --> o6
	i2-- "0.9215721889665835" --> o0
	i2-- "0.22510751427548303" --> o1
	i2-- "0.2320956182462015" --> o2
	i2-- "0.29697437039248253" --> o3
	i2-- "0.7878382828564794" --> o4
	i2-- "0.7242920664304714" --> o5
	i2-- "0.5821434095387715" --> o6
	i3-- "0.1864161218773699" --> o0
	i3-- "0.25392603848492" --> o1
	i3-- "0.8645876679410466" --> o2
	i3-- "0.3903365580801993" --> o3
	i3-- "0.9005565113163779" --> o4
	i3-- "0.9712177890372641" --> o5
	i3-- "0.035269129241661035" --> o6"#;

        assert_eq!(expected, phenotype.render_mermaid());
    }
}
