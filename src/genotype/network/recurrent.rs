use crate::{
    gene::network::recurrent::Instruction,
    genotype::{
        network::{Network, Recurrent},
        Genotype,
    },
    neural::{neuron::Neuron, value::Value},
    random::Random,
};
use rand::Rng;

impl<const IN: usize, const OUT: usize, T> Genotype for Network<IN, OUT, T, Recurrent>
where
    T: Value,
{
    type Gene = Instruction<T>;
}

/// This implementation generates a fully connected recurrent network,
/// but keeps all activation functions to the default identity function.
impl<const IN: usize, const OUT: usize, T> Random for Network<IN, OUT, T, Recurrent>
where
    T: Value + Random,
{
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized,
    {
        let mut instructions = Vec::new();

        for input in 0..IN {
            if input > 0 {
                instructions.push(Instruction::NextOrigin);
            }

            for output in 0..OUT {
                if output > 0 {
                    instructions.push(Instruction::NextDestination);
                }
                instructions.push(Instruction::Connect);
                instructions.push(Instruction::Weight(T::random(rng)));
            }

            #[allow(clippy::arithmetic_side_effects)]
            let reset = core::iter::once(Instruction::PreviousDestination).cycle().take(OUT - 1);
            instructions.extend(reset);
        }

        Self {
            outputs: core::array::from_fn(|_| Neuron::default()),
            instructions,
            bias: T::random(rng),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        gene::network::recurrent::Instruction,
        genotype::network::{Network as NetworkGenotype, Recurrent},
        neural::network::Network,
        phenotype::FromGenotype,
        random::Random,
    };
    use pretty_assertions::assert_eq;
    use rand::{rngs::SmallRng, SeedableRng};

    #[test]
    fn simple_random_recurrent() {
        let mut rng = SmallRng::seed_from_u64(0);
        let genotype = NetworkGenotype::<1, 1, f64, Recurrent>::random(&mut rng);

        let expected_instructions = vec![Instruction::Connect, Instruction::Weight(0.447_325_038_120_276_35)];

        assert_eq!(expected_instructions, genotype.instructions);

        // To verify that our instruction set produces the network we want, we render it using Mermaid.
        let phenotype = Network::from_genotype(&genotype, &(), &mut rng);

        let expected = r#"graph LR
	subgraph Inputs
		direction TB
		i0(("Input 0"))
	end

	subgraph Outputs
		direction TB
		o0(("Output 0<br>Identity: x"))
	end

	i0-- "0.44732503812027635" --> o0"#;

        assert_eq!(expected, phenotype.render_mermaid());
    }

    #[test]
    #[allow(clippy::too_many_lines)]
    fn random_recurrent() {
        let mut rng = SmallRng::seed_from_u64(0);
        let genotype = NetworkGenotype::<7, 4, f64, Recurrent>::random(&mut rng);

        let expected_instructions = vec![
            Instruction::Connect,
            Instruction::Weight(0.447_325_038_120_276_35),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.439_140_270_900_856_4),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.979_880_251_245_095_6),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.462_167_227_291_666_8),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.897_079_022_279_925_3),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.942_949_831_320_834_5),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.588_147_506_808_707_5),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.456_371_967_112_629_84),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.395_144_166_898_645_07),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.818_850_944_859_681_4),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.240_907_377_849_783_78),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.976_381_911_235_181_7),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.644_516_786_145_846_7),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.054_609_122_883_568_006),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.921_572_188_966_583_5),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.225_107_514_275_483_03),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.232_095_618_246_201_5),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.296_974_370_392_482_53),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.787_838_282_856_479_4),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.724_292_066_430_471_4),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.582_143_409_538_771_5),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.186_416_121_877_369_9),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.253_926_038_484_92),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.864_587_667_941_046_6),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::NextOrigin,
            Instruction::Connect,
            Instruction::Weight(0.390_336_558_080_199_3),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.900_556_511_316_377_9),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.971_217_789_037_264_1),
            Instruction::NextDestination,
            Instruction::Connect,
            Instruction::Weight(0.035_269_129_241_661_035),
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
            Instruction::PreviousDestination,
        ];

        assert_eq!(expected_instructions, genotype.instructions);

        // To verify that our instruction set produces the network we want, we render it using Mermaid.
        let phenotype = Network::from_genotype(&genotype, &(), &mut rng);

        let expected = r#"graph LR
	subgraph Inputs
		direction TB
		i0(("Input 0"))
		i1(("Input 1"))
		i2(("Input 2"))
		i3(("Input 3"))
		i4(("Input 4"))
		i5(("Input 5"))
		i6(("Input 6"))
	end

	subgraph Outputs
		direction TB
		o0(("Output 0<br>Identity: x"))
		o1(("Output 1<br>Identity: x"))
		o2(("Output 2<br>Identity: x"))
		o3(("Output 3<br>Identity: x"))
	end

	i0-- "0.44732503812027635" --> o0
	i0-- "0.4391402709008564" --> o1
	i0-- "0.9798802512450956" --> o2
	i0-- "0.4621672272916668" --> o3
	i1-- "0.8970790222799253" --> o0
	i1-- "0.9429498313208345" --> o1
	i1-- "0.5881475068087075" --> o2
	i1-- "0.45637196711262984" --> o3
	i2-- "0.39514416689864507" --> o0
	i2-- "0.8188509448596814" --> o1
	i2-- "0.24090737784978378" --> o2
	i2-- "0.9763819112351817" --> o3
	i3-- "0.6445167861458467" --> o0
	i3-- "0.054609122883568006" --> o1
	i3-- "0.9215721889665835" --> o2
	i3-- "0.22510751427548303" --> o3
	i4-- "0.2320956182462015" --> o0
	i4-- "0.29697437039248253" --> o1
	i4-- "0.7878382828564794" --> o2
	i4-- "0.7242920664304714" --> o3
	i5-- "0.5821434095387715" --> o0
	i5-- "0.1864161218773699" --> o1
	i5-- "0.25392603848492" --> o2
	i5-- "0.8645876679410466" --> o3
	i6-- "0.3903365580801993" --> o0
	i6-- "0.9005565113163779" --> o1
	i6-- "0.9712177890372641" --> o2
	i6-- "0.035269129241661035" --> o3"#;

        assert_eq!(expected, phenotype.render_mermaid());
    }
}
