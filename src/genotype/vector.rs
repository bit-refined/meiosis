use crate::genotype::Distance;
use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

use crate::random::Random;

use super::{Gene, Genotype};

impl<T> Genotype for Vec<T>
where
    T: Gene,
{
    type Gene = T;
}

impl<T> Distance for Vec<T>
where
    T: Gene + PartialEq,
{
    /// We use the hamming distance until we run out of elements on the shortest [`Vec`],
    /// after which the remaining distance is added on top, essentially weighting missing
    /// genes the same distance as differing ones.
    #[allow(clippy::float_arithmetic, clippy::cast_precision_loss)]
    fn distance(&self, other: &Self) -> f32 {
        #[allow(clippy::as_conversions)]
        let size_difference = self.len().abs_diff(other.len()) as f32;
        let gene_difference = self
            .iter()
            .zip(other)
            .fold(0.0, |distance, (l, r)| distance + if l == r { 0. } else { 1. });

        #[allow(clippy::as_conversions)]
        let max_size = self.len().max(other.len()) as f32;

        (size_difference + gene_difference) / max_size
    }
}

/// Typically, to uniformly generate a random [Vec], we'd have to allow [`usize::MAX`] characters.
/// This is not only going to use a lot of memory, it is also very slow.
///
/// To prevent all that, instead we start with a single entry and rely on [`crate::operators::mutation::Mutation`]
/// to grow the [Vec] over time. This is still relatively slow if the goal is to find
/// rather large [Vec]s, but better than trying to allocate all of the system memory.
impl<T> Random for Vec<T>
where
    Standard: Distribution<T>,
{
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized,
    {
        let entry: T = rng.gen();

        vec![entry]
    }
}

#[cfg(test)]
mod tests {
    use crate::genotype::Distance;

    #[test]
    fn distance_same() {
        // the genomes share a single gene out of 3, so their distance is 2/3
        let arr1 = vec![1, 2, 3];
        let arr2 = vec![1, 2, 3];

        let distance = arr1.distance(&arr2);

        assert!((distance - 0.0).abs() < 0.0001)
    }

    #[test]
    fn distance_same_size() {
        // the genomes share a single gene out of 3, so their distance is 2/3
        let arr1 = vec![1, 2, 3];
        let arr2 = vec![3, 2, 1];

        let distance = arr1.distance(&arr2);

        assert!((distance - 0.66666).abs() < 0.0001)
    }

    #[test]
    fn distance_different_size() {
        // the genomes share 3 genes out of 5, so their distance is 2/5
        let arr1 = vec![1, 2, 3];
        let arr2 = vec![1, 2, 3, 4, 5];

        let distance = arr1.distance(&arr2);

        assert!((distance - 0.4).abs() < 0.0001)
    }

    #[test]
    fn distance_completely_different() {
        // the genomes share 3 genes out of 5, so their distance is 2/5
        let arr1 = vec![1, 2, 3];
        let arr2 = vec![3, 4, 5, 6, 7];

        let distance = arr1.distance(&arr2);

        assert!((distance - 1.0).abs() < 0.0001)
    }
}
