use crate::{
    genotype::{Distance, Genotype},
    neural::{neuron::Neuron, value::Value},
};

/// MISSING DOCS
pub mod feedforward;
/// MISSING DOCS
pub mod recurrent;

/// This is a marker struct used to declare feedforward network genotypes.
#[derive(Copy, Clone, Debug)]
#[allow(clippy::exhaustive_structs)]
pub struct Feedforward;

/// This is a marker struct used to declare recurrent network genotypes.
#[derive(Copy, Clone, Debug)]
#[allow(clippy::exhaustive_structs)]
pub struct Recurrent;

/// MISSING DOCS
#[derive(Clone, Debug, PartialEq)]
pub struct Network<const IN: usize, const OUT: usize, T, K>
where
    T: Value,
    Self: Genotype,
{
    /// These kinds of neurons can/should not be constructed using variable instructions, but are
    /// essential for the network to function, so we store them separately. Mutations and crossover
    /// can still manipulate them, for example the activation function.
    pub(crate) outputs: [Neuron<T>; OUT],
    /// MISSING DOCS
    pub(crate) instructions: Vec<<Self as Genotype>::Gene>,
    /// MISSING DOCS
    pub(crate) bias: T,
}

impl<const IN: usize, const OUT: usize, T, K> Network<IN, OUT, T, K>
where
    Self: Genotype,
    T: Value,
{
    /// MISSING DOCS
    pub fn instructions(&self) -> &[<Self as Genotype>::Gene] {
        &self.instructions
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the network has no in- or outputs.
    #[must_use]
    pub fn empty() -> Self {
        assert!(IN > 0, "network requires at least one input");
        assert!(OUT > 0, "network requires at least one output");

        Self {
            outputs: core::array::from_fn(|_| Neuron::default()),
            instructions: vec![],
            bias: T::zero(),
        }
    }

    /// MISSING DOCS
    #[must_use]
    pub fn feedforward() -> Network<IN, OUT, T, Feedforward> {
        Network::<IN, OUT, T, Feedforward>::empty()
    }

    /// MISSING DOCS
    #[must_use]
    pub fn recurrent() -> Network<IN, OUT, T, Recurrent> {
        Network::<IN, OUT, T, Recurrent>::empty()
    }
}

impl<const IN: usize, const OUT: usize, T, K> Distance for Network<IN, OUT, T, K>
where
    T: Value,
    Self: Genotype,
{
    #[allow(
        clippy::cast_precision_loss,
        clippy::float_arithmetic,
        clippy::arithmetic_side_effects,
        clippy::as_conversions
    )]
    fn distance(&self, other: &Self) -> f32 {
        let max_distance = OUT // outputs
            + 1 // bias
            + self.instructions.len().max(other.instructions.len());

        let mut differing_genes = self.instructions.len().abs_diff(other.instructions.len());
        for (s, o) in self.outputs.iter().zip(other.outputs.iter()) {
            differing_genes =
                differing_genes.saturating_add(usize::from(s.activation_function.ne(&o.activation_function)));
        }

        differing_genes = differing_genes.saturating_add(usize::from(self.bias == other.bias));

        for (s, o) in self.instructions.iter().zip(other.instructions.iter()) {
            if s != o {
                differing_genes = differing_genes.saturating_add(1);
            }
        }

        differing_genes as f32 / max_distance as f32
    }
}
