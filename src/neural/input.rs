/// This trait is used to allow users to keep their inputs structured, converting them into an
/// array for the network network on the fly.
///
/// This trait has a blanket implementation for arrays of the correct size, so if there is no
/// abstraction needed, there is no additional code to get those values.
#[allow(clippy::module_name_repetitions)]
pub trait ToNetworkInput<T, const IN: usize> {
    /// Converts the type this is implemented for to inputs for the neural network in form of an array.
    fn to_network_input(self) -> [T; IN];
}

/// This implementation allows direct input of arrays into the neural network.
impl<T, const IN: usize> ToNetworkInput<T, IN> for [T; IN] {
    fn to_network_input(self) -> [T; IN] {
        self
    }
}

/// If the neural network only has one input, the type can be inserted directly
/// without having to create one-sized arrays thanks to this implementation.
impl<T> ToNetworkInput<T, 1> for T {
    fn to_network_input(self) -> [T; 1] {
        [self]
    }
}
