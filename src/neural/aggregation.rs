/// MISSING DOCS
pub mod sum;

/// MISSING DOCS
#[derive(Debug)]
#[non_exhaustive]
pub enum Aggregation<T> {
    /// Summation of input values.
    Sum(sum::Sum<T>),
}

/// MISSING DOCS
#[allow(clippy::module_name_repetitions)]
pub trait AggregationFunction {
    /// The type this function will receive as inputs in a slice.
    type Input;
    /// The type this function will return, that activation functions require for activation.
    type Output;
    /// Calculates the result of the aggregation function given any input.
    fn aggregate(&self, input: &[Self::Input]) -> Self::Input;
}
