use core::{
    fmt::{Display, Formatter},
    marker::PhantomData,
};

use crate::neural::{aggregation::AggregationFunction, value::Value};

/// MISSING DOCS
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
#[non_exhaustive]
pub struct Sum<T> {
    /// MISSING DOCS
    pub(crate) marker: PhantomData<T>,
}

impl<T> Sum<T> {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self {
            marker: PhantomData::<T>,
        }
    }
}

impl<T> AggregationFunction for Sum<T>
where
    T: Value,
{
    type Input = T;
    type Output = T;

    #[allow(clippy::arithmetic_side_effects)]
    fn aggregate(&self, input: &[T]) -> T {
        input
            .iter()
            .cloned()
            .reduce(|acc, value| acc + value)
            .unwrap_or_else(|| T::zero())
    }
}

impl<T> Display for Sum<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("Sum")
    }
}
