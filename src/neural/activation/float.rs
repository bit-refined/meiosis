use core::{
    fmt::{Display, Formatter},
    hash::{Hash, Hasher},
};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

use crate::neural::activation::{
    binary::SummationBinary,
    gaussian::SummationGaussian,
    identity::SummationIdentity,
    linear::SummationLinear,
    linear_unit::{
        SummationExponentialLinearUnit, SummationLeakyRectifiedLinearUnit, SummationParametricRectifiedLinearUnit,
        SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
    },
    logistic::SummationLogistic,
    mish::SummationMish,
    softplus::SummationSoftPlus,
    tangent::SummationHyperbolicTangent,
    ActivationFunction as AF, ActivationType,
};

impl ActivationType for f32 {
    type Function = ActivationFunction<f32>;
}

impl ActivationType for f64 {
    type Function = ActivationFunction<f64>;
}

#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
/// Built-in activation function type for floats.
#[derive(Clone, Debug, Eq, PartialEq)]
#[non_exhaustive]
pub enum ActivationFunction<T> {
    /// MISSING DOCS
    Binary(SummationBinary),
    /// MISSING DOCS
    Gaussian(SummationGaussian),
    /// MISSING DOCS
    Identity(SummationIdentity),
    /// MISSING DOCS
    Linear(SummationLinear<T>),
    /// MISSING DOCS
    RectifiedLinearUnit(SummationRectifiedLinearUnit),
    /// MISSING DOCS
    ParametricRectifiedLinearUnit(SummationParametricRectifiedLinearUnit<T>),
    /// MISSING DOCS
    LeakyRectifiedLinearUnit(SummationLeakyRectifiedLinearUnit),
    /// MISSING DOCS
    SigmoidLinearUnit(SummationSigmoidLinearUnit),
    /// MISSING DOCS
    Logistic(SummationLogistic),
    /// MISSING DOCS
    Mish(SummationMish),
    /// MISSING DOCS
    SoftPlus(SummationSoftPlus),
    /// MISSING DOCS
    HyperbolicTangent(SummationHyperbolicTangent),
    /// MISSING DOCS
    ExponentialLinearUnit(SummationExponentialLinearUnit<T>),
}

impl AF<f32> for ActivationFunction<f32> {
    fn activate(&self, inputs: &[f32]) -> f32 {
        match *self {
            Self::Binary(ref a) => a.activate(inputs),
            Self::Identity(ref a) => a.activate(inputs),
            Self::Gaussian(ref a) => a.activate(inputs),
            Self::Linear(ref a) => a.activate(inputs),
            Self::RectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::ParametricRectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::LeakyRectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::SigmoidLinearUnit(ref a) => a.activate(inputs),
            Self::Logistic(ref a) => a.activate(inputs),
            Self::Mish(ref a) => a.activate(inputs),
            Self::SoftPlus(ref a) => a.activate(inputs),
            Self::HyperbolicTangent(ref a) => a.activate(inputs),
            ActivationFunction::ExponentialLinearUnit(ref a) => a.activate(inputs),
        }
    }
}

impl AF<f64> for ActivationFunction<f64> {
    fn activate(&self, inputs: &[f64]) -> f64 {
        match *self {
            Self::Binary(ref a) => a.activate(inputs),
            Self::Identity(ref a) => a.activate(inputs),
            Self::Gaussian(ref a) => a.activate(inputs),
            Self::Linear(ref a) => a.activate(inputs),
            Self::RectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::ParametricRectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::LeakyRectifiedLinearUnit(ref a) => a.activate(inputs),
            Self::SigmoidLinearUnit(ref a) => a.activate(inputs),
            Self::Logistic(ref a) => a.activate(inputs),
            Self::Mish(ref a) => a.activate(inputs),
            Self::SoftPlus(ref a) => a.activate(inputs),
            Self::HyperbolicTangent(ref a) => a.activate(inputs),
            ActivationFunction::ExponentialLinearUnit(ref a) => a.activate(inputs),
        }
    }
}

impl<T> Default for ActivationFunction<T> {
    fn default() -> Self {
        ActivationFunction::Identity(SummationIdentity::new())
    }
}

impl Display for ActivationFunction<f32> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match *self {
            Self::Binary(ref a) => Display::fmt(a, f),
            Self::Identity(ref a) => Display::fmt(a, f),
            Self::Gaussian(ref a) => Display::fmt(a, f),
            Self::Linear(ref a) => Display::fmt(a, f),
            Self::RectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::ParametricRectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::LeakyRectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::SigmoidLinearUnit(ref a) => Display::fmt(a, f),
            Self::Logistic(ref a) => Display::fmt(a, f),
            Self::Mish(ref a) => Display::fmt(a, f),
            Self::SoftPlus(ref a) => Display::fmt(a, f),
            Self::HyperbolicTangent(ref a) => Display::fmt(a, f),
            ActivationFunction::ExponentialLinearUnit(ref a) => Display::fmt(a, f),
        }
    }
}

impl Display for ActivationFunction<f64> {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match *self {
            Self::Binary(ref a) => Display::fmt(a, f),
            Self::Identity(ref a) => Display::fmt(a, f),
            Self::Gaussian(ref a) => Display::fmt(a, f),
            Self::Linear(ref a) => Display::fmt(a, f),
            Self::RectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::ParametricRectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::LeakyRectifiedLinearUnit(ref a) => Display::fmt(a, f),
            Self::SigmoidLinearUnit(ref a) => Display::fmt(a, f),
            Self::Logistic(ref a) => Display::fmt(a, f),
            Self::Mish(ref a) => Display::fmt(a, f),
            Self::SoftPlus(ref a) => Display::fmt(a, f),
            Self::HyperbolicTangent(ref a) => Display::fmt(a, f),
            ActivationFunction::ExponentialLinearUnit(ref a) => Display::fmt(a, f),
        }
    }
}

impl Hash for ActivationFunction<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            ActivationFunction::Binary(ref a) => {
                state.write_u8(0);
                a.hash(state);
            }
            ActivationFunction::Gaussian(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            ActivationFunction::Identity(ref a) => {
                state.write_u8(2);
                a.hash(state);
            }
            ActivationFunction::Linear(ref a) => {
                state.write_u8(3);
                a.hash(state);
            }
            ActivationFunction::RectifiedLinearUnit(ref a) => {
                state.write_u8(4);
                a.hash(state);
            }
            ActivationFunction::ParametricRectifiedLinearUnit(ref a) => {
                state.write_u8(5);
                a.hash(state);
            }
            ActivationFunction::LeakyRectifiedLinearUnit(ref a) => {
                state.write_u8(6);
                a.hash(state);
            }
            ActivationFunction::SigmoidLinearUnit(ref a) => {
                state.write_u8(7);
                a.hash(state);
            }
            ActivationFunction::Logistic(ref a) => {
                state.write_u8(8);
                a.hash(state);
            }
            ActivationFunction::Mish(ref a) => {
                state.write_u8(9);
                a.hash(state);
            }
            ActivationFunction::SoftPlus(ref a) => {
                state.write_u8(10);
                a.hash(state);
            }
            ActivationFunction::HyperbolicTangent(ref a) => {
                state.write_u8(11);
                a.hash(state);
            }
            ActivationFunction::ExponentialLinearUnit(ref a) => {
                state.write_u8(12);
                a.hash(state);
            }
        }
    }
}

impl Hash for ActivationFunction<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            ActivationFunction::Binary(ref a) => {
                state.write_u8(0);
                a.hash(state);
            }
            ActivationFunction::Gaussian(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            ActivationFunction::Identity(ref a) => {
                state.write_u8(2);
                a.hash(state);
            }
            ActivationFunction::Linear(ref a) => {
                state.write_u8(3);
                a.hash(state);
            }
            ActivationFunction::RectifiedLinearUnit(ref a) => {
                state.write_u8(4);
                a.hash(state);
            }
            ActivationFunction::ParametricRectifiedLinearUnit(ref a) => {
                state.write_u8(5);
                a.hash(state);
            }
            ActivationFunction::LeakyRectifiedLinearUnit(ref a) => {
                state.write_u8(6);
                a.hash(state);
            }
            ActivationFunction::SigmoidLinearUnit(ref a) => {
                state.write_u8(7);
                a.hash(state);
            }
            ActivationFunction::Logistic(ref a) => {
                state.write_u8(8);
                a.hash(state);
            }
            ActivationFunction::Mish(ref a) => {
                state.write_u8(9);
                a.hash(state);
            }
            ActivationFunction::SoftPlus(ref a) => {
                state.write_u8(10);
                a.hash(state);
            }
            ActivationFunction::HyperbolicTangent(ref a) => {
                state.write_u8(11);
                a.hash(state);
            }
            ActivationFunction::ExponentialLinearUnit(ref a) => {
                state.write_u8(12);
                a.hash(state);
            }
        }
    }
}
