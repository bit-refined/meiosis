use crate::neural::{
    activation::{
        mish::SummationMish, softplus::SummationSoftPlus, tangent::SummationHyperbolicTangent, ActivationFunction,
    },
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationMish {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        let softplus = SummationSoftPlus;
        let hyperbolic = SummationHyperbolicTangent;

        input * hyperbolic.activate(&[softplus.activate(inputs)])
    }
}
