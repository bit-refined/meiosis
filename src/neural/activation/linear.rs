use core::{
    fmt::{Display, Formatter},
    hash::{Hash, Hasher},
};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

/// Built-in implementation for `f32`.
mod f32;
/// Built-in implementation for `f64`.
mod f64;

/// MISSING DOCS
#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[allow(clippy::module_name_repetitions)]
pub struct SummationLinear<T> {
    /// MISSING DOCS
    slope: T,
    /// MISSING DOCS
    y_intercept: T,
}

impl<T> SummationLinear<T> {
    /// MISSING DOCS
    pub const fn new(slope: T, y_intercept: T) -> Self {
        Self { slope, y_intercept }
    }

    /// MISSING DOCS
    pub fn new_boxed(slope: T, y_intercept: T) -> Box<Self> {
        Box::new(Self::new(slope, y_intercept))
    }
}

impl<T> Display for SummationLinear<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "Linear: {} * x + {}", self.slope, self.y_intercept)
    }
}

impl Hash for SummationLinear<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.slope.is_nan());
        state.write_u32(self.slope.to_bits());

        debug_assert!(!self.y_intercept.is_nan());
        state.write_u32(self.y_intercept.to_bits());
    }
}

impl Hash for SummationLinear<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.slope.is_nan());
        state.write_u64(self.slope.to_bits());

        debug_assert!(!self.y_intercept.is_nan());
        state.write_u64(self.y_intercept.to_bits());
    }
}
