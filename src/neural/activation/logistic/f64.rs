use crate::neural::{
    activation::{logistic::SummationLogistic, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationLogistic {
    #[allow(clippy::arithmetic_side_effects, clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);

        1.0_f64 / (1.0_f64 + (-input).exp())
    }
}
