use crate::neural::{
    activation::{logistic::SummationLogistic, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationLogistic {
    #[allow(clippy::arithmetic_side_effects, clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);

        1.0 / (1.0 + (-input).exp())
    }
}
