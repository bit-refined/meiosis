use crate::neural::{
    activation::{linear::SummationLinear, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationLinear<f32> {
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);
        input.mul_add(self.slope, self.y_intercept)
    }
}
