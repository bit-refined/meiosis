use crate::neural::{
    activation::{linear::SummationLinear, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationLinear<f64> {
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        input.mul_add(self.slope, self.y_intercept)
    }
}
