use core::fmt::{Display, Formatter};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

/// Built-in implementation for `f32`.
mod f32;
/// Built-in implementation for `f64`.
mod f64;

/// For those more used to the term `Sigmoid` this type alias can be used instead of [`SummationLogistic`].
pub type Sigmoid = SummationLogistic;
/// For those more used to the term `SoftStep` this type alias can be used instead of [`SummationLogistic`].
pub type SoftStep = SummationLogistic;

/// MISSING DOCS
/// # Range
/// `(0, 1)`
/// # Alias
/// Sigmoid, Soft Step
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationLogistic;

impl SummationLogistic {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn new_boxed() -> Box<Self> {
        Box::new(Self)
    }
}

impl Display for SummationLogistic {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("Logistic: 1 / (1 + e ^ -x)")
    }
}
