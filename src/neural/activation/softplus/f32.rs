use crate::neural::{
    activation::{softplus::SummationSoftPlus, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationSoftPlus {
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);
        input.exp().ln_1p()
    }
}
