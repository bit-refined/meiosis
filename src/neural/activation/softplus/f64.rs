use crate::neural::{
    activation::{softplus::SummationSoftPlus, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationSoftPlus {
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        input.exp().ln_1p()
    }
}
