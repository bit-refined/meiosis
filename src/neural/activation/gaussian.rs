use core::fmt::{Display, Formatter};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

/// Built-in implementation for `f32`.
mod f32;
/// Built-in implementation for `f64`.
mod f64;

/// MISSING DOCS
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationGaussian;

impl SummationGaussian {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn new_boxed() -> Box<Self> {
        Box::new(Self)
    }
}

impl Display for SummationGaussian {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("Gaussian: e ^ (-x ^ 2)")
    }
}
