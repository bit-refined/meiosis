use core::fmt::{Display, Formatter};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

use crate::neural::{
    activation::ActivationFunction,
    aggregation::{sum::Sum, AggregationFunction},
    value::Value,
};

/// MISSING DOCS
#[derive(Clone, Copy, Debug, PartialEq, Eq, Default, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationIdentity;

impl SummationIdentity {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }
}

impl<T> ActivationFunction<T> for SummationIdentity
where
    T: Value,
{
    fn activate(&self, inputs: &[T]) -> T {
        Sum::new().aggregate(inputs)
    }
}

impl Display for SummationIdentity {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("Identity: x")
    }
}
