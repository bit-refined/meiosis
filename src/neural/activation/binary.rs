use core::fmt::{Display, Formatter};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

/// Built-in implementation for `f32`.
mod f32;
/// Built-in implementation for `f64`.
mod f64;

/// MISSING DOCS
/// # Range
/// `{0, 1}`
/// # Alias
/// Heaviside step, unit step
#[derive(Clone, Copy, Debug, Default, Hash, PartialEq, Eq)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationBinary;

impl SummationBinary {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }
}

impl Display for SummationBinary {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("Binary: 1 if x > 0, 0 if x <= 0")
    }
}
