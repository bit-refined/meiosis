use crate::neural::{
    activation::{tangent::SummationHyperbolicTangent, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationHyperbolicTangent {
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        input.tanh()
    }
}
