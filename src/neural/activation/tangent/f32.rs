use crate::neural::{
    activation::{tangent::SummationHyperbolicTangent, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationHyperbolicTangent {
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);
        input.tanh()
    }
}
