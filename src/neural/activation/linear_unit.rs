use core::{
    fmt::{Display, Formatter},
    hash::{Hash, Hasher},
};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

/// Built-in implementation for `f32`.
mod f32;
/// Built-in implementation for `f64`.
mod f64;

/// MISSING DOCS
/// # Range
/// `[0, +Infinity)`
/// # Alias
/// `ReLU`
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationRectifiedLinearUnit;

impl SummationRectifiedLinearUnit {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn new_boxed() -> Box<Self> {
        Box::new(Self)
    }
}

impl Display for SummationRectifiedLinearUnit {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("ReLU: max(0, x)")
    }
}

/// MISSING DOCS
/// # Alias
/// `PReLU`
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[allow(clippy::module_name_repetitions)]
pub struct SummationParametricRectifiedLinearUnit<T> {
    /// MISSING DOCS
    alpha: T,
}

impl<T> SummationParametricRectifiedLinearUnit<T> {
    /// MISSING DOCS
    pub const fn new(alpha: T) -> Self {
        Self { alpha }
    }

    /// MISSING DOCS
    pub fn new_boxed(alpha: T) -> Box<Self> {
        Box::new(Self::new(alpha))
    }
}

impl<T> Display for SummationParametricRectifiedLinearUnit<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "Parametric ReLU: {} * x if x < 0, x if x >= 0", self.alpha)
    }
}

impl Hash for SummationParametricRectifiedLinearUnit<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.alpha.is_nan());
        state.write_u32(self.alpha.to_bits());
    }
}

impl Hash for SummationParametricRectifiedLinearUnit<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.alpha.is_nan());
        state.write_u64(self.alpha.to_bits());
    }
}

/// MISSING DOCS
/// # Alias
/// Leaky `ReLU`
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationLeakyRectifiedLinearUnit;

impl SummationLeakyRectifiedLinearUnit {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn new_boxed() -> Box<Self> {
        Box::new(Self)
    }
}

impl Display for SummationLeakyRectifiedLinearUnit {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "Leaky ReLU: 0.01 * x if x < 0, x if x >= 0")
    }
}

/// MISSING DOCS
/// # Alias
/// `SiLU`, Sigmoid Shrinkage, `SiL`, Swish-1
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[non_exhaustive]
#[allow(clippy::module_name_repetitions)]
pub struct SummationSigmoidLinearUnit;

impl SummationSigmoidLinearUnit {
    /// MISSING DOCS
    #[must_use]
    pub const fn new() -> Self {
        Self
    }

    /// MISSING DOCS
    #[must_use]
    pub fn new_boxed() -> Box<Self> {
        Box::new(Self)
    }
}

impl Display for SummationSigmoidLinearUnit {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        f.write_str("SiLU: x / (1 + e ^ -x)")
    }
}

/// MISSING DOCS
/// # Alias
/// `ELU`
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[allow(clippy::module_name_repetitions)]
pub struct SummationExponentialLinearUnit<T> {
    /// MISSING DOCS
    alpha: T,
}

impl<T> SummationExponentialLinearUnit<T> {
    /// MISSING DOCS
    pub const fn new(alpha: T) -> Self {
        Self { alpha }
    }

    /// MISSING DOCS
    pub fn new_boxed(alpha: T) -> Box<Self> {
        Box::new(Self::new(alpha))
    }
}

impl<T> Display for SummationExponentialLinearUnit<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "ELU: {} * (exp(x) - 1) if x < 0, x if x >= 0", self.alpha)
    }
}

impl Hash for SummationExponentialLinearUnit<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.alpha.is_nan());
        state.write_u32(self.alpha.to_bits());
    }
}

impl Hash for SummationExponentialLinearUnit<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        debug_assert!(!self.alpha.is_nan());
        state.write_u64(self.alpha.to_bits());
    }
}
