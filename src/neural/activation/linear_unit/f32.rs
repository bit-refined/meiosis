use crate::neural::{
    activation::{
        linear_unit::{
            SummationExponentialLinearUnit, SummationLeakyRectifiedLinearUnit, SummationParametricRectifiedLinearUnit,
            SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
        },
        logistic::SummationLogistic,
        ActivationFunction,
    },
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationRectifiedLinearUnit {
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);
        input.max(0.)
    }
}

impl ActivationFunction<f32> for SummationParametricRectifiedLinearUnit<f32> {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);

        if input >= 0.0_f32 {
            input
        } else {
            self.alpha * input
        }
    }
}

impl ActivationFunction<f32> for SummationLeakyRectifiedLinearUnit {
    fn activate(&self, inputs: &[f32]) -> f32 {
        let unit = SummationParametricRectifiedLinearUnit { alpha: 0.01_f32 };

        unit.activate(inputs)
    }
}

impl ActivationFunction<f32> for SummationSigmoidLinearUnit {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);

        let sigmoid = SummationLogistic;
        input * sigmoid.activate(inputs)
    }
}

impl ActivationFunction<f32> for SummationExponentialLinearUnit<f32> {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f32]) -> f32 {
        let input = Sum::new().aggregate(inputs);

        if input >= 0.0 {
            input
        } else {
            self.alpha * (f32::exp(input) - 1.0)
        }
    }
}
