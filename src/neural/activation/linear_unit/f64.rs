use crate::neural::{
    activation::{
        linear_unit::{
            SummationExponentialLinearUnit, SummationLeakyRectifiedLinearUnit, SummationParametricRectifiedLinearUnit,
            SummationRectifiedLinearUnit, SummationSigmoidLinearUnit,
        },
        logistic::SummationLogistic,
        ActivationFunction,
    },
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationRectifiedLinearUnit {
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        input.max(0.)
    }
}

impl ActivationFunction<f64> for SummationParametricRectifiedLinearUnit<f64> {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);

        if input >= 0.0_f64 {
            input
        } else {
            self.alpha * input
        }
    }
}

impl ActivationFunction<f64> for SummationLeakyRectifiedLinearUnit {
    fn activate(&self, inputs: &[f64]) -> f64 {
        let unit = SummationParametricRectifiedLinearUnit { alpha: 0.01_f64 };

        unit.activate(inputs)
    }
}

impl ActivationFunction<f64> for SummationSigmoidLinearUnit {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);

        let sigmoid = SummationLogistic;
        input * sigmoid.activate(inputs)
    }
}

impl ActivationFunction<f64> for SummationExponentialLinearUnit<f64> {
    #[allow(clippy::float_arithmetic)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);

        if input >= 0.0_f64 {
            input
        } else {
            self.alpha * (f64::exp(input) - 1.0_f64)
        }
    }
}
