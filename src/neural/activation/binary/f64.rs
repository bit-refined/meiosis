use crate::neural::{
    activation::{binary::SummationBinary, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationBinary {
    fn activate(&self, inputs: &[f64]) -> f64 {
        if Sum::new().aggregate(inputs) > 0.0_f64 {
            1.0_f64
        } else {
            0.0_f64
        }
    }
}
