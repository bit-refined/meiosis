use crate::neural::{
    activation::{binary::SummationBinary, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f32> for SummationBinary {
    fn activate(&self, inputs: &[f32]) -> f32 {
        if Sum::new().aggregate(inputs) > 0.0 {
            1.0
        } else {
            0.0
        }
    }
}
