use crate::neural::{
    activation::{gaussian::SummationGaussian, ActivationFunction},
    aggregation::{sum::Sum, AggregationFunction},
};

impl ActivationFunction<f64> for SummationGaussian {
    #[allow(clippy::float_arithmetic, clippy::arithmetic_side_effects)]
    fn activate(&self, inputs: &[f64]) -> f64 {
        let input = Sum::new().aggregate(inputs);
        (-input.powi(2)).exp()
    }
}
