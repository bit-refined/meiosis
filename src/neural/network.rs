use std::collections::HashMap;

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};
#[cfg(feature = "serialization")]
use serde_with::{serde_as, As, Same};

use crate::neural::{
    activation::ActivationType,
    connection::{InputIndex, OutputIndex},
    input::ToNetworkInput,
    network::{feedforward::FeedForward, recurrent::Recurrent},
    neuron::Neuron,
    output::FromNetworkOutput,
    value::Value,
};

/// MISSING DOCS
pub mod feedforward;
/// MISSING DOCS
pub mod recurrent;

/// A (possibly) fully recurrent neural network, but with variable hidden neurons and connections.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(
    feature = "serialization",
    serde_as,
    derive(Serialize, Deserialize),
    serde(bound(
        serialize = "T: Value, K: Clone + Serialize + for<'des> Deserialize<'des>",
        deserialize = "T: Value, K: Clone + Serialize + for<'des> Deserialize<'des>"
    )),
    allow(clippy::single_char_lifetime_names)
)]
pub struct Network<const IN: usize, const OUT: usize, T, K>
where
    T: Value,
{
    /// MISSING DOCS
    #[cfg_attr(feature = "serialization", serde(with = "As::<[Same; IN]>"))]
    input: [T; IN],
    /// MISSING DOCS
    #[cfg_attr(feature = "serialization", serde(with = "As::<[Same; OUT]>"))]
    output: [Neuron<T>; OUT],
    /// MISSING DOCS
    bias: T,
    /// MISSING DOCS
    structure: K,
}

impl<T> Network<0, 0, T, ()>
where
    T: Value,
{
    /// Creates a new neural network of undefined structure and without a bias set.
    /// If Rust can't infer the amount of in- and outputs, they're configurable as generic parameters.
    #[must_use]
    pub fn new<const INPUTS: usize, const OUTPUTS: usize>() -> Network<INPUTS, OUTPUTS, T, ()> {
        Network {
            input: core::array::from_fn(|_| T::zero()),
            output: core::array::from_fn(|_| Neuron::default()),
            bias: T::zero(),
            structure: (),
        }
    }
}

impl<T, const IN: usize, const OUT: usize> Network<IN, OUT, T, ()>
where
    T: Value,
{
    /// MISSING DOCS
    pub fn recurrent(self) -> Network<IN, OUT, T, Recurrent<T>> {
        Network {
            input: self.input,
            output: self.output,
            bias: self.bias,
            structure: Recurrent {
                hidden: vec![],
                connections: HashMap::new(),
            },
        }
    }

    /// MISSING DOCS
    pub fn feedforward(self) -> Network<IN, OUT, T, FeedForward<T>> {
        Network {
            input: self.input,
            output: self.output,
            bias: self.bias,
            structure: FeedForward::new(),
        }
    }
}

impl<K, T, const IN: usize, const OUT: usize> Network<IN, OUT, T, K>
where
    T: Value,
{
    /// MISSING DOCS
    #[must_use]
    pub fn with_bias(mut self, bias: T) -> Self {
        self.set_bias(bias);
        self
    }

    /// MISSING DOCS
    pub fn set_bias(&mut self, bias: T) {
        self.bias = bias;
    }

    /// MISSING DOCS
    pub const fn get_bias(&self) -> &T {
        &self.bias
    }

    /// MISSING DOCS
    pub fn set_inputs<INPUT>(&mut self, input: INPUT)
    where
        INPUT: ToNetworkInput<T, IN>,
    {
        self.input = input.to_network_input();
    }

    /// MISSING DOCS
    pub fn set_outputs(&mut self, outputs: [Neuron<T>; OUT]) {
        self.output = outputs;
    }

    /// MISSING DOCS
    // The output index is verified to be safe.
    #[allow(clippy::indexing_slicing)]
    pub fn set_output_activation_function(
        &mut self,
        output: OutputIndex,
        activation_function: <T as ActivationType>::Function,
    ) {
        Self::assert_valid_output(output);

        self.output[output].activation_function = activation_function;
    }

    /// MISSING DOCS
    pub fn get_outputs<'output, OUTPUT>(&'output self) -> OUTPUT
    where
        OUTPUT: FromNetworkOutput<'output, T, OUT>,
    {
        OUTPUT::from_network_output(&self.output)
    }

    /// MISSING DOCS
    fn assert_valid_input(input: InputIndex) {
        assert!(
            input < IN,
            "trying to add connection to non-existent input {input}; {IN} inputs available",
        );
    }

    /// MISSING DOCS
    fn assert_valid_output(output: OutputIndex) {
        assert!(
            output < OUT,
            "trying to add connection to non-existent output {output}; {OUT} outputs available",
        );
    }
}

// /// A feedforward network is a recurrent network without loops which means conversion can fail.
// impl<T, const IN: usize, const OUT: usize> TryFrom<Network<IN, OUT, T, Recurrent<T>>>
//     for Network<IN, OUT, T, FeedForward<T>>
// where
//     T: Value,
// {
//     type Error = ();
//
//     fn try_from(_network: Network<IN, OUT, T, Recurrent<T>>) -> Result<Self, Self::Error> {
//         todo!()
//     }
// }
//
// /// A feedforward network is the equivalent of a recurrent network without loops, and thus can trivially be converted.
// impl<T, const IN: usize, const OUT: usize> From<Network<IN, OUT, T, FeedForward<T>>>
//     for Network<IN, OUT, T, Recurrent<T>>
// where
//     T: Value,
// {
//     fn from(_network: Network<IN, OUT, T, FeedForward<T>>) -> Self {
//         todo!()
//     }
// }

#[cfg(test)]
mod tests {
    use crate::neural::network::Network;
    use pretty_assertions::assert_eq;

    #[test]
    fn serialize_round_trip_feedforward() {
        let mut network = Network::new::<1, 2>().feedforward();
        network.set_bias(42.7);
        network.set_inputs([7.0]);
        network.connect_input_to_output(0, 1, 0.5);
        network.evaluate();
        let json = serde_json::to_string_pretty(&network).unwrap();

        let expected_json = r#"{
  "input": [
    7.0
  ],
  "output": [
    {
      "activation_function": {
        "Identity": null
      },
      "inputs": [],
      "output": 0.0
    },
    {
      "activation_function": {
        "Identity": null
      },
      "inputs": [],
      "output": 3.5
    }
  ],
  "bias": 42.7,
  "structure": {
    "tick": "InputToOutput",
    "layers": [],
    "output_connections": [
      [
        {
          "FromPrevious": {
            "from": 0,
            "to": 1
          }
        },
        0.5
      ]
    ]
  }
}"#;

        assert_eq!(expected_json, json);

        let deserialized = serde_json::from_str(&json).unwrap();

        assert_eq!(network, deserialized);
    }

    #[test]
    fn serialize_round_trip_recurrent() {
        let mut network = Network::new::<1, 2>().recurrent();
        network.set_bias(42.7);
        network.set_inputs([7.0]);
        network.connect_input_to_output(0, 1, 0.5);
        network.tick();
        let json = serde_json::to_string_pretty(&network).unwrap();

        let expected_json = r#"{
  "input": [
    7.0
  ],
  "output": [
    {
      "activation_function": {
        "Identity": null
      },
      "inputs": [],
      "output": 0.0
    },
    {
      "activation_function": {
        "Identity": null
      },
      "inputs": [],
      "output": 3.5
    }
  ],
  "bias": 42.7,
  "structure": {
    "hidden": [],
    "connections": [
      [
        {
          "InputToOutput": [
            0,
            1
          ]
        },
        0.5
      ]
    ]
  }
}"#;

        assert_eq!(expected_json, json);

        let deserialized = serde_json::from_str(&json).unwrap();

        assert_eq!(network, deserialized);
    }
}
