use core::fmt::Debug;

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};

use crate::neural::{
    activation::{ActivationFunction, ActivationType},
    value::Value,
};

/// # Alias
/// Node, Perceptron
#[derive(Debug, PartialEq)]
#[allow(clippy::type_repetition_in_bounds)]
#[cfg_attr(
    feature = "serialization",
    derive(Serialize, Deserialize),
    serde(bound(serialize = "T: Value", deserialize = "T: Value"))
)]
pub struct Neuron<T>
where
    T: Value,
{
    /// MISSING DOCS
    pub(crate) activation_function: <T as ActivationType>::Function,
    /// TODO: replace with something more efficient
    pub(crate) inputs: Vec<T>,
    /// MISSING DOCS
    pub(crate) output: T,
}

impl<T> Neuron<T>
where
    T: Value,
{
    /// Creates a new neuron with the given activation function.
    ///
    /// # Note
    /// The activation function requires a `'static` lifetime because they need to be known at
    /// compile time (and so we don't have to deal with lifetimes in network networks :)
    pub fn new(activation_function: <T as ActivationType>::Function) -> Self {
        Self {
            activation_function,
            inputs: Vec::new(),
            output: T::zero(),
        }
    }

    /// MISSING DOCS
    pub fn set_activation_function(&mut self, activation_function: <T as ActivationType>::Function) {
        self.activation_function = activation_function;
    }

    /// MISSING DOCS
    pub fn activate(&mut self) {
        // We only update the output if there are actually inputs available.
        if !self.inputs.is_empty() {
            self.output = self.activation_function.activate(&self.inputs);

            self.inputs.clear();
        }
    }

    /// MISSING DOCS
    pub fn reset(&mut self)
    where
        T: Value,
    {
        self.output = T::zero();
        self.inputs.clear();
    }

    /// MISSING DOCS
    pub fn add_input(&mut self, input: T) {
        self.inputs.push(input);
    }

    /// MISSING DOCS
    pub const fn output(&self) -> &T {
        &self.output
    }
}

impl<T> Clone for Neuron<T>
where
    T: Value,
{
    fn clone(&self) -> Self {
        Self {
            activation_function: self.activation_function.clone(),
            inputs: self.inputs.clone(),
            output: self.output.clone(),
        }
    }
}

/// By default a [`Neuron`] has an identity activation function and the default value of whatever
/// type it is transmitting.
impl<T> Default for Neuron<T>
where
    T: Value + ActivationType,
{
    fn default() -> Self {
        Neuron::new(<T as ActivationType>::Function::default())
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use crate::neural::neuron::Neuron;

    #[test]
    fn debug() {
        let neuron: Neuron<f64> = Neuron::default();

        let debug = format!("{neuron:?}");

        // TODO: clean this up by eliding certain things like phantomdata
        let expect = "Neuron { activation_function: Identity(SummationIdentity), inputs: [], output: 0.0 }";

        assert_eq!(expect, debug);
    }
}
