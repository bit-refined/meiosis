/// MISSING DOCS
pub(crate) type InputIndex = usize;
/// In recurrent networks this is the index at which a neuron is stored.
/// In feedforward networks this is the index at which a neuron is stored in a layer.
pub(crate) type NeuronIndex = usize;
/// MISSING DOCS
pub(crate) type OutputIndex = usize;
