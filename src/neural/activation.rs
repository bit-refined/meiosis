use core::fmt::{Debug, Display};

use crate::serialization::MaybeSerde;

/// MISSING DOCS
pub mod binary;
/// MISSING DOCS
pub mod float;
/// MISSING DOCS
pub mod gaussian;
/// MISSING DOCS
pub mod identity;
/// MISSING DOCS
pub mod linear;
/// MISSING DOCS
pub mod linear_unit;
/// MISSING DOCS
pub mod logistic;
/// MISSING DOCS
pub mod mish;
/// MISSING DOCS
pub mod softplus;
/// MISSING DOCS
pub mod tangent;

/// This trait is used to determine which activation function type to use at compile time.
/// There are default implementations for the most common data types, but adding your own can be
/// done when implementing this.
///
/// # Design decision
/// Originally, the plan was to integrate activation functions using trait objects, but it very
/// quickly resulted in a complicated mess of narrow constraints and abstraction hell.
///
/// The trade-off of using enums over trait objects is that they're not extendable by the user,
/// but this trait is here to allow you to implement your own.
///
/// # Extending available activation functions for existing types
/// Should you want to add an activation function, e.g. for `f64`, you have two choices.
/// - Create a new-type around `f64` and then let your functions be an enum of your own implementations
///   plus a variant that contains the existing activation functions enum from this library.
///   I know this sounds messy, and it is, but it's better than to try and maintain trait object safety.
/// - Submit code to the repository, so the built-in list grows and future users can benefit too (this is the preferred method)
#[allow(clippy::module_name_repetitions)]
pub trait ActivationType: Sized {
    /// This type is used to define at compile time which `ActivationFunction` is allowed to be used
    /// with the type it is implemented for.
    type Function: Clone + Debug + PartialEq + Display + Default + ActivationFunction<Self> + MaybeSerde;
}

/// # Implementation requirements
///
/// ## `Clone`
/// Clone is used to duplicate [`crate::genotype::Genotype`]s and [`crate::phenotype::Phenotype`]s. It is important that *all* values
/// in the type are cloned 1-to-1 so that a cloned [`crate::genotype::Genotype`] has the exact same genetic makeup.
///
/// ## `PartialEq`
/// When determining the distance of two [`crate::genotype::Genotype`]s, we need to know whether two activation functions
/// are the same.
///
/// ## `Display`
/// Neural networks can be rendered using DOT which requires them to print neuron activation functions
/// including all parameters, so that one could take any rendered neural network and perfectly
/// reconstruct it in any other context, programming language or application.
///
/// ## `Debug`
/// While technically not necessary, this trait is a requirement so that any other structures storing
/// activation functions (e.g. genetic instructions and final networks) can be debugged.
#[allow(clippy::module_name_repetitions)]
pub trait ActivationFunction<T>: Clone + PartialEq + Display + Debug {
    /// Calculates the result of the activation function given any inputs. Each input value is the
    /// output of a connected neuron. Having a slice here helps implement various aggregation functions.
    /// Default implementations in this library mostly use summation.
    fn activate(&self, inputs: &[T]) -> T;
}
