use core::{
    fmt::Debug,
    ops::{Add, AddAssign, Mul},
};

use crate::{neural::activation::ActivationType, serialization::MaybeSerde};

/// MISSING DOCS
mod floats;

/// Neural networks are fully generic over the type they're calculating which means some basic
/// information and operations are required of the type.
pub trait Value:
    Debug + Clone + AddAssign + Mul<Output = Self> + PartialEq + Add<Output = Self> + ActivationType + MaybeSerde
{
    /// Irrelevant for which type this is implemented, this value multiplied by any other value
    /// will always be zero. It is used as a default value and to clean up useless connections.
    fn zero() -> Self;
}
