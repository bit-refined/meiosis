use crate::neural::{neuron::Neuron, value::Value};

/// This trait is used to allow users to make their output structured, converting the result
/// from a network network into something less abstract than an array.
///
/// This trait has a blanket implementations for arrays of the correct size, so if there is no
/// abstraction needed, there is no additional code running to get those values.
#[allow(clippy::module_name_repetitions)]
pub trait FromNetworkOutput<'output, T, const OUT: usize>
where
    T: Value,
{
    /// MISSING DOCS
    fn from_network_output(output: &'output [Neuron<T>; OUT]) -> Self;
}

impl<'output, T, const OUT: usize> FromNetworkOutput<'output, T, OUT> for [Neuron<T>; OUT]
where
    Self: Copy,
    T: Value,
{
    fn from_network_output(output: &'output [Neuron<T>; OUT]) -> Self {
        *output
    }
}

impl<'output, T, const OUT: usize> FromNetworkOutput<'output, T, OUT> for [&'output Neuron<T>; OUT]
where
    T: Value,
{
    fn from_network_output(output: &'output [Neuron<T>; OUT]) -> Self {
        output.each_ref()
    }
}

impl<'output, T> FromNetworkOutput<'output, T, 1> for T
where
    T: Value + Copy,
{
    fn from_network_output(output: &'output [Neuron<T>; 1]) -> Self {
        output[0].output
    }
}

impl<'output, T, const OUT: usize> FromNetworkOutput<'output, T, OUT> for [&'output T; OUT]
where
    T: Value,
{
    fn from_network_output(output: &'output [Neuron<T>; OUT]) -> Self {
        output.each_ref().map(|n| &n.output)
    }
}
