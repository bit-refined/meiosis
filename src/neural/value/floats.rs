use crate::neural::value::Value;

impl Value for f32 {
    fn zero() -> Self {
        0.0
    }
}

impl Value for f64 {
    fn zero() -> Self {
        0.0_f64
    }
}
