use core::fmt::{Display, Write};
use std::collections::{hash_map::Entry, HashMap};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};
#[cfg(feature = "serialization")]
use serde_with::{serde_as, As, Same};

use crate::neural::{
    connection::{InputIndex, NeuronIndex, OutputIndex},
    network::Network,
    neuron::Neuron,
    value::Value,
};

/// MISSING DOCS
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Ord, PartialOrd)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[allow(clippy::exhaustive_enums)]
pub enum Connection {
    /// MISSING DOCS
    InputToOutput(InputIndex, OutputIndex),
    /// MISSING DOCS
    InputToHidden(InputIndex, NeuronIndex),
    /// MISSING DOCS
    HiddenToHidden(NeuronIndex, NeuronIndex),
    /// MISSING DOCS
    HiddenToOutput(NeuronIndex, OutputIndex),
    /// MISSING DOCS
    BiasToHidden(NeuronIndex),
    /// MISSING DOCS
    BiasToOutput(OutputIndex),
}

/// This kind of network is fully recurrent and can have loops, but can not be evaluated instantly
/// and has to be "ticked" to advance values in it. The recommended use case for this kind of
/// network is real-time applications.
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(
    feature = "serialization",
    serde_as,
    derive(Serialize, Deserialize),
    serde(bound(serialize = "T: Value", deserialize = "T: Value"))
)]
pub struct Recurrent<T>
where
    T: Value,
{
    /// MISSING DOCS
    pub(crate) hidden: Vec<Neuron<T>>,
    /// MISSING DOCS
    #[cfg_attr(feature = "serialization", serde(with = "As::<Vec<(Same, Same)>>"))]
    pub(crate) connections: HashMap<Connection, T>,
}

/// Recurrent network specific methods.
impl<T, const IN: usize, const OUT: usize> Network<IN, OUT, T, Recurrent<T>>
where
    T: Value,
{
    /// MISSING DOCS
    pub fn tick(&mut self) {
        self.sum_outputs();
        self.activate_neurons();
    }

    /// For every input, bias and hidden neuron we use its output to add up values in connected
    /// neurons.
    fn sum_outputs(&mut self) {
        // the following code is written very naively and has likely a lot of optimzation potential

        for (direction, weight) in &self.structure.connections {
            #[allow(clippy::shadow_reuse)]
            let weight = weight.clone();

            // As an invariant of the network, connections are always valid, so we can safely use
            // index slicing here.
            #[allow(clippy::indexing_slicing, clippy::arithmetic_side_effects)]
            match *direction {
                Connection::InputToHidden(input, hidden) => {
                    let transmitted_value = self.input[input].clone() * weight;
                    self.structure.hidden[hidden].inputs.push(transmitted_value);
                }
                Connection::InputToOutput(input, output) => {
                    let transmitted_value = self.input[input].clone() * weight;

                    self.output[output].inputs.push(transmitted_value);
                }
                Connection::HiddenToHidden(hidden_out, hidden_in) => {
                    let transmitted_value = self.structure.hidden[hidden_out].output.clone() * weight;

                    self.structure.hidden[hidden_in].inputs.push(transmitted_value);
                }
                Connection::HiddenToOutput(hidden, output) => {
                    let transmitted_value = self.structure.hidden[hidden].output.clone() * weight;

                    self.output[output].inputs.push(transmitted_value);
                }
                Connection::BiasToHidden(hidden) => {
                    let transmitted_value = self.bias.clone() * weight;

                    self.structure.hidden[hidden].inputs.push(transmitted_value);
                }
                Connection::BiasToOutput(output) => {
                    let transmitted_value = self.bias.clone() * weight;

                    self.output[output].inputs.push(transmitted_value);
                }
            }
        }
    }

    /// Using the stored sums in each hidden and output neuron, we calculate its final output using
    /// the activation function.
    fn activate_neurons(&mut self) {
        for neuron in self.structure.hidden.iter_mut().chain(self.output.iter_mut()) {
            neuron.activate();
        }
    }

    /// MISSING DOCS
    fn assert_valid_hidden(&self, hidden: NeuronIndex) {
        let hidden_count = self.structure.hidden.len();

        assert!(
            hidden < hidden_count,
            "trying to add connection to non-existent hidden neuron {hidden}; {hidden_count} neurons available",
        );
    }

    /// Add a connection between an input and an output.
    ///
    /// # Panics
    /// This method panics if either input or output does not exist or a connection of this type already
    /// exists.
    #[allow(clippy::panic)]
    pub fn connect_input_to_output(&mut self, input: InputIndex, output: OutputIndex, weight: T) {
        Self::assert_valid_input(input);
        Self::assert_valid_output(output);

        let direction = Connection::InputToOutput(input, output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Input({input}) -> Output({output}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_input_and_output(&mut self, input: InputIndex, output: OutputIndex) {
        Self::assert_valid_input(input);
        Self::assert_valid_output(output);

        let direction = Connection::InputToOutput(input, output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Input({input}) -> Output({output}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_input_to_hidden(&mut self, input: InputIndex, hidden: NeuronIndex, weight: T) {
        Self::assert_valid_input(input);
        self.assert_valid_hidden(hidden);

        let direction = Connection::InputToHidden(input, hidden);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Input({input}) -> Hidden({hidden}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_input_and_hidden(&mut self, input: InputIndex, hidden: NeuronIndex) {
        Self::assert_valid_input(input);
        self.assert_valid_hidden(hidden);

        let direction = Connection::InputToHidden(input, hidden);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Input({input}) -> Hidden({hidden}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_hidden_to_hidden(&mut self, hidden_from: NeuronIndex, hidden_to: NeuronIndex, weight: T) {
        self.assert_valid_hidden(hidden_from);
        self.assert_valid_hidden(hidden_to);

        let direction = Connection::HiddenToHidden(hidden_from, hidden_to);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Hidden({hidden_from}) -> Hidden({hidden_to}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_hidden_and_hidden(&mut self, hidden_from: NeuronIndex, hidden_to: NeuronIndex) {
        self.assert_valid_hidden(hidden_from);
        self.assert_valid_hidden(hidden_to);

        let direction = Connection::HiddenToHidden(hidden_from, hidden_to);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Hidden({hidden_from}) -> Hidden({hidden_to}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_hidden_to_output(&mut self, hidden: NeuronIndex, output: OutputIndex, weight: T) {
        self.assert_valid_hidden(hidden);
        Self::assert_valid_output(output);

        let direction = Connection::HiddenToOutput(hidden, output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Hidden({hidden}) -> Output({output}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_hidden_and_output(&mut self, hidden: NeuronIndex, output: OutputIndex) {
        self.assert_valid_hidden(hidden);
        Self::assert_valid_output(output);

        let direction = Connection::HiddenToOutput(hidden, output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Hidden({hidden}) -> Output({output}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_bias_to_hidden(&mut self, hidden: NeuronIndex, weight: T) {
        self.assert_valid_hidden(hidden);

        let direction = Connection::BiasToHidden(hidden);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Bias -> Hidden({hidden}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_bias_to_hidden(&mut self, hidden: NeuronIndex) {
        self.assert_valid_hidden(hidden);

        let direction = Connection::BiasToHidden(hidden);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Bias -> Hidden({hidden}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_bias_to_output(&mut self, output: OutputIndex, weight: T) {
        Self::assert_valid_output(output);

        let direction = Connection::BiasToOutput(output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Bias -> Output({output}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_bias_to_output(&mut self, output: OutputIndex) {
        Self::assert_valid_output(output);

        let direction = Connection::BiasToOutput(output);

        match self.structure.connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Bias -> Output({output}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    pub fn connect_hidden_to_self(&mut self, hidden: NeuronIndex, weight: T) {
        self.connect_hidden_to_hidden(hidden, hidden, weight);
    }

    /// MISSING DOCS
    pub fn disconnect_hidden_from_self(&mut self, hidden: NeuronIndex) {
        self.disconnect_hidden_and_hidden(hidden, hidden);
    }

    /// MISSING DOCS
    pub fn connect(&mut self, connection: Connection, weight: T) {
        match connection {
            Connection::InputToHidden(input, hidden) => self.connect_input_to_hidden(input, hidden, weight),
            Connection::InputToOutput(input, output) => self.connect_input_to_output(input, output, weight),
            Connection::HiddenToHidden(hidden_from, hidden_to) => {
                self.connect_hidden_to_hidden(hidden_from, hidden_to, weight);
            }
            Connection::HiddenToOutput(hidden, output) => self.connect_hidden_to_output(hidden, output, weight),
            Connection::BiasToHidden(hidden) => self.connect_bias_to_hidden(hidden, weight),
            Connection::BiasToOutput(output) => self.connect_bias_to_output(output, weight),
        }
    }

    /// MISSING DOCS
    pub fn disconnect(&mut self, direction: Connection) {
        match direction {
            Connection::InputToHidden(input, hidden) => self.disconnect_input_and_hidden(input, hidden),
            Connection::InputToOutput(input, output) => self.disconnect_input_and_output(input, output),
            Connection::HiddenToHidden(hidden_from, hidden_to) => {
                self.disconnect_hidden_and_hidden(hidden_from, hidden_to);
            }
            Connection::HiddenToOutput(hidden, output) => self.disconnect_hidden_and_output(hidden, output),
            Connection::BiasToHidden(hidden) => self.disconnect_bias_to_hidden(hidden),
            Connection::BiasToOutput(output) => self.disconnect_bias_to_output(output),
        }
    }

    /// MISSING DOCS
    pub fn add_hidden(&mut self, hidden: Neuron<T>) {
        self.structure.hidden.push(hidden);
    }

    /// MISSING DOCS
    pub fn remove_hidden(&mut self, hidden: NeuronIndex) {
        self.assert_valid_hidden(hidden);

        let _removed = self.structure.hidden.remove(hidden);

        let affected_connections = self
            .structure
            .connections
            .extract_if(|connection, _weight| match *connection {
                Connection::InputToHidden(_, neuron)
                | Connection::HiddenToOutput(neuron, _)
                | Connection::BiasToHidden(neuron) => neuron >= hidden,
                Connection::HiddenToHidden(neuron_from, neuron_to) => neuron_from >= hidden || neuron_to >= hidden,
                Connection::InputToOutput(_, _) | Connection::BiasToOutput(_) => false,
            });

        // finally, adjust all neuron references
        let adjusted_connections = affected_connections
            .into_iter()
            .filter_map(|(mut connection, weight)| match connection {
                Connection::InputToHidden(_, ref mut neuron)
                | Connection::HiddenToOutput(ref mut neuron, _)
                | Connection::BiasToHidden(ref mut neuron) => {
                    #[allow(clippy::arithmetic_side_effects, clippy::if_then_some_else_none)]
                    if *neuron > hidden {
                        // If the neuron being referenced has a bigger index than the removed one,
                        // we point to the new position of it.
                        *neuron -= 1;
                        Some((connection, weight))
                    } else {
                        // This case only happens if the neuron is the removed one, so we filter
                        // it out.
                        None
                    }
                }
                Connection::HiddenToHidden(ref mut neuron_from, ref mut neuron_to) => {
                    // This arithmetic is safe because in both cases neuron indices are guaranteed
                    // to be greater than 0.
                    #[allow(clippy::arithmetic_side_effects, clippy::if_then_some_else_none)]
                    if *neuron_from != hidden && *neuron_to != hidden {
                        if *neuron_from > hidden {
                            *neuron_from -= 1;
                        }

                        if *neuron_to > hidden {
                            *neuron_to -= 1;
                        }

                        Some((connection, weight))
                    } else {
                        None
                    }
                }
                Connection::InputToOutput(_, _) | Connection::BiasToOutput(_) => unreachable!(),
            })
            .collect::<Vec<_>>();

        self.structure.connections.extend(adjusted_connections);
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the neuron does not exist.
    #[allow(clippy::panic)]
    pub fn get_hidden(&self, hidden: NeuronIndex) -> &Neuron<T> {
        self.structure.hidden.get(hidden).map_or_else(
            || {
                panic!("neuron {hidden} does not exist");
            },
            |neuron| neuron,
        )
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the neuron does not exist.
    #[allow(clippy::panic)]
    pub fn get_hidden_mut(&mut self, hidden: NeuronIndex) -> &mut Neuron<T> {
        self.structure.hidden.get_mut(hidden).map_or_else(
            || {
                panic!("neuron {hidden} does not exist");
            },
            |neuron| neuron,
        )
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn set_weight(&mut self, connection: Connection, weight: T) {
        match self.structure.connections.entry(connection) {
            Entry::Occupied(mut entry) => {
                let _old = entry.insert(weight);
            }
            Entry::Vacant(entry) => {
                panic!("trying to set weight of invalid connection {:?}", entry.into_key());
            }
        }
    }

    /// MISSING DOCS
    pub fn reset(&mut self) {
        let neurons = self.output.iter_mut().chain(self.structure.hidden.iter_mut());

        for neuron in neurons {
            neuron.reset();
        }
    }

    /// MISSING DOCS
    pub fn neurons(&self) -> &[Neuron<T>] {
        &self.structure.hidden
    }

    /// MISSING DOCS
    pub const fn connections(&self) -> &HashMap<Connection, T> {
        &self.structure.connections
    }

    /// MISSING DOCS
    pub fn has_connection(&self, connection: &Connection) -> bool {
        self.structure.connections.contains_key(connection)
    }

    /// MISSING DOCS
    pub fn cleanup_connections(&mut self) {
        self.structure
            .connections
            .retain(|connection, weight| match *connection {
                Connection::InputToHidden(_, _)
                | Connection::HiddenToOutput(_, _)
                | Connection::HiddenToHidden(_, _)
                | Connection::InputToOutput(_, _) => *weight != T::zero(),
                // We remove bias connections if the bias is zero or if the weight is zero.
                Connection::BiasToHidden(_) | Connection::BiasToOutput(_) => {
                    !(self.bias == T::zero() || *weight == T::zero())
                }
            });
    }

    /// MISSING DOCS
    pub fn render_mermaid(&self) -> String
    where
        T: Display,
    {
        let mut graph = String::new();

        graph.push_str("graph LR\n");

        // Inputs:
        graph.push_str("\tsubgraph Inputs\n");
        graph.push_str("\t\tdirection TB\n");

        for (index, _) in self.input.iter().enumerate() {
            writeln!(graph, "\t\ti{index}((\"Input {index}\"))").expect("writing failed");
        }
        // we count the bias also as an input
        if self.bias != T::zero()
            && self
                .structure
                .connections
                .keys()
                .any(|k| matches!(*k, Connection::BiasToHidden(_) | Connection::BiasToOutput(_)))
        {
            writeln!(graph, "\t\tbias((\"Bias: {}\"))", self.bias).expect("writing failed");
        }
        graph.push_str("\tend\n\n");

        // Neurons:
        for (index, neuron) in self.structure.hidden.iter().enumerate() {
            writeln!(
                graph,
                "\tn{index}((\"Neuron {index}<br>{}\"))",
                neuron.activation_function
            )
            .expect("writing failed");
        }

        // Outputs:
        graph.push_str("\tsubgraph Outputs\n");
        graph.push_str("\t\tdirection TB\n");
        for (index, out) in self.output.iter().enumerate() {
            writeln!(
                graph,
                "\t\to{index}((\"Output {index}<br>{}\"))",
                out.activation_function
            )
            .expect("writing failed");
        }
        graph.push_str("\tend\n\n");

        let mut connections = self.connections().iter().collect::<Vec<_>>();

        // We sort using the enum as neurons and outputs will be in the right order that way.
        connections.sort_unstable_by(|&(c1, _), &(c2, _)| c1.cmp(c2));

        for (index, (connection, weight)) in connections.into_iter().enumerate() {
            if index > 0 {
                // We write a new line here for the *last* written connection, so that the very
                // last one does not end with a newline, giving the user more freedom to decide what
                // to do.
                writeln!(graph).expect("writing failed");
            }

            let line = match *connection {
                Connection::InputToOutput(from, to) => {
                    format!("i{from}-- \"{weight}\" --> o{to}")
                }
                Connection::InputToHidden(from, to) => {
                    format!("i{from}-- \"{weight}\" --> n{to}")
                }
                Connection::HiddenToHidden(from, to) => {
                    format!("n{from}-- \"{weight}\" --> n{to}")
                }
                Connection::HiddenToOutput(from, to) => {
                    format!("n{from}-- \"{weight}\" --> o{to}")
                }
                Connection::BiasToHidden(to) => {
                    format!("bias-- \"{weight}\" --> n{to}")
                }
                Connection::BiasToOutput(to) => {
                    format!("bias-- \"{weight}\" --> o{to}")
                }
            };

            write!(graph, "\t{line}").expect("writing failed");
        }

        graph
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use crate::neural::{
        network::{
            recurrent::{Connection, Recurrent},
            Network,
        },
        neuron::Neuron,
    };

    #[test]
    #[allow(clippy::float_cmp)]
    fn most_basic_network() {
        let mut network = Network::new().recurrent();
        network.set_inputs(1.0);

        assert!(network.structure.hidden.is_empty());
        assert!(network.structure.connections.is_empty());
        assert_eq!(network.bias, 0.0);
        assert_eq!(network.input, [1.0]);
        assert_eq!(network.output.len(), 1);
        assert_eq!(network.get_outputs::<f64>(), 0.0);

        network.connect_input_to_output(0, 0, 0.25);

        let expected_direction = Connection::InputToOutput(0, 0);
        let expected_weight = 0.25;

        assert_eq!(
            network.structure.connections.iter().collect::<Vec<_>>(),
            vec![(&expected_direction, &expected_weight)]
        );

        network.sum_outputs();

        assert_eq!(network.output[0].inputs, &[0.25]);
        assert_eq!(network.output[0].output, 0.);

        network.activate_neurons();

        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.25);
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn two_inputs() {
        let mut network = Network::new().recurrent();
        network.set_inputs([1.0, 2.0]);

        network.connect_input_to_output(0, 0, 0.5);
        network.connect_input_to_output(1, 0, 1.0);

        network.tick();

        // input0: 1.0 * 0.5 = 0.5
        // input1: 2.0 * 1.0 = 2.0
        // given the default identity function, we should expect 2.5
        assert_eq!(network.get_outputs::<f64>(), 2.5)
    }

    #[test]
    #[allow(clippy::float_cmp)]
    fn one_input_and_bias() {
        let mut network = Network::new().recurrent().with_bias(2.0);
        network.set_inputs(1.0);

        network.connect_input_to_output(0, 0, 0.5);
        network.connect_bias_to_output(0, 1.0);

        network.tick();

        // input0: 1.0 * 0.5 = 0.5
        // bias: 2.0 * 1.0 = 2.0
        // given the default identity function, we should expect 2.5
        assert_eq!(network.get_outputs::<f64>(), 2.5)
    }

    #[test]
    #[allow(clippy::float_cmp, clippy::indexing_slicing)]
    fn simple_with_one_hidden() {
        let mut network = Network::new::<1, 1>().recurrent();
        network.set_inputs(1.0);

        network.add_hidden(Neuron::default());

        network.connect_input_to_hidden(0, 0, 0.5);
        network.connect_hidden_to_output(0, 0, 0.5);

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 0.0);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.0);

        network.tick();

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 0.5);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.0);

        network.tick();

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 0.5);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.25);
    }

    #[test]
    #[allow(clippy::float_cmp, clippy::indexing_slicing)]
    fn simple_with_recursive_hidden() {
        let mut network = Network::new::<1, 1>().recurrent();
        network.set_inputs(10.0);

        network.add_hidden(Neuron::default());

        network.connect_input_to_hidden(0, 0, 1.);
        network.connect_hidden_to_output(0, 0, 1.);
        network.connect_hidden_to_self(0, 1.);

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 0.0);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.0);

        network.tick();

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 10.);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 0.0);

        network.tick();

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 20.);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 10.);

        network.tick();

        assert!(network.structure.hidden[0].inputs.is_empty());
        assert_eq!(network.structure.hidden[0].output, 30.);
        assert!(network.output[0].inputs.is_empty());
        assert_eq!(network.output[0].output, 20.);
    }

    #[test]
    fn serialize() {
        let mut structure = Recurrent::default();
        structure.connections.insert(Connection::InputToOutput(0, 1), 0.5);

        let json = serde_json::to_string_pretty(&structure).unwrap();

        let expected_json = r#"{
  "hidden": [],
  "connections": [
    [
      {
        "InputToOutput": [
          0,
          1
        ]
      },
      0.5
    ]
  ]
}"#;

        assert_eq!(expected_json, json);
    }
}
