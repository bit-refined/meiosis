use core::{
    fmt::{Display, Write},
    iter::once,
};
use std::collections::{hash_map::Entry, HashMap};

#[cfg(feature = "serialization")]
use serde::{Deserialize, Serialize};
#[cfg(feature = "serialization")]
use serde_with::{serde_as, As, Same};

use crate::neural::{
    connection::{InputIndex, NeuronIndex, OutputIndex},
    network::Network,
    neuron::Neuron,
    value::Value,
};

/// MISSING DOCS
type LayerIndex = usize;

/// MISSING DOCS
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
enum TickState {
    /// MISSING DOCS
    #[default]
    InputToOutput,
    /// MISSING DOCS
    Input,
    /// MISSING DOCS
    LayerToLayer(LayerIndex),
    /// MISSING DOCS
    Output,
}

/// MISSING DOCS
#[derive(Debug, Clone, Copy, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serialization", derive(Serialize, Deserialize))]
#[allow(clippy::exhaustive_enums)]
pub enum Connection {
    /// A connection from the previous layer.
    FromPrevious {
        /// MISSING DOCS
        from: NeuronIndex,
        /// MISSING DOCS
        to: NeuronIndex,
    },
    /// Layer-independent bias connection.
    FromBias {
        /// MISSING DOCS
        to: NeuronIndex,
    },
}

/// MISSING DOCS
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(
    feature = "serialization",
    serde_as,
    derive(Serialize, Deserialize),
    serde(bound(serialize = "T: Value", deserialize = "T: Value"))
)]
pub struct Layer<T>
where
    T: Value,
{
    /// MISSING DOCS
    hidden: Vec<Neuron<T>>,
    /// MISSING DOCS
    #[cfg_attr(feature = "serialization", serde(with = "As::<Vec<(Same, Same)>>"))]
    connections: HashMap<Connection, T>,
}

impl<T> Layer<T>
where
    T: Value,
{
    /// MISSING DOCS
    #[must_use]
    pub fn neurons(&self) -> &[Neuron<T>] {
        &self.hidden
    }

    /// MISSING DOCS
    #[must_use]
    pub const fn connections(&self) -> &HashMap<Connection, T> {
        &self.connections
    }
}

/// This kind of network can not form any loops and is organized in layers.
/// Because of it's structure it can be evaluated instantly, which makes its recommended use case
/// that of function approximation.
///
/// # Aliases
/// Feedforward
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(
    feature = "serialization",
    derive(Serialize, Deserialize),
    serde(bound(serialize = "T: Value", deserialize = "T: Value"))
)]
pub struct FeedForward<T>
where
    T: Value,
{
    /// MISSING DOCS
    tick: TickState,
    /// MISSING DOCS
    pub(crate) layers: Vec<Layer<T>>,
    /// MISSING DOCS
    #[cfg_attr(feature = "serialization", serde(with = "As::<Vec<(Same, Same)>>"))]
    pub(crate) output_connections: HashMap<Connection, T>,
}

impl<T> FeedForward<T>
where
    T: Value,
{
    /// MISSING DOCS
    #[must_use]
    pub fn new() -> Self {
        Self {
            tick: TickState::InputToOutput,
            layers: vec![],
            output_connections: HashMap::new(),
        }
    }
}

/// Feedforward network specific methods.
impl<T, const IN: usize, const OUT: usize> Network<IN, OUT, T, FeedForward<T>>
where
    T: Value,
{
    /// Evaluates the network net input to output through all layers by ticking it `amount_of_layers + 1`
    /// times. Because this kind of network does not have loops it is always evaluated "forwards"
    /// layer to layer, giving a function-approximating result as the output.
    pub fn evaluate(&mut self) {
        match self.structure.layers.len() {
            0 => {
                self.structure.tick = TickState::InputToOutput;
                self.tick();
            }
            1 => {
                self.structure.tick = TickState::Input;
                self.tick();
                self.structure.tick = TickState::Output;
                self.tick();
            }
            layers => {
                self.structure.tick = TickState::Input;
                self.tick();

                for layer in 1..layers {
                    self.structure.tick = TickState::LayerToLayer(layer);
                    self.tick();
                }

                self.structure.tick = TickState::Output;
                self.tick();
            }
        }
    }

    /// MISSING DOCS
    fn tick(&mut self) {
        self.sum_outputs();
        self.activate_neurons();
    }

    /// For every input, bias and hidden neuron we use its output to add up values in connected
    /// neurons.
    fn sum_outputs(&mut self) {
        // Connections are always considered to be correct, so any indexing related to stored indices
        // should be correct. We also consider any arithmetic in here to be correct as it is a basic
        // requirement for the algorithm to function properly.
        #[allow(clippy::indexing_slicing, clippy::arithmetic_side_effects)]
        match self.structure.tick {
            TickState::InputToOutput => {
                for (connection, weight) in &self.structure.output_connections {
                    #[allow(clippy::shadow_reuse)]
                    let weight = weight.clone();

                    match *connection {
                        Connection::FromPrevious { from, to } => {
                            let transmitted_value = self.input[from].clone() * weight;
                            self.output[to].inputs.push(transmitted_value);
                        }
                        Connection::FromBias { to } => {
                            let transmitted_value = self.bias.clone() * weight;
                            self.output[to].inputs.push(transmitted_value);
                        }
                    }
                }
            }
            TickState::Input => {
                let layer = &mut self.structure.layers[0];
                let neurons = &mut layer.hidden;
                let connections = &layer.connections;

                for (connection, weight) in connections {
                    #[allow(clippy::shadow_reuse)]
                    let weight = weight.clone();

                    match *connection {
                        Connection::FromPrevious { from, to } => {
                            let transmitted_value = self.input[from].clone() * weight;
                            neurons[to].inputs.push(transmitted_value);
                        }
                        Connection::FromBias { to } => {
                            let transmitted_value = self.bias.clone() * weight;
                            neurons[to].inputs.push(transmitted_value);
                        }
                    }
                }
            }
            TickState::LayerToLayer(layer_index) => {
                // `LayerToLayer` is only used in `evaluate()` when there's at least two layers, so this can not fail.
                let [ref mut previous_layer, ref mut layer] = self.structure.layers[layer_index - 1..=layer_index]
                else {
                    unreachable!()
                };
                let neurons = &mut layer.hidden;

                for (connection, weight) in &layer.connections {
                    #[allow(clippy::shadow_reuse)]
                    let weight = weight.clone();

                    match *connection {
                        Connection::FromPrevious { from, to } => {
                            let transmitted_value = previous_layer.hidden[from].output.clone() * weight;
                            neurons[to].inputs.push(transmitted_value);
                        }
                        Connection::FromBias { to } => {
                            let transmitted_value = self.bias.clone() * weight;
                            neurons[to].inputs.push(transmitted_value);
                        }
                    }
                }
            }
            TickState::Output => {
                for (connection, weight) in &self.structure.output_connections {
                    #[allow(clippy::shadow_reuse)]
                    let weight = weight.clone();

                    match *connection {
                        Connection::FromPrevious { from, to } => {
                            let transmitted_value = self.structure.layers[self.structure.layers.len() - 1].hidden[from]
                                .output
                                .clone()
                                * weight;
                            self.output[to].inputs.push(transmitted_value);
                        }
                        Connection::FromBias { to } => {
                            let transmitted_value = self.bias.clone() * weight;
                            self.output[to].inputs.push(transmitted_value);
                        }
                    }
                }
            }
        }
    }

    /// Using the stored sums in each hidden and output neuron, we calculate its final output using
    /// the activation function.
    fn activate_neurons(&mut self) {
        // Because we evaluate the network layer by layer, we do not need to activate all neurons
        // just yet.

        /// MISSING DOCS
        fn activate_slice<T>(neurons: &mut [Neuron<T>])
        where
            T: Value,
        {
            for neuron in neurons {
                neuron.activate();
            }
        }

        // These tick states are calculated beforehand and will therefore be safe.
        #[allow(clippy::indexing_slicing)]
        match self.structure.tick {
            TickState::InputToOutput | TickState::Output => {
                activate_slice(&mut self.output);
            }
            TickState::Input => activate_slice(&mut self.structure.layers[0].hidden),
            TickState::LayerToLayer(layer) => activate_slice(&mut self.structure.layers[layer].hidden),
        }
    }

    /// MISSING DOCS
    fn assert_valid_layer(&self, layer: LayerIndex) {
        assert!(
            layer < self.structure.layers.len(),
            "trying to access non-existent layer {}; {} layers available",
            layer,
            self.structure.layers.len(),
        );
    }

    /// MISSING DOCS
    fn assert_valid_hidden(&self, layer: LayerIndex, neuron: NeuronIndex) {
        self.assert_valid_layer(layer);

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        let hidden_count = self.structure.layers[layer].hidden.len();

        assert!(
            neuron < hidden_count,
            "trying to access non-existent hidden neuron {neuron} in layer {layer}; {hidden_count} neurons available",
        );
    }

    /// MISSING DOC
    /// # Panics
    /// Panics if the connection already exits
    #[allow(clippy::panic)]
    pub fn connect_input_to_output(&mut self, input: InputIndex, output: OutputIndex, weight: T) {
        Self::assert_valid_input(input);
        Self::assert_valid_output(output);

        assert!(
            self.structure.layers.is_empty(),
            "can not connect Input({input}) -> Output({output}) directly if a network contains hidden layers",
        );

        let direction = Connection::FromPrevious {
            from: input,
            to: output,
        };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Input({input}) -> Output({output}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_input_and_output(&mut self, input: InputIndex, output: OutputIndex) {
        Self::assert_valid_input(input);
        Self::assert_valid_output(output);

        let direction = Connection::FromPrevious {
            from: input,
            to: output,
        };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Input({input}) -> Output({output}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_input_to_hidden(&mut self, input: InputIndex, hidden: NeuronIndex, weight: T) {
        Self::assert_valid_input(input);
        self.assert_valid_hidden(0, hidden);

        let direction = Connection::FromPrevious {
            from: input,
            to: hidden,
        };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        match self.structure.layers[0].connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Input({input}) -> Hidden(0-{hidden}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_input_and_hidden(&mut self, input: InputIndex, hidden: NeuronIndex) {
        Self::assert_valid_input(input);
        self.assert_valid_hidden(0, hidden);

        let direction = Connection::FromPrevious {
            from: input,
            to: hidden,
        };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        match self.structure.layers[0].connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Input({input}) -> Hidden(0-{hidden}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_hidden_to_hidden(
        &mut self,
        from_neuron: NeuronIndex,
        to_layer: LayerIndex,
        to_neuron: NeuronIndex,
        weight: T,
    ) {
        assert!(to_layer > 0, "target layer can not be the first layer");

        #[allow(clippy::arithmetic_side_effects)]
        self.assert_valid_hidden(to_layer - 1, from_neuron);
        self.assert_valid_hidden(to_layer, to_neuron);

        let direction = Connection::FromPrevious {
            from: from_neuron,
            to: to_neuron,
        };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing, clippy::arithmetic_side_effects)]
        match self.structure.layers[to_layer].connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!(
                    "connection Hidden({}-{from_neuron}) -> Hidden({to_layer}-{to_neuron}) already exists",
                    to_layer - 1
                );
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_hidden_and_hidden(
        &mut self,
        from_neuron: NeuronIndex,
        to_layer: LayerIndex,
        to_neuron: NeuronIndex,
    ) {
        assert!(to_layer > 0, "target layer can not be the first layer");

        #[allow(clippy::arithmetic_side_effects)]
        self.assert_valid_hidden(to_layer - 1, from_neuron);
        self.assert_valid_hidden(to_layer, to_neuron);

        let direction = Connection::FromPrevious {
            from: from_neuron,
            to: to_neuron,
        };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing, clippy::arithmetic_side_effects)]
        match self.structure.layers[to_layer].connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!(
                    "connection Hidden({}-{from_neuron}) -> Hidden({to_layer}-{to_neuron}) does not exist",
                    to_layer - 1
                );
            }
        }
    }

    /// MISSING DOCS
    /// # Note
    /// Because only the last layer connects to the output, it is assumed that the neuron being
    /// referenced by its index is in the last layer.
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_hidden_to_output(&mut self, neuron: NeuronIndex, output: OutputIndex, weight: T) {
        // We're using saturating sub here for the case of there not being a hidden layer. If that
        // is the case, the hidden validation will panic.
        let last_layer = self.structure.layers.len().saturating_sub(1);
        self.assert_valid_hidden(last_layer, neuron);
        Self::assert_valid_output(output);

        let direction = Connection::FromPrevious {
            from: neuron,
            to: output,
        };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Hidden({last_layer}-{neuron}) -> Output({output}) already exists",);
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_hidden_and_output(&mut self, neuron: NeuronIndex, output: OutputIndex) {
        // We're using saturating sub here for the case of there not being a hidden layer. If that
        // is the case, the hidden validation will panic.
        let last_layer = self.structure.layers.len().saturating_sub(1);
        self.assert_valid_hidden(last_layer, neuron);
        Self::assert_valid_output(output);

        let direction = Connection::FromPrevious {
            from: neuron,
            to: output,
        };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Hidden({last_layer}-{neuron}) -> Output({output}) does not exist",);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_bias_to_hidden(&mut self, layer: LayerIndex, neuron: NeuronIndex, weight: T) {
        self.assert_valid_hidden(layer, neuron);

        let direction = Connection::FromBias { to: neuron };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        match self.structure.layers[layer].connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Bias -> Hidden({layer}-{neuron}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_bias_to_hidden(&mut self, layer: LayerIndex, neuron: NeuronIndex) {
        self.assert_valid_hidden(layer, neuron);

        let direction = Connection::FromBias { to: neuron };

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        match self.structure.layers[layer].connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Bias -> Hidden({layer}-{neuron}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection already exists.
    #[allow(clippy::panic)]
    pub fn connect_bias_to_output(&mut self, output: OutputIndex, weight: T) {
        Self::assert_valid_output(output);

        let direction = Connection::FromBias { to: output };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(_) => {
                panic!("connection Bias -> Output({output}) already exists");
            }
            Entry::Vacant(entry) => {
                let _inserted = entry.insert(weight);
            }
        }
    }

    /// MISSING DOCS
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn disconnect_bias_to_output(&mut self, output: OutputIndex) {
        Self::assert_valid_output(output);

        let direction = Connection::FromBias { to: output };

        match self.structure.output_connections.entry(direction) {
            Entry::Occupied(entry) => {
                let _removed = entry.remove();
            }
            Entry::Vacant(_) => {
                panic!("connection Bias -> Output({output}) does not exist");
            }
        }
    }

    /// MISSING DOCS
    pub fn has_output_connection(&self, direction: &Connection) -> bool {
        self.structure.output_connections.contains_key(direction)
    }

    /// MISSING DOCS
    pub fn has_layer_connection(&self, direction: &Connection, layer: LayerIndex) -> bool {
        self.assert_valid_layer(layer);

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        self.structure.layers[layer].connections.contains_key(direction)
    }

    /// MISSING DOCS
    pub fn add_hidden(&mut self, layer: LayerIndex, hidden: Neuron<T>) {
        self.assert_valid_layer(layer);

        // The layer is verified to exist so this is safe.
        #[allow(clippy::indexing_slicing)]
        self.structure.layers[layer].hidden.push(hidden);
    }

    /// MISSING DOCS
    pub fn remove_hidden(&mut self, layer: LayerIndex, hidden: NeuronIndex) {
        self.assert_valid_hidden(layer, hidden);

        // 1. remove neuron
        #[allow(clippy::indexing_slicing)]
        let _removed = self.structure.layers[layer].hidden.remove(hidden);

        // 2. remove all connections referring to it in the layer
        #[allow(clippy::indexing_slicing)]
        self.structure.layers[layer]
            .connections
            .retain(|connection, _| match *connection {
                Connection::FromPrevious { to, .. } | Connection::FromBias { to } => to != hidden,
            });

        // 3. remove all connections referring to it in the next layer (which could also be the output)

        // We know at this point, that this layer index is safe, meaning there is at least one layer,
        // which makes the arithmetic safe.
        #[allow(clippy::arithmetic_side_effects)]
        let last_layer = layer == self.structure.layers.len() - 1;

        // The condition makes sure there is a following layer, making indexing and arithmetic safe.
        #[allow(clippy::indexing_slicing, clippy::arithmetic_side_effects)]
        if last_layer {
            // if removed neuron is in the last layer we have to check the output
            &mut self.structure.output_connections
        } else {
            // otherwise we simply pick the next layer
            let next_layer = layer + 1;
            &mut self.structure.layers[next_layer].connections
        }
        .retain(|connection, _| match *connection {
            Connection::FromPrevious { from, .. } => from != hidden,
            Connection::FromBias { .. } => true,
        });

        // 4. adjust connections of layer because all following neurons will change index

        // Indexing is safe here because the function verified the layer index.
        #[allow(clippy::indexing_slicing)]
        let mut drained_layer = self.structure.layers[layer]
            .connections
            .extract_if(|connection, _| match *connection {
                Connection::FromPrevious { to, .. } | Connection::FromBias { to } => to > hidden,
            })
            .collect::<Vec<_>>();

        for &mut (ref mut connection, _) in &mut drained_layer {
            // The previous draining makes sure this value is at least bigger than `hidden`, which means
            // it is at least 1.
            #[allow(clippy::arithmetic_side_effects)]
            match *connection {
                Connection::FromPrevious { ref mut to, .. } | Connection::FromBias { ref mut to } => {
                    *to -= 1;
                }
            }
        }

        // The layer index is verified at the very beginning of this function, making indexing here safe.
        #[allow(clippy::indexing_slicing)]
        self.structure.layers[layer].connections.extend(drained_layer);

        // 5. do the same for the following layer (which could also be the output)

        // This arithmetic is safe because the condition makes sure there is a following layer which
        // also makes the indexing safe.
        #[allow(clippy::arithmetic_side_effects, clippy::indexing_slicing)]
        let drained_following_connections = if last_layer {
            &mut self.structure.output_connections
        } else {
            &mut self.structure.layers[layer + 1].connections
        };

        let mut follow_layer_connections = drained_following_connections
            .extract_if(|connection, _| match *connection {
                Connection::FromPrevious { from, .. } => from > hidden,
                Connection::FromBias { .. } => false,
            })
            .collect::<Vec<_>>();

        for &mut (ref mut connection, _) in &mut follow_layer_connections {
            // We know `from` is greater than `hidden`, we also know that `hidden` at the least was
            // zero, which means `from` is at least 1, making this arithmetic safe.
            #[allow(clippy::arithmetic_side_effects)]
            match *connection {
                Connection::FromPrevious { ref mut from, .. } => *from -= 1,
                Connection::FromBias { .. } => unreachable!(),
            }
        }

        drained_following_connections.extend(follow_layer_connections);
    }

    /// MISSING DOCS
    #[allow(clippy::indexing_slicing)]
    pub fn get_hidden(&self, layer: LayerIndex, hidden: NeuronIndex) -> &Neuron<T> {
        self.assert_valid_hidden(layer, hidden);

        &self.structure.layers[layer].hidden[hidden]
    }

    /// MISSING DOCS
    #[allow(clippy::indexing_slicing)]
    pub fn get_hidden_mut(&mut self, layer: LayerIndex, hidden: NeuronIndex) -> &mut Neuron<T> {
        self.assert_valid_hidden(layer, hidden);

        &mut self.structure.layers[layer].hidden[hidden]
    }

    /// This adds a new layer at the specified index. Existing connections between the layers
    /// surrounding this new one (including input and output) will be removed as they would otherwise
    /// invalidate the feedforward property.
    ///
    /// # Network state
    /// If executed on an existing connected network, it will no longer be connected from
    /// input to output and requires adding at least a single hidden neuron
    /// (as well as connections to and from it) to this new layer.
    /// # Panics
    /// Panics if the given index is greater than the amount of layers.
    pub fn add_layer(&mut self, index: LayerIndex) {
        let original_layer_count = self.structure.layers.len();

        // We need to make sure the index is valid for insertion.
        assert!(
            index <= original_layer_count,
            "index ({index}) of new layer is invalid; available range: [0, {original_layer_count}]",
        );

        #[allow(clippy::indexing_slicing)]
        let layer_to_clean_up = if index == original_layer_count {
            &mut self.structure.output_connections
        } else {
            // the existing layer at this position will become `index + 1`, so we can clean it up
            // before we insert the new layer
            &mut self.structure.layers[index].connections
        };

        // Only the bias is layer-unrelated so we can keep that for the layer
        layer_to_clean_up.retain(|connection, _| matches!(*connection, Connection::FromBias { .. }));

        self.structure.layers.insert(
            index,
            Layer {
                hidden: vec![],
                connections: HashMap::new(),
            },
        );
    }

    /// This removes the specified layer and cleans up all connections referring to it.
    ///
    /// # Network state
    /// If executed on an existing connected network, it will no longer be connected from input to
    /// output and requires at least a single connection from the previous to the following layer.
    /// # Panics
    /// Panics if there is no layer with the given index.
    pub fn remove_layer(&mut self, layer: LayerIndex) {
        let original_layer_count = self.structure.layers.len();

        // We need to make sure the index is valid for removal.
        assert!(
            layer < original_layer_count,
            "layer {layer} not found; available layers: [0, {original_layer_count})",
        );

        // The condition makes sure the layer we're accessing is either the output or the actual
        // following layer after the one we're about to delete, which also makes the indexing safe.
        #[allow(clippy::arithmetic_side_effects, clippy::indexing_slicing)]
        if layer == original_layer_count - 1 {
            // The layer is the last one, so we need to check output connections.
            &mut self.structure.output_connections
        } else {
            &mut self.structure.layers[layer + 1].connections
        }
        .retain(|connection, _| matches!(*connection, Connection::FromBias { .. }));

        let _removed = self.structure.layers.remove(layer);
    }

    /// MISSING DOCS
    // The layer index is verified and thus safe for indexing.
    #[allow(clippy::indexing_slicing)]
    pub fn get_layer(&self, layer: LayerIndex) -> &Layer<T> {
        self.assert_valid_layer(layer);

        &self.structure.layers[layer]
    }

    /// MISSING DOCS
    pub fn get_layers(&self) -> &[Layer<T>] {
        &self.structure.layers
    }

    /// MISSING DOCS
    pub const fn get_output_connections(&self) -> &HashMap<Connection, T> {
        &self.structure.output_connections
    }

    /// MISSING DOCS
    /// # Note
    /// If `layer` is set to `None` this will set the weight of output connections.
    /// # Panics
    /// Panics if the connection does not exist.
    #[allow(clippy::panic)]
    pub fn set_weight(&mut self, layer: Option<LayerIndex>, connection: Connection, weight: T) {
        // The layer is checked for validity and thus safe to use as an index.
        #[allow(clippy::indexing_slicing)]
        let connections = if let Some(index) = layer {
            self.assert_valid_layer(index);

            &mut self.structure.layers[index].connections
        } else {
            &mut self.structure.output_connections
        };

        match connections.entry(connection) {
            Entry::Occupied(mut entry) => {
                let _old = entry.insert(weight);
            }
            Entry::Vacant(entry) => {
                panic!("trying to set weight of invalid connection {:?}", entry.into_key());
            }
        }
    }

    /// MISSING DOCS
    pub fn reset(&mut self) {
        let neurons = self.output.iter_mut().chain(
            self.structure
                .layers
                .iter_mut()
                .flat_map(|layer| layer.hidden.iter_mut()),
        );

        for neuron in neurons {
            neuron.reset();
        }
    }

    /// Cleans up connections if their weight is zero. Also checks if the bias is zero, and if so,
    /// removes all bias connections as well.
    ///
    /// # Note
    /// Usually, as a user there is no need to run this. It is primarily used by the genetic algorithm
    /// to clean up the messy networks it creates, so that evaluation of networks is more performant.
    pub fn cleanup_connections(&mut self) {
        let all_connections = once(&mut self.structure.output_connections)
            .chain(self.structure.layers.iter_mut().map(|l| &mut l.connections));

        for connections in all_connections {
            connections.retain(|connection, weight| match *connection {
                Connection::FromPrevious { .. } => *weight != T::zero(),
                Connection::FromBias { .. } => !(self.bias == T::zero() || *weight == T::zero()),
            });
        }
    }

    /// Cleans up neurons if they have missing connections, as that makes them useless.
    ///
    /// It is recommended to run [`Self::cleanup_connections()`] first to make sure there are no
    /// connections left that in reality have no use either.
    ///
    /// # Note
    /// Usually, as a user there is no need to run this. It is primarily used by the genetic algorithm
    /// to clean up the messy networks it creates, so that evaluation of networks is more performant.
    pub fn cleanup_neurons(&mut self) {
        let layers = self.get_layers().len();

        // We flip the iteration direction so that we can safely delete neurons.
        for layer in (0..layers).rev() {
            for neuron in (0..self.get_layer(layer).neurons().len()).rev() {
                let connections = self.get_layer(layer).connections();
                // this arithmetic is safe as we can only reach this code if there is at least one layer
                #[allow(clippy::arithmetic_side_effects)]
                // if the layer is the last one, we need output connections, otherwise the next layer
                let next_layer_connections = if layer == layers - 1 {
                    self.get_output_connections()
                } else {
                    self.get_layer(layer).connections()
                };

                let has_incoming_connections = connections.iter().any(|(connection, _)| match *connection {
                    Connection::FromPrevious { to, .. } => to == neuron,
                    Connection::FromBias { .. } => false,
                });

                let has_outgoing_connections = next_layer_connections.iter().any(|(connection, _)| match *connection {
                    Connection::FromPrevious { from, .. } => from == neuron,
                    Connection::FromBias { .. } => false,
                });

                if !has_incoming_connections || !has_outgoing_connections {
                    self.remove_hidden(layer, neuron);

                    if self.get_layer(layer).neurons().is_empty() {
                        self.remove_layer(layer);
                    }
                }
            }
        }
    }

    /// MISSING DOCS
    #[allow(clippy::too_many_lines)]
    pub fn render_mermaid(&self) -> String
    where
        T: Display,
    {
        /// MISSING DOCS
        #[derive(Ord, PartialOrd, Eq, PartialEq)]
        enum SortedConnection {
            /// MISSING DOCS
            InputOutput(NeuronIndex, NeuronIndex),
            /// MISSING DOCS
            Input(NeuronIndex, NeuronIndex),
            /// MISSING DOCS
            Layer(LayerIndex, NeuronIndex, LayerIndex, NeuronIndex),
            /// MISSING DOCS
            Output(NeuronIndex, NeuronIndex),
            /// MISSING DOCS
            LayerBias(LayerIndex, NeuronIndex),
            /// MISSING DOCS
            OutputBias(NeuronIndex),
        }
        let mut connections = Vec::new();

        let mut graph = String::new();

        graph.push_str("graph LR\n");

        // Inputs:
        graph.push_str("\tsubgraph Inputs\n");
        graph.push_str("\t\tdirection TB\n");

        for (index, _) in self.input.iter().enumerate() {
            writeln!(graph, "\t\ti{index}((\"Input {index}\"))").expect("writing failed");
        }
        // we count the bias also as an input
        if self.bias != T::zero()
            && self.structure.layers.iter().any(|layer| {
                layer
                    .connections
                    .keys()
                    .any(|k| matches!(*k, Connection::FromBias { .. }))
            })
        {
            writeln!(graph, "\t\tbias((\"Bias: {}\"))", self.bias).expect("writing failed");
        }

        graph.push_str("\tend\n\n");

        // Layers and Neurons:
        for (index, layer) in self.structure.layers.iter().enumerate() {
            writeln!(graph, "\tsubgraph Layer {index}").expect("writing failed");
            graph.push_str("\t\tdirection TB\n");

            for (neuron_index, neuron) in layer.hidden.iter().enumerate() {
                writeln!(
                    graph,
                    "\t\tn{index}_{neuron_index}((\"Neuron {neuron_index}<br>{}\"))",
                    neuron.activation_function
                )
                .expect("writing failed");
            }

            // Index slicing is safe here because the index comes from the above `for`.
            #[allow(clippy::indexing_slicing)]
            for (connection, weight) in &self.structure.layers[index].connections {
                match *connection {
                    Connection::FromPrevious { from, to } => {
                        // The condition makes sure that this arithmetic is safe.
                        #[allow(clippy::arithmetic_side_effects)]
                        if index == 0 {
                            // the first layer always goes from the input
                            connections.push((SortedConnection::Input(from, to), weight));
                        } else {
                            connections.push((SortedConnection::Layer(index - 1, from, index, to), weight));
                        }
                    }
                    Connection::FromBias { to } => {
                        connections.push((SortedConnection::LayerBias(index, to), weight));
                    }
                }
            }

            graph.push_str("\tend\n\n");
        }

        // Outputs:
        graph.push_str("\tsubgraph Outputs\n");
        graph.push_str("\t\tdirection TB\n");
        for (index, out) in self.output.iter().enumerate() {
            writeln!(
                graph,
                "\t\to{index}((\"Output {index}<br>{}\"))",
                out.activation_function
            )
            .expect("writing failed");
        }
        graph.push_str("\tend\n\n");

        // Finally, output connections:
        for (connection, weight) in &self.structure.output_connections {
            match *connection {
                Connection::FromPrevious { from, to } => {
                    if self.structure.layers.is_empty() {
                        // if there's no layers this is input to output
                        connections.push((SortedConnection::InputOutput(from, to), weight));
                    } else {
                        connections.push((SortedConnection::Output(from, to), weight));
                    }
                }
                Connection::FromBias { to } => {
                    connections.push((SortedConnection::OutputBias(to), weight));
                }
            }
        }

        // We sort using the enum as neurons and outputs will be in the right order that way.
        #[allow(clippy::pattern_type_mismatch)]
        connections.sort_unstable_by(|(c1, _), (c2, _)| c1.cmp(c2));

        for (index, (connection, weight)) in connections.into_iter().enumerate() {
            if index > 0 {
                // We write a new line here for the *last* written connection, so that the very
                // last one does not end with a newline, giving the user more freedom to decide what
                // to do.
                writeln!(graph).expect("writing failed");
            }

            let line = match connection {
                SortedConnection::InputOutput(from, to) => {
                    format!("i{from}-- \"{weight}\" --> o{to}")
                }
                SortedConnection::Input(from, to) => {
                    format!("i{from}-- \"{weight}\" --> n0_{to}")
                }
                SortedConnection::Layer(from_layer, from, to_layer, to) => {
                    format!("n{from_layer}_{from}-- \"{weight}\" --> n{to_layer}_{to}")
                }
                // This arithmetic is safe as this kind of connection is only possible if there
                // is at least one layer.
                #[allow(clippy::arithmetic_side_effects)]
                SortedConnection::Output(from, to) => {
                    format!("n{}_{from}-- \"{weight}\" --> o{to}", self.structure.layers.len() - 1)
                }
                SortedConnection::LayerBias(layer, to) => {
                    format!("bias-- \"{weight}\" --> n{layer}_{to}")
                }
                SortedConnection::OutputBias(to) => {
                    format!("bias-- \"{weight}\" --> o{to}")
                }
            };

            write!(graph, "\t{line}").expect("writing failed");
        }

        graph
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;

    use crate::neural::network::{
        feedforward::{Connection, Layer},
        Network,
    };

    #[test]
    #[allow(clippy::float_cmp)]
    fn most_basic_network() {
        let mut network = Network::new().feedforward();
        network.set_inputs(1.0_f64);

        assert!(network.structure.layers.is_empty());
        assert!(network.structure.output_connections.is_empty());
        assert_eq!(network.input, [1.0]);
        assert_eq!(network.get_outputs::<f64>(), 0.0);

        network.connect_input_to_output(0, 0, 0.25);

        let expected_direction = Connection::FromPrevious { from: 0, to: 0 };
        let expected_weight = 0.25;

        assert_eq!(
            network.structure.output_connections.iter().collect::<Vec<_>>(),
            vec![(&expected_direction, &expected_weight)]
        );
    }

    #[test]
    fn serialize_layer() {
        let mut layer = Layer::default();
        layer
            .connections
            .insert(Connection::FromPrevious { from: 0, to: 1 }, 0.5);

        let json = serde_json::to_string_pretty(&layer).unwrap();

        let expected_json = r#"{
  "hidden": [],
  "connections": [
    [
      {
        "FromPrevious": {
          "from": 0,
          "to": 1
        }
      },
      0.5
    ]
  ]
}"#;

        assert_eq!(expected_json, json);
    }
}
