use rand::Rng;

use crate::{environment::Environment, genotype::Genotype};

/// MISSING DOCS
#[cfg(feature = "neural_networks")]
pub mod network;

/// # Aliases
/// Agent, Creature, Individual, Solution
pub trait Phenotype {
    /// This is the type used to construct the phenotype.
    /// Simple optimization problems may even opt to use the exact same type as the phenotype.
    type Genotype: Genotype;
}

/// Constructing a [`Phenotype`] is used to potentially differentiate between the genetic and final
/// representation.
pub trait FromGenotype<GENOTYPE, ENVIRONMENT>: Phenotype
where
    GENOTYPE: Genotype,
    ENVIRONMENT: Environment,
{
    /// [`Phenotype`]s may "grow up" different depending on their environment or random
    /// influences - just like their biological counterparts.
    fn from_genotype<RNG>(genotype: &GENOTYPE, environment: &ENVIRONMENT, rng: &mut RNG) -> Self
    where
        RNG: Rng;
}
