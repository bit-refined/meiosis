// Send

#[cfg(not(feature = "multithreading"))]
/// MISSING DOCS
pub trait MaybeSend {}

#[cfg(not(feature = "multithreading"))]
/// MISSING DOCS
impl<T> MaybeSend for T {}

#[cfg(feature = "multithreading")]
/// MISSING DOCS
pub trait MaybeSend: Send {}

#[cfg(feature = "multithreading")]
/// MISSING DOCS
impl<T: Send> MaybeSend for T {}

// Sync

#[cfg(not(feature = "multithreading"))]
/// MISSING DOCS
pub trait MaybeSync {}

#[cfg(not(feature = "multithreading"))]
/// MISSING DOCS
impl<T> MaybeSync for T {}

#[cfg(feature = "multithreading")]
/// MISSING DOCS
pub trait MaybeSync: Sync {}

#[cfg(feature = "multithreading")]
/// MISSING DOCS
impl<T: Sync> MaybeSync for T {}
