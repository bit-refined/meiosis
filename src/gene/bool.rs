use rand::Rng;

use crate::random::Random;

use super::Gene;

impl Gene for bool {}

impl Random for bool {
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
    {
        rng.gen()
    }
}
