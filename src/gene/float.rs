use rand::Rng;

use crate::{
    gene::Gene,
    neighbour::{DistancedNeighbour, Neighbour},
    random::Random,
};

impl Gene for f32 {}
impl Random for f32 {
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized,
    {
        rng.gen()
    }
}
impl DistancedNeighbour<f32> for f32 {
    #[allow(clippy::float_arithmetic, clippy::arithmetic_side_effects)]
    fn random_forward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &f32) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self + maximal_distance;
        possible.is_finite().then(|| rng.gen_range(*self..=possible))
    }

    #[allow(clippy::float_arithmetic, clippy::arithmetic_side_effects)]
    fn random_backward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &f32) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self - maximal_distance;
        possible.is_finite().then(|| rng.gen_range(*self..=possible))
    }
}

impl Neighbour for f32 {
    fn successor<RNG>(&self, _rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self.next_up();
        possible.is_finite().then_some(possible)
    }

    fn predecessor<RNG>(&self, _rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self.next_down();
        possible.is_finite().then_some(possible)
    }
}

impl Gene for f64 {}
impl Random for f64 {
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized,
    {
        rng.gen()
    }
}
impl DistancedNeighbour<f64> for f64 {
    #[allow(clippy::float_arithmetic, clippy::arithmetic_side_effects)]
    fn random_forward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &f64) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self + maximal_distance;
        possible.is_finite().then(|| rng.gen_range(*self..=possible))
    }

    #[allow(clippy::float_arithmetic, clippy::arithmetic_side_effects)]
    fn random_backward_neighbour<RNG>(&self, rng: &mut RNG, maximal_distance: &f64) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self - maximal_distance;
        possible.is_finite().then(|| rng.gen_range(possible..=*self))
    }
}

impl Neighbour for f64 {
    fn successor<RNG>(&self, _rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self.next_up();
        possible.is_finite().then_some(possible)
    }

    fn predecessor<RNG>(&self, _rng: &mut RNG) -> Option<Self>
    where
        RNG: Rng,
    {
        let possible = self.next_down();
        possible.is_finite().then_some(possible)
    }
}
