use core::{
    fmt::Debug,
    hash::{Hash, Hasher},
};

use crate::{
    gene::Gene,
    neural::{activation::ActivationType, value::Value},
};

/// These are the basic instructions used to construct any feedforward network net.
#[derive(Clone, Debug)]
pub enum Instruction<T>
where
    T: Value,
{
    /// Adds a new neuron with an activation function set to [`Identity`].
    Neuron,
    /// Sets the activation function of the neuron the layer pointer is set to.
    LayerActivationFunction(<T as ActivationType>::Function),
    /// Sets the activation function of the neuron the output pointer is set to.
    OutputActivationFunction(<T as ActivationType>::Function),
    /// Moves origin pointer right.
    NextOrigin,
    /// Moves origin pointer left.
    PreviousOrigin,
    /// Selects the next neuron in the latest layer.
    NextLayerNeuron,
    /// Selects the previous neuron in the latest layer.
    PreviousLayerNeuron,
    /// Moves output pointer right.
    NextOutput,
    /// Moves output pointer left.
    PreviousOutput,
    /// Creates a connection between the selected origin and layer neuron.
    /// If there is no hidden layer yet, connects input to output.
    ConnectTo,
    /// Creates a connection between the selected layer and output neuron.
    /// If there is no hidden layer yet, connects input to output.
    ConnectFrom,
    /// Connects the network bias to the selected layer neuron.
    Bias,
    /// Connects the network bias to the selected output neuron.
    OutputBias,
    /// Sets the weight of the most recently added connection.
    Weight(T),
    /// Adds a new layer, sets the layer tape to it and unless its the first layer sets the
    /// origin tape to the previous layer. This will also insert a single neuron into this layer
    /// to help the genetic algorithm, otherwise we would end up with empty layers, which is invalid.
    Layer,
}

impl<T> Gene for Instruction<T> where T: Value {}

impl<T> PartialEq for Instruction<T>
where
    T: Value,
{
    fn eq(&self, other: &Self) -> bool {
        #[allow(clippy::pattern_type_mismatch)]
        match (self, other) {
            (Instruction::LayerActivationFunction(af1), Instruction::LayerActivationFunction(af2))
            | (Instruction::OutputActivationFunction(af1), Instruction::OutputActivationFunction(af2)) => af1 == af2,
            (Instruction::Weight(w1), Instruction::Weight(w2)) => w1 == w2,
            (Instruction::Neuron, Instruction::Neuron)
            | (Instruction::NextOrigin, Instruction::NextOrigin)
            | (Instruction::PreviousOrigin, Instruction::PreviousOrigin)
            | (Instruction::NextLayerNeuron, Instruction::NextLayerNeuron)
            | (Instruction::PreviousLayerNeuron, Instruction::PreviousLayerNeuron)
            | (Instruction::NextOutput, Instruction::NextOutput)
            | (Instruction::PreviousOutput, Instruction::PreviousOutput)
            | (Instruction::ConnectTo, Instruction::ConnectTo)
            | (Instruction::ConnectFrom, Instruction::ConnectFrom)
            | (Instruction::Bias, Instruction::Bias)
            | (Instruction::OutputBias, Instruction::OutputBias)
            | (Instruction::Layer, Instruction::Layer) => true,
            (_, _) => false,
        }
    }
}

impl Hash for Instruction<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            Instruction::Neuron => state.write_u8(0),
            Instruction::LayerActivationFunction(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            Instruction::OutputActivationFunction(ref a) => {
                state.write_u8(2);
                a.hash(state);
            }
            Instruction::NextOrigin => state.write_u8(3),
            Instruction::PreviousOrigin => state.write_u8(4),
            Instruction::NextLayerNeuron => state.write_u8(5),
            Instruction::PreviousLayerNeuron => state.write_u8(6),
            Instruction::NextOutput => state.write_u8(7),
            Instruction::PreviousOutput => state.write_u8(8),
            Instruction::ConnectTo => state.write_u8(9),
            Instruction::ConnectFrom => state.write_u8(10),
            Instruction::Bias => state.write_u8(11),
            Instruction::OutputBias => state.write_u8(12),
            Instruction::Weight(ref weight) => {
                debug_assert!(!weight.is_nan());

                state.write_u8(13);
                state.write_u32(weight.to_bits());
            }
            Instruction::Layer => state.write_u8(14),
        }
    }
}

impl Hash for Instruction<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            Instruction::Neuron => state.write_u8(0),
            Instruction::LayerActivationFunction(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            Instruction::OutputActivationFunction(ref a) => {
                state.write_u8(2);
                a.hash(state);
            }
            Instruction::NextOrigin => state.write_u8(3),
            Instruction::PreviousOrigin => state.write_u8(4),
            Instruction::NextLayerNeuron => state.write_u8(5),
            Instruction::PreviousLayerNeuron => state.write_u8(6),
            Instruction::NextOutput => state.write_u8(7),
            Instruction::PreviousOutput => state.write_u8(8),
            Instruction::ConnectTo => state.write_u8(9),
            Instruction::ConnectFrom => state.write_u8(10),
            Instruction::Bias => state.write_u8(11),
            Instruction::OutputBias => state.write_u8(12),
            Instruction::Weight(ref weight) => {
                debug_assert!(!weight.is_nan());

                state.write_u8(13);
                state.write_u64(weight.to_bits());
            }
            Instruction::Layer => state.write_u8(14),
        }
    }
}
