use core::{
    fmt::Debug,
    hash::{Hash, Hasher},
};

use crate::{
    gene::Gene,
    neural::{activation::ActivationType, value::Value},
};

/// These are the basic instructions used to construct any recurrent network net.
#[derive(Clone, Debug)]
pub enum Instruction<T>
where
    T: Value,
{
    /// Adds a new neuron with an activation function set to [`Identity`].
    Neuron,
    /// Sets the activation function of the neuron the destination pointer is set to.
    ActivationFunction(<T as ActivationType>::Function),
    /// Moves origin pointer right.
    NextOrigin,
    /// Moves origin pointer left.
    PreviousOrigin,
    /// Moves destination pointer right.
    NextDestination,
    /// Moves destination pointer left.
    PreviousDestination,
    /// Creates a connection between the stored connection at the origin and destination pointer.
    /// If any of the two variables is not set, does nothing.
    Connect,
    /// Connects the connection destination with the network bias.
    ConnectBias,
    /// Sets the weight of the most recently added connection.
    Weight(T),
}

impl<T> Gene for Instruction<T> where T: Value {}

impl<T> PartialEq for Instruction<T>
where
    T: Value,
{
    fn eq(&self, other: &Self) -> bool {
        #[allow(clippy::pattern_type_mismatch)]
        match (self, other) {
            (Instruction::ActivationFunction(af1), Instruction::ActivationFunction(af2)) => af1 == af2,
            (Instruction::Weight(w1), Instruction::Weight(w2)) => w1 == w2,
            (Instruction::Neuron, Instruction::Neuron)
            | (Instruction::NextOrigin, Instruction::NextOrigin)
            | (Instruction::PreviousOrigin, Instruction::PreviousOrigin)
            | (Instruction::NextDestination, Instruction::NextDestination)
            | (Instruction::PreviousDestination, Instruction::PreviousDestination)
            | (Instruction::Connect, Instruction::Connect)
            | (Instruction::ConnectBias, Instruction::ConnectBias) => true,
            (_, _) => false,
        }
    }
}

impl Hash for Instruction<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            Instruction::Neuron => state.write_u8(0),
            Instruction::ActivationFunction(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            Instruction::NextOrigin => state.write_u8(2),
            Instruction::PreviousOrigin => state.write_u8(3),
            Instruction::NextDestination => state.write_u8(4),
            Instruction::PreviousDestination => state.write_u8(5),
            Instruction::Connect => state.write_u8(6),
            Instruction::ConnectBias => state.write_u8(7),
            Instruction::Weight(ref weight) => {
                state.write_u8(8);

                debug_assert!(!weight.is_nan());
                state.write_u32(weight.to_bits());
            }
        }
    }
}

impl Hash for Instruction<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            Instruction::Neuron => state.write_u8(0),
            Instruction::ActivationFunction(ref a) => {
                state.write_u8(1);
                a.hash(state);
            }
            Instruction::NextOrigin => state.write_u8(2),
            Instruction::PreviousOrigin => state.write_u8(3),
            Instruction::NextDestination => state.write_u8(4),
            Instruction::PreviousDestination => state.write_u8(5),
            Instruction::Connect => state.write_u8(6),
            Instruction::ConnectBias => state.write_u8(7),
            Instruction::Weight(ref weight) => {
                state.write_u8(8);

                debug_assert!(!weight.is_nan());
                state.write_u64(weight.to_bits());
            }
        }
    }
}
