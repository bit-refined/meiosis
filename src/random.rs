use rand::{Rng, RngCore, SeedableRng};

/// Some mutations or the creation of a new population may require to create random [`crate::gene::Gene`]s and
/// [`crate::genotype::Genotype`]s.
pub trait Random {
    /// This function should produce a fully random instance of the type it is being implemented for.
    fn random<RNG>(rng: &mut RNG) -> Self
    where
        RNG: Rng,
        Self: Sized;

    /// To reliably produce the same instance of a type, a seed can be used. This is for example
    /// very useful in tests.
    fn seeded<R, S>(seed: S) -> Self
    where
        R: RngCore + SeedableRng<Seed = S>,
        Self: Sized,
    {
        let mut rng = R::from_seed(seed);
        Self::random(&mut rng)
    }
}
