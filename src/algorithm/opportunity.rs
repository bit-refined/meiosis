//! Opportunity based selection has agents in a potentially shared environment in which they may
//! compete for resources.

/// This algorithm keeps the `Environment` and all `Phenotypes` persistent, allowing for continuous
/// evolution in a possibly always-changing environment.
///
/// This kind of `Algorithm` is often used to simulate artificial life.
#[allow(dead_code)]
struct Persistent;

/// This algorithm resets the `Environment` and all `Phenotypes` once a Generation is over, but still
/// allows for a shared environment
#[allow(dead_code)]
struct Generational;
