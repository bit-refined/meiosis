use core::fmt::Debug;

/// [`Gene`] and related implementations for [`core::bool`].
mod bool;
/// [`Gene`] and related implementations for [`core::char`].
mod char;
/// [`Gene`] and related implementations for [`core::f32`] and [`core::f64`].
mod float;
/// [`Gene`] and related implementations for primitive integers.
mod integers;
/// [`Gene`] and related implementations for neural networks.
#[cfg(feature = "neural_networks")]
pub(crate) mod network;

/// This trait is used to describe singular genes that make up sequences.
///
/// # Note
/// At the moment it is simply a marker trait.
pub trait Gene: Clone + Debug + PartialEq {}
