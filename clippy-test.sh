#!/bin/sh -xe

cargo clippy --all-targets --all-features -- \
`# Forbid: Clippy Groups` \
--forbid clippy::perf \
--forbid clippy::style \
\
`# Deny: Clippy Groups` \
--deny clippy::complexity \
--deny clippy::correctness \
--deny clippy::nursery \
--deny clippy::pedantic \
--deny clippy::restriction \
--deny clippy::suspicious \
\
`# Allow: Clippy Lints` \
--allow clippy::blanket-clippy-restriction-lints \
--allow clippy::implicit-return \
--allow clippy::missing-assert-message \
--allow clippy::missing-inline-in-public-items \
--allow clippy::missing-trait-methods \
--allow clippy::question-mark-used \
--allow clippy::redundant-pub-crate \
--allow clippy::ref-patterns \
--allow clippy::self-named-module-files \
--allow clippy::separated-literal-suffix \
--allow clippy::type-repetition-in-bounds `# False positives with serde: https://github.com/rust-lang/rust-clippy/issues/10504` \
--allow clippy::unreachable \
--allow clippy::use-self `# unreachable!() triggers this` \
\
`# Forbid: RustC Groups` \
--forbid future-incompatible \
--forbid rust-2018-compatibility \
--forbid rustdoc::all \
\
`# Deny: RustC Groups` \
--deny future-incompatible \
--deny nonstandard-style \
--deny rust-2018-idioms \
\
`# Forbid: RustC Lints` \
--forbid deprecated-in-future \
--forbid macro-use-extern-crate \
--forbid meta-variable-misuse \
--forbid missing-copy-implementations \
--forbid rustdoc::missing-crate-level-docs \
--forbid non-ascii-idents \
--forbid single-use-lifetimes \
--forbid trivial-casts \
--forbid trivial-numeric-casts \
--forbid unreachable-pub \
--forbid unsafe-code \
--forbid unused-import-braces \
--forbid unused-lifetimes \
--forbid variant-size-differences \
--forbid asm-sub-register \
--forbid bindings-with-variant-name \
--forbid clashing-extern-declarations \
--forbid confusable-idents \
--forbid const-item-mutation \
--forbid deprecated \
--forbid drop-bounds \
--forbid exported-private-dependencies \
--forbid function-item-references \
--forbid improper-ctypes \
--forbid improper-ctypes-definitions \
--forbid inline-no-sanitize \
--forbid invalid-value \
--forbid irrefutable-let-patterns \
--forbid mixed-script-confusables \
--forbid non-shorthand-field-patterns \
--forbid no-mangle-generic-items \
--forbid renamed-and-removed-lints \
--forbid stable-features \
--forbid temporary-cstring-as-ptr \
--forbid trivial-bounds \
--forbid type-alias-bounds \
--forbid uncommon-codepoints \
--forbid unconditional-recursion \
--forbid unknown-lints \
--forbid unnameable-test-items \
--forbid unused-comparisons \
--forbid warnings \
--forbid while-true \
--forbid arithmetic-overflow \
--forbid incomplete-include \
--forbid mutable-transmutes \
--forbid no-mangle-const-items \
--forbid overflowing-literals \
--forbid unconditional-panic \
--forbid unknown-crate-types \
--forbid useless-deprecated \
\
`# Deny: RustC Lints` \
--deny incomplete-features \
--deny missing-debug-implementations \
--deny unstable-features \
--deny unused \
--deny unused-crate-dependencies \
\
`# Allow: RustC Lints` \
--allow box-pointers \
--allow unused-qualifications \
\
\
`# The following is test/bench/dev specific.` \
--allow clippy::as-conversions \
--allow clippy::decimal-literal-representation \
--allow clippy::default-numeric-fallback \
--allow clippy::arithmetic-side-effects \
--allow clippy::let-underscore-must-use \
--allow clippy::min_ident_chars \
--allow clippy::missing-docs-in-private-items \
--allow clippy::panic \
--allow clippy::pub_with_shorthand \
--allow clippy::similar-names \
--allow clippy::single_call_fn \
--allow clippy::semicolon-if-nothing-returned \
--allow clippy::unwrap-used \
\
--allow missing-docs \
--allow unused-crate-dependencies \
--allow unused-results
